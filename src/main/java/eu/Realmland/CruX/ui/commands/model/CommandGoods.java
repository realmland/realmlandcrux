package eu.Realmland.CruX.ui.commands.model;

import org.bukkit.permissions.PermissionDefault;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.CONSTRUCTOR)
public @interface CommandGoods {

    String name();
    String permission() default "realmland.admin";

    String[] aliases() default {};
    PermissionDefault permissionDefault() default PermissionDefault.OP;

    boolean safe() default false;
}
