package eu.Realmland.CruX.ui.commands.model;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.localization.Localization;
import eu.Realmland.CruX.ux.profiles.model.Profile;
import me.MrWener.RealmLandDatabase.Database;
import me.MrWener.RealmLandLoggerLib.Logger;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public abstract class Command {

    protected CruX instance;

    protected Database database;
    protected Localization generalLocalization;
    protected Localization commandsLocalization;
    protected Logger logger;

    public Command(@NotNull CruX instance) {
        this.instance = instance;
        this.generalLocalization = instance.getGeneralLocalization();
        this.commandsLocalization = instance.getCommandsLocalization();
        this.database = instance.getDatabase();
        this.logger = instance.logger();
    }


    public abstract boolean executeCommand(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer);

    public List<String> executeTabComplete(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) {
        return new ArrayList<>();
    }

    public List<String> emptyTabComplete() {
        return new ArrayList<>();
    }

    /**
     * Requires to be only player, returns Profile if its player, returns null otherwise
     * @param sender Sender
     * @return returns Profile if its player, returns null otherwise
     */
    public Profile requireOnlyPlayer(@NotNull CommandSender sender) {
        if(!(sender instanceof Player)) {
            sender.sendMessage("only_player");
            return null;
        }

        return instance.getProfileRegistry().getProfile(((Player) sender).getUniqueId());
    }


}
