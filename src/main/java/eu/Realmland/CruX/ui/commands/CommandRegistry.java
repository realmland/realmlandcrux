package eu.Realmland.CruX.ui.commands;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.localization.Lang;
import eu.Realmland.CruX.localization.Localization;
import eu.Realmland.CruX.services.IService;
import eu.Realmland.CruX.ui.commands.model.Command;
import eu.Realmland.CruX.ui.commands.model.CommandGoods;
import me.MrWener.RealmLandLoggerLib.Logger;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_14_R1.CraftServer;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class CommandRegistry implements IService {

    private CruX instance;
    private Logger logger;

    private Localization localization;

    /**
     * Safe _commands
     */
    private List<String> safeCommands = new ArrayList<>();


    /**
     * Default constructor
     * @param instance CruX Instance
     */
    public CommandRegistry(@NotNull CruX instance) {
        this.instance = instance;
        this.logger = instance.logger();

        this.localization = instance.getGeneralLocalization();
    }

    @Override
    public void initialize() throws Exception {
        search();
    }

    public void registerCommand(@NotNull Command command) {
        try {
            CommandGoods goods = command.getClass().getConstructor(CruX.class).getAnnotation(CommandGoods.class);
            if(goods == null) {
                logger.error("Command class '" + command.getClass().getSimpleName() + "' does not have the CommandGoods annotation!");
                return;
            }

            // safe? If yes
            if(goods.safe())
                safeCommands.add(goods.name());

            // if permission does not exist... gotta register it!
            if(Bukkit.getPluginManager().getPermission(goods.permission()) == null) {
               Bukkit.getPluginManager().addPermission(new Permission(goods.permission(), goods.permissionDefault()));
            }
            ((CraftServer) Bukkit.getServer()).getCommandMap().register("realmlandcrux:" + goods.name(), new org.bukkit.command.Command(goods.name(), "Description", "Usage message", Arrays.asList(goods.aliases())) {
                @Override
                public boolean execute(@NotNull CommandSender sender, @NotNull String label, @NotNull String[] args) {
                    boolean isPlayer = sender instanceof Player;

                    // permission check
                    if(!sender.hasPermission(goods.permission())) {

                        if(isPlayer) {
                            Player p = (Player) sender;
                            sender.sendMessage(localization.get("missing_permissions", instance.getProfileRegistry().getProfile(p.getUniqueId()).getLang()));
                        } else {
                            sender.sendMessage(localization.get("missing_permissions", Lang.EN));
                        }

                        logger.debug("Command '%s' was denied to '%s' due to lack of permissions", goods.name(), sender.getName());
                        return true;
                    }

                    return command.executeCommand(sender, label, args, isPlayer);
                }

                @Override
                public @NotNull List<String> tabComplete(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args) throws IllegalArgumentException {

                    // permission check
                    if(!sender.hasPermission(goods.permission())) {
                        return new ArrayList<>();
                    }
                    List<String> complete = command.executeTabComplete(sender, alias, args, sender instanceof Player);
                    return (complete != null ? complete : new ArrayList<>());
                }
            });

        } catch (NoSuchMethodException e) {
           logger.error("Command class '" + command.getClass().getSimpleName() + "' does not have the right constructor!");
        }
    }

    public void search() {
        try {
            JarFile file = new JarFile(new File(this.getClass().getProtectionDomain().getCodeSource().getLocation().toURI()));
            file.stream()
                    .filter(entry -> !entry.isDirectory())
                    .map(JarEntry::getName)
                    .filter(entry -> entry.endsWith(".class"))
                    // TODO REMOVE IF WANT YOU TO SEARCH IN OTHER PROJECTS
                    .filter(entry -> entry.startsWith("eu/Realmland"))
                    .map(entry -> entry.replaceAll("/", "."))
                    .map(entry -> entry.replace("$", "."))
                    .map(entry -> entry.substring(0, entry.length() - ".class".length()))
                    .map(entry -> {
                        try {
                            return Class.forName(entry);
                        } catch (ClassNotFoundException ignored) {}
                        return null;
                    })
                    .filter(Objects::nonNull)
                    .filter(Command.class::isAssignableFrom)
                    .filter(clazz -> !Modifier.isAbstract(clazz.getModifiers()))
                    .map(clazz -> {
                        try {
                            return clazz.getConstructor(CruX.class);
                        } catch (NoSuchMethodException ignored) {
                            return null;
                        }
                    })
                    .filter(Objects::nonNull)
                    .map(constructor -> {
                        try {
                            return constructor.newInstance(instance);
                        }
                        catch (Exception ignored) {}
                        return null;
                    })
                    .filter(Objects::nonNull)
                    .map(cmd -> (Command) cmd)
                    .forEach(this::registerCommand);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    /**
     * Checks if specified string is command or alias
     * @param command Command
     * @return True/False
     */
    public boolean isSafeCommand(@NotNull String command) {
        return safeCommands.contains(command);
    }

    /**
     * Executes command
     * @param raw    Raw Command
     * @param sender Sender
     * @return True, False
     */
    public boolean executeCommand(@NotNull String raw, @NotNull Player sender) {
        // /ty kokot pojebany
        String[] all = raw.split(" ");

        // ty
        if(all.length == 0)
            return false;

        String cmd = all[0].replaceAll("^/", "");

        String[] args = null;
        // has args
        if(all.length > 1) {
            // (-ty) kokot pojebany
            args = new String[all.length - 1];

            for(int i = 1; i < all.length; i++) {
                args[i-1]= all[i];
            }
        }

        org.bukkit.command.Command command = ((CraftServer) Bukkit.getServer()).getCommandMap().getCommand(cmd);
        if(command == null) {
            return false;
        }
        command.execute(sender, cmd, (args == null ? new String[0] : args));
        return true;
    }
}
