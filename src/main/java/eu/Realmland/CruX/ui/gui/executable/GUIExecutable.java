package eu.Realmland.CruX.ui.gui.executable;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryAction;
import org.jetbrains.annotations.NotNull;

@FunctionalInterface
public interface GUIExecutable {

    void execute(@NotNull Player player, @NotNull InventoryAction action);

}
