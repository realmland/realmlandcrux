package eu.Realmland.CruX.ui.gui.custom;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.localization.Lang;
import eu.Realmland.CruX.ui.gui.GUI;
import eu.Realmland.CruX.ui.gui.component.GUIComponent;
import eu.Realmland.CruX.ui.gui.custom.general.GUITemplate;
import eu.Realmland.CruX.ux.profiles.model.Profile;
import lombok.Getter;
import me.MrWener.RealmLandConfigurationsLib.Configuration;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class InfoGUI extends GUITemplate {

    @Getter
    private static Configuration infoConfiguration;

    private int[] slots = {
            10, 11, 12, 13, 14, 15, 16,
            19, 20, 21, 22, 23, 14, 25,
            28, 19, 30, 31, 32, 33, 34,
            37, 38, 39, 40, 41, 42, 43
    };

    /**
     * Default constructor
     *
     * @param instance Instance of CruX
     */
    public InfoGUI(@NotNull CruX instance) {
        super(instance);

        try {
            infoConfiguration = new Configuration(instance, "localization/info/info.yml", true);
        } catch (IOException e) {
            logger.error("Failed to load info config", e);
        }

    }

    public void showTo(@NotNull Player player, @NotNull Profile profile) {
        Lang lang = profile.getLang();
        String displayName = getInfoConfiguration().getConfig().getString("display_name." + lang.getConfigName());
        if (displayName == null)
            displayName = "null";

        GUI gui = new GUI(displayName, 54, getInstance());

        ConfigurationSection cats = getConfig().getConfigurationSection("categories");
        if (cats != null) {
            List<String> categories = new ArrayList<>(cats.getKeys(false));

            for (int i = 0; i < categories.size(); i++) {
                String leCategory = categories.get(i);

                ConfigurationSection catSec = getConfig().getConfigurationSection("categories." + leCategory);
                if (catSec == null) {
                    gui.addComponent(
                            new GUIComponent(Material.RED_TERRACOTTA,
                                    "§cFailed to load category '" + leCategory + "'",
                                    Arrays.asList("Report this to Administrators;", "Failed to fetch category = null cat sec"),
                                    1,
                                    0,
                                    null));
                    continue;
                }
                String name = catSec.getString("display_name." + lang.getConfigName());
                List<String> data = catSec.getStringList("data." + lang.getConfigName());

                String rawItem = catSec.getString("item");
                if (rawItem == null || rawItem.split(";").length != 2) {
                    logger.error("Raw item data failed to get from config for category %s", leCategory);
                    continue;
                }

                String materialName = rawItem.split(";")[0];
                String materialData = rawItem.split(";")[1];

                if (materialName.equalsIgnoreCase("item")) {
                    materialData = materialData.toUpperCase();
                    if (Material.getMaterial(materialData) == null) {
                        logger.error("Failed to resolve material for category %s", leCategory);
                    } else
                        gui.addComponent(new GUIComponent(Material.getMaterial(materialData), name, data, 1, slots[i], null));
                } else {
                    gui.addComponent(new GUIComponent(materialData, name, data, 1, slots[i], null));
                }


            }
        } else {
            logger.error("Failed to fetch categories!");
            gui.addComponent(
                    new GUIComponent(Material.RED_TERRACOTTA,
                            "§cFailed to load GUI",
                            Arrays.asList("Report this to Administrators;", "Failed to fetch categories = null cats"),
                            1,
                            0,
                            null));
        }


        gui.openTo(player);
    }

    public static void reloadConfig() {
        getInfoConfiguration().load();
    }

    public FileConfiguration getConfig() {
        return getInfoConfiguration().getConfig();
    }

}
