package eu.Realmland.CruX.ui.gui.custom.general;

import eu.Realmland.CruX.CruX;
import lombok.Getter;
import me.MrWener.RealmLandLoggerLib.Logger;
import org.jetbrains.annotations.NotNull;

public abstract class GUITemplate {

    @Getter
    private @NotNull CruX instance;

    @Getter
    protected @NotNull Logger logger;

    /**
     * Default constructor
     *
     * @param instance Instance
     */
    public GUITemplate(@NotNull CruX instance) {
        this.instance = instance;
        this.logger = instance.logger();
    }
}
