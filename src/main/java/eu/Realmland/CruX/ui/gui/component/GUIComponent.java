package eu.Realmland.CruX.ui.gui.component;


import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import eu.Realmland.CruX.ui.gui.executable.GUIExecutable;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.craftbukkit.libs.org.apache.commons.codec.binary.Base64;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.Field;
import java.util.List;
import java.util.UUID;

public class GUIComponent {

    @Getter
    @Setter
    private String callName;

    @Getter
    @Setter
    private GUIExecutable executable = new GUIExecutable() {
        @Override
        public void execute(@NotNull Player player, @NotNull InventoryAction action) {
            return;
        }
    };

    @NotNull
    @Getter
    @Setter
    private Material material;

    @Getter
    @Setter
    private int stackSize = 0;

    @Getter
    @Setter
    private int position = 0;

    @Getter
    @NotNull
    private ItemStack finalItem;


    /**
     * Constructor
     *
     * @param material   Material
     * @param stackSize  Stack site
     * @param executable Code that will be executed on click
     */
    public GUIComponent(@NotNull Material material, int stackSize, int position, @NotNull GUIExecutable executable) {
        this(material, null, stackSize, position, executable);
    }

    /**
     * Constructor
     *
     * @param material    Material
     * @param displayName Item displayName
     * @param lore        Item lore
     * @param stackSize   Stack site
     * @param executable  Code that will be executed on click
     */
    public GUIComponent(@NotNull Material material, @Nullable String displayName, @Nullable List<String> lore, int stackSize, int position, @Nullable GUIExecutable executable) {
        if (executable != null)
            this.executable = executable;
        this.material = material;
        this.stackSize = stackSize;
        this.position = position;

        this.finalItem = new ItemStack(material, stackSize);
        ItemMeta meta = finalItem.getItemMeta();
        if (meta != null) {
            if (displayName != null)
                meta.setDisplayName(displayName);
            if (lore != null)
                meta.setLore(lore);

            this.finalItem.setItemMeta(meta);
        }

    }

    /**
     * Constructor
     *
     * @param material   Material
     * @param itemMeta   Item MetaData
     * @param stackSize  Stack site
     * @param executable Code that will be executed on click
     */
    public <Meta extends ItemMeta> GUIComponent(@NotNull Material material, @Nullable Meta itemMeta, int stackSize, int position, @NotNull GUIExecutable executable) {
        this.executable = executable;
        this.material = material;
        this.stackSize = stackSize;
        this.position = position;

        this.finalItem = new ItemStack(material, stackSize);
        if (itemMeta != null)
            this.finalItem.setItemMeta(itemMeta);
    }


    /**
     * Constructor
     *
     * @param item       Item
     * @param executable Code that will be executed on click
     */
    public GUIComponent(@NotNull ItemStack item, int position, @Nullable GUIExecutable executable) {
        this.executable = executable;
        this.material = item.getType();
        this.stackSize = item.getAmount();
        this.position = position;

        this.finalItem = item;
    }

    /**
     * Skull item constructor
     *
     * @param displayName DisplayName
     * @param lore        Lore
     * @param url         Skin URL
     * @param position    Position in GUI
     * @param amount      Amount
     * @param executable  Executable
     */
    public GUIComponent(@NotNull String url, @Nullable String displayName, @Nullable List<String> lore, int amount, int position, @Nullable GUIExecutable executable) {
        this.executable = executable;
        this.material = Material.PLAYER_HEAD;
        this.stackSize = amount;
        this.position = position;

        byte[] encodedData = Base64.encodeBase64(String.format("{textures:{SKIN:{url:\"%s\"}}}", url).getBytes());

        GameProfile gp = new GameProfile(UUID.randomUUID(), null);
        gp.getProperties().put("textures", new Property("textures", new String(encodedData)));

        this.finalItem = new ItemStack(material, amount);
        SkullMeta meta = (SkullMeta) finalItem.getItemMeta();
        if (meta != null) {
            if (displayName != null)
                meta.setDisplayName(displayName);
            if (lore != null)
                meta.setLore(lore);

            Field profileField = null;
            try {
                // get field profile
                profileField = meta.getClass().getDeclaredField("profile");
                profileField.setAccessible(true);
                profileField.set(meta, gp);
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }


        this.finalItem.setItemMeta(meta);
    }

    /**
     * Skull item constructor
     *
     * @param displayName DisplayName
     * @param lore        Lore
     * @param skin        Player with skin
     * @param position    Position in GUI
     * @param amount      Amount
     * @param executable  Executable
     */
    public GUIComponent(@Nullable String displayName, @Nullable List<String> lore, @NotNull OfflinePlayer skin, int position, int amount, @Nullable GUIExecutable executable) {
        if (executable != null)
            this.executable = executable;
        this.material = Material.PLAYER_HEAD;
        this.stackSize = amount;
        this.position = position;

        this.finalItem = new ItemStack(material, amount);
        SkullMeta meta = (SkullMeta) finalItem.getItemMeta();
        if (meta != null) {
            if (displayName != null)
                meta.setDisplayName(displayName);
            if (lore != null)
                meta.setLore(lore);

            meta.setOwningPlayer(skin);
        }


        this.finalItem.setItemMeta(meta);
    }

}
