package eu.Realmland.CruX.ui.gui;


import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.nbt.ItemNBTEditor;
import eu.Realmland.CruX.ui.gui.component.GUIComponent;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.RandomStringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Minecraft GUI
 */
public class GUI implements Listener {

    private CruX plugin;

    @Setter
    @NotNull
    private String title;

    @Getter
    private int size;

    @NotNull
    private Inventory inventory;

    @Getter
    @Setter
    @Nullable
    private Sound openSound;

    @Getter
    @NotNull
    private final Map<String, GUIComponent> components = new HashMap<>();

    /**
     * Default constructor
     *
     * @param title GUI's title, supports colors
     */
    public GUI(@NotNull String title, int size, @NotNull CruX plugin) {
        this.plugin = plugin;
        this.title = title;
        this.size = size;
        inventory = Bukkit.createInventory(null, size, ChatColor.translateAlternateColorCodes('&', title));
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }

    /**
     * Constructor
     *
     * @param title GUI's title, supports colors
     */
    public GUI(@NotNull String title, InventoryType type, @NotNull CruX plugin) {
        this(title, type.getDefaultSize(), plugin);
    }


    /**
     * Adds component to the GUI components
     *
     * @param component GUIComponent
     * @return Edited GUI.
     */
    public @NotNull GUI addComponent(@Nullable GUIComponent component) {
        if (component == null)
            return this;

        String callName = RandomStringUtils.random(10);
        if (components.containsKey(callName))
            return addComponent(component);

        component.setCallName(callName);
        components.put(callName, component);
        return this;
    }

    public void addComponents(@NotNull List<GUIComponent> componentList) {
        componentList.forEach(this::addComponent);
    }

    /**
     * Generates inventory
     */
    private void generateInventory() {
        components.forEach((callName, comp) -> {
                ItemStack item =  ItemNBTEditor.writeNBT(comp.getFinalItem(), "guiKey", callName);
                ItemMeta meta = item.getItemMeta();
                if(meta != null) {
                    meta.addItemFlags(ItemFlag.values());
                    item.setItemMeta(meta);
                }
                inventory.setItem(comp.getPosition(), item);
        });
    }

    public Inventory getInventory() {
        generateInventory();
        return inventory;
    }

    /**
     * Opens inventory to Player
     *
     * @param guy Player
     */
    public void openTo(@NotNull Player guy) {
        generateInventory();

        if (openSound != null)
            guy.playSound(guy.getLocation(), openSound, 1f, 1f);

        guy.openInventory(inventory);
    }

    @EventHandler
    public void onItem(InventoryClickEvent event) {
        ItemStack item = event.getCurrentItem();

        // we dont want anything moving boys
        if(this.inventory.getViewers()
                .stream()
                .map(HumanEntity::getUniqueId)
                .collect(Collectors.toList())
                .contains(event.getWhoClicked().getUniqueId())) {
            event.setCancelled(true);
        }
        String callName = null;
        if (item != null)
            callName = ItemNBTEditor.getNBT(item, "guiKey");

        if (callName != null) {
            GUIComponent component = components.get(callName);
            if (component != null)
                if(component.getExecutable() != null)
                    component.getExecutable().execute((Player) event.getWhoClicked(), event.getAction());
            event.setCancelled(true);
        }
    }
}
