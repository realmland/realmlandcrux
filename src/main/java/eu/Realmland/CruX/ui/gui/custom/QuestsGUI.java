package eu.Realmland.CruX.ui.gui.custom;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.localization.Lang;
import eu.Realmland.CruX.localization.Localization;
import eu.Realmland.CruX.localization.model.LangNode;
import eu.Realmland.CruX.ui.gui.GUI;
import eu.Realmland.CruX.ui.gui.component.GUIComponent;
import eu.Realmland.CruX.ui.gui.custom.general.GUITemplate;
import eu.Realmland.CruX.ux.profiles.model.Profile;
import eu.Realmland.CruX.ux.quests.QuestRegistry;
import eu.Realmland.CruX.ux.quests.model.Quest;
import eu.Realmland.CruX.ux.quests.model.QuestType;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.MissingFormatArgumentException;
import java.util.concurrent.atomic.AtomicInteger;

public class QuestsGUI extends GUITemplate {

    @NotNull
    private QuestRegistry registry;

    @NotNull
    private Localization localization;

    /**
     * Default constructor
     *
     * @param instance CruX Instance
     */
    public QuestsGUI(@NotNull CruX instance, @NotNull QuestRegistry registry) {
        super(instance);

        this.registry = registry;
        this.localization = registry.getLocalization();
    }


    public void showTo(@NotNull Player target, @NotNull Profile targetProfile, @NotNull Lang lang) {
        GUI gui = new GUI(localization.get("gui_title", lang), 9, registry.getInstance());

        AtomicInteger pos = new AtomicInteger(-1);
        registry.getQuests().forEach((name, quest) -> {
            if (!quest.getCompleted().contains(target.getUniqueId()))
                gui.addComponent(generateQuestComponent(lang, pos.addAndGet(1), target, targetProfile, quest));
        });


        if (!(gui.getComponents().size() > 0))
            gui.addComponent(new GUIComponent(Material.RED_TERRACOTTA, localization.get("no_quests_available", lang), null, 1, 4, null));
        target.openInventory(gui.getInventory());
    }

    private GUIComponent generateQuestComponent(@NotNull Lang lang, int pos, @NotNull Player target, @NotNull Profile targetProfile, @NotNull Quest quest) {

        // Info about quest
        List<String> itemLore = new ArrayList<>();
        itemLore.add(localization.get("quest_type", lang) + " " +
                (quest.getType() == QuestType.DAILY_QUEST ?
                        localization.get("quest_type_daily", lang) :
                        localization.get("quest_type_npc", lang)));
        itemLore.add("§f");

        // Add quest lore
        itemLore.addAll(addStylisticLore(lang, quest.getQuestLore().listIterator()));

        // All about goal
        itemLore.addAll(addStylisticLore(lang, quest.getGoal().getGoalLore().listIterator()));

        // Add goal progression
        itemLore.addAll(quest.getGoal().getGoalProgressionLore(target.getUniqueId()));

        List<String> rewards = new ArrayList<>();
        quest.getRewardBulk().getRewards().forEach(reward -> {
            String value = reward.getValue();
            try {
                int val = Integer.valueOf(value);
                value = String.valueOf(quest.calculateDifficultyBonus(val));
            } catch (NumberFormatException ignored) {
            }
            rewards.add(value);
        });

        itemLore.add("§f ");
        List<LangNode> rewardsLore = quest.getRewardBulk().getRewardLore();
        rewardsLore.forEach(node -> {
            try {
                itemLore.add(node.get(lang, rewards.toArray()));
            } catch (MissingFormatArgumentException x) {
                registry.getInstance().logger().warn("Rewards lore for quest '%s' is missing format specifier '%s'! ", quest.getName(), x.getFormatSpecifier());
            }

        });
        itemLore.add("§f");
        itemLore.add("§aBonus +" + quest.getDifficultyPercentage() * 100 + "%");


        if (quest.getGoal().isGoalReached(target)) {
            itemLore.add("§f ");
            itemLore.add(ChatColor.translateAlternateColorCodes('&', localization.get("turn_in", lang)));
        }

        return new GUIComponent(Material.LANTERN, "§a" + quest.getDisplayName().get(lang), itemLore, 1, pos, (player, action) -> {
            boolean success = quest.turnIn(player, targetProfile);
            player.closeInventory();
            player.sendMessage((success ? localization.get("turn_in_success", targetProfile.getLang())
                    : localization.get("turn_in_failed", targetProfile.getLang())));
        });
    }

    private List<String> addStylisticLore(@NotNull Lang lang, @NotNull ListIterator<LangNode> nodes) {
        List<String> inReturn = new ArrayList<>();
        while (nodes.hasNext()) {
            LangNode nodeLine = nodes.next();
            String localizedLine = nodeLine.get(lang);
            if (!Localization.stripColor(localizedLine).equalsIgnoreCase("")) {
                inReturn.add(localizedLine);
                if (!nodes.hasNext())
                    inReturn.add("§f");

            }
        }
        return (inReturn.size() > 1 ? inReturn : new ArrayList<>());
    }

}
