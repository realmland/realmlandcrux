package eu.Realmland.CruX.ui.gui.custom;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.localization.Lang;
import eu.Realmland.CruX.localization.Localization;
import eu.Realmland.CruX.ui.gui.GUI;
import eu.Realmland.CruX.ui.gui.component.GUIComponent;
import eu.Realmland.CruX.ui.gui.custom.general.GUITemplate;
import eu.Realmland.CruX.ui.gui.executable.GUIExecutable;
import eu.Realmland.CruX.ux.profiles.model.Profile;
import eu.Realmland.CruX.ux.ranks.model.Rank;
import lombok.Getter;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.inventory.Inventory;
import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ProfileGUI extends GUITemplate {

    @NotNull
    private Localization localization;

    @Getter
    private @NotNull SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");

    /**
     * Default constructor
     *
     * @param instance Instance of CruX
     */
    public ProfileGUI(@NotNull CruX instance) {
        super(instance);
        this.localization = instance.getGeneralLocalization();
    }

    public Inventory generateGUIProfile(@NotNull Lang language, @NotNull Profile targetProfile, @NotNull Player targetPlayer, boolean includeReport) {
        GUI profileGui = new GUI(localization.get("gui_profile_title", language, targetPlayer.getName()), 54, getInstance());
        String reportUrl = "http://textures.minecraft.net/texture/e7f9c6fef2ad96b3a5465642ba954671be1c4543e2e25e56aef0a47d5f1f";

        if (includeReport)
            profileGui.addComponent(new GUIComponent(reportUrl, localization.get("report_player", language, targetPlayer.getName()), null, 1, 8, new GUIExecutable() {
                @Override
                public void execute(@NotNull Player player, @NotNull InventoryAction action) {
                    player.sendMessage(localization.get("report_player_command", language, targetPlayer.getName()));
                    player.closeInventory();
                }
            }));


        if (targetProfile.isStaff()) {
            profileGui.addComponent(new GUIComponent(Material.DIAMOND, localization.get("player_is_staff", language), null, 1, 22, null));
        }
        if (getInstance().getControlCenter().getAfkService().isPlayerAfk(targetPlayer.getUniqueId())) {
            GUIComponent component = getAFKComponent(targetPlayer, language, 7);
            if (component != null)
                profileGui.addComponent(component);
        }

        profileGui
                .addComponent(getPlayerComponent(targetPlayer, language, 13))
                .addComponent(getLangComponent(targetProfile.getLang(), language, 19))
                // TODO CONNECT
                .addComponent(getRankComponent(targetProfile.getRankPoints(), language, 25))
                .addComponent(getCoinsComponent(-1, language, 29))
                .addComponent(getGemsComponent(-1, language, 33))
                .addComponent(getDiscordComponent(targetProfile, language, 36));


        for (int i = 46; i <= 54; i++) {
            profileGui.addComponent(new GUIComponent(Material.GRAY_STAINED_GLASS_PANE, "§f", null, 1, i - 1, null));
        }
        return profileGui.getInventory();
    }

    /**
     * Generates Player component
     *
     * @param player      Player
     * @param guiLanguage Language the GUI is in.
     * @param position    Position of component
     * @return Player Component
     */
    private GUIComponent getPlayerComponent(@NotNull Player player, @NotNull Lang guiLanguage, int position) {
        List<String> params = new ArrayList<>();
        params.add("§f");
        params.add(localization.get("name_game_mode", guiLanguage) + " §e" + player.getGameMode().toString().toLowerCase());
        params.add("§fPing §e" + ((CraftPlayer) player).getHandle().ping + "ms");
        params.add(localization.get("name_playing_since", guiLanguage) + " §e" + format.format(new Date(player.getFirstPlayed())));
        params.add("§f");
        return new GUIComponent(player.getName(), params, player, position, 1, null);
    }

    /**
     * Generates AFK Component
     *
     * @param player   Player
     * @param guiLang  Gui Language
     * @param position Position
     * @return AFK Component
     */
    private GUIComponent getAFKComponent(@NotNull Player player, @NotNull Lang guiLang, int position) {
        long lastAction = 0;
        try {
            lastAction = getInstance().getControlCenter().getAfkService().getLastAction(player.getUniqueId());
        } catch (Exception x) {
            return null;
        }
        return new GUIComponent(Material.GRAY_WOOL,
                localization.get("player_is_afk", guiLang, ((System.currentTimeMillis() - lastAction) / 60000)),
                null,
                1,
                position,
                null);
    }


    /**
     * Generates Coins component
     *
     * @param coins       Coins
     * @param guiLanguage Language the GUI is in.
     * @param position    Position of component
     * @return Coins Component
     */
    private GUIComponent getCoinsComponent(int coins, @NotNull Lang guiLanguage, int position) {
        return new GUIComponent(Material.GOLD_INGOT, localization.get("name_coins", guiLanguage) + " §e" + coins, null, 1, position, null);
    }


    /**
     * Generates Gems component
     *
     * @param gems        Gems
     * @param guiLanguage Language the GUI is in.
     * @param position    Position of component
     * @return Gems Component
     */
    private GUIComponent getGemsComponent(int gems, @NotNull Lang guiLanguage, int position) {
        return new GUIComponent(Material.EMERALD, localization.get("name_gems", guiLanguage) + " §e" + gems, null, 1, position, null);
    }

    /**
     * Generates Discord component
     *
     * @param profile     Profile
     * @param guiLanguage Language the GUI is in.
     * @param position    Position of component
     * @return Rank Component
     */
    public GUIComponent getDiscordComponent(@NotNull Profile profile, @NotNull Lang guiLanguage, int position) {
        String url = "http://textures.minecraft.net/texture/4d42337be0bdca2128097f1c5bb1109e5c633c17926af5fb6fc20000011aeb53";

        String discordID = profile.getDiscordID().trim();

        if (!discordID.isEmpty()) {
            return new GUIComponent(url, "§9§lDiscord §f" + discordID, null, 1, position, null);
        }
        return null;
    }


    /**
     * Generates Rank component
     *
     * @param rp          Rank Points
     * @param guiLanguage Language the GUI is in.
     * @param position    Position of component
     * @return Rank Component
     */
    public GUIComponent getRankComponent(int rp, @NotNull Lang guiLanguage, int position) {
        String rpUrl = "http://textures.minecraft.net/texture/2cdc0feb7001e2c10fd5066e501b87e3d64793092b85a50c856d962f8be92c78";
        List<String> lore = new ArrayList<>();
        lore.add("§f");
        lore.add(localization.get("rank_points", guiLanguage) + " §e" + rp);
        lore.add(localization.get("rank", guiLanguage) + " §e" + getInstance().getRankRegistry().getRank(rp).getPrefix());
        lore.add("§f");

        Rank next = getInstance().getRankRegistry().getNextRank(rp);
        int toNext = next.getRRP() - rp;

        lore.add(localization.get("next_rank", guiLanguage, next.getPrefix(), toNext));
        lore.add("§f");

        return new GUIComponent(rpUrl, localization.get("rank_info", guiLanguage), lore, 1, position, null);
    }

    /**
     * Generates language component
     *
     * @param lang        Lang
     * @param guiLanguage Language the GUI is in.
     * @param position    Position of component
     * @return Component
     */
    public GUIComponent getLangComponent(@NotNull Lang lang, @NotNull Lang guiLanguage, int position) {
        String langUrl = null;
        switch (lang) {
            case SK:
                langUrl = "http://textures.minecraft.net/texture/6c72a8c115a1fb669a25715c4d15f22136ac4c2452784e4894b3d56bc5b0b9";
                break;
            case CZ:
                langUrl = "http://textures.minecraft.net/texture/48152b7334d7ecf335e47a4f35defbd2eb6957fc7bfe94212642d62f46e61e";
                break;
            case EN:
                langUrl = "http://textures.minecraft.net/texture/a1701f21835a898b20759fb30a583a38b994abf60d3912ab4ce9f2311e74f72";
                break;
        }

        return new GUIComponent(langUrl, localization.get("name_language", guiLanguage) + " §e" + lang.name(), null, 1, position, null);
    }

//    List<GUIComponent> special = new ArrayList<>();
//        if (targetPlayer.getName().length() <= 9) {
//        char[] letters = targetPlayer.getName().toCharArray();
//        for (int i = 0; i < letters.length; i++) {
//            char letter = letters[i];
//            int code = (int) letter;
////          make lowercase if in uppercase
//            if ((int) letter >= 90)
//                code -= 32;
//
//            letter = (char) code;
//            special.add(new GUIComponent(getLetterUrl(letter), "§f", null, 1, 46 + i, null));
//        }
//    }


//    public String getLetterUrl(@NotNull char letter) {
//        switch (letter) {
//            case 'A':
//                return "http://textures.minecraft.net/texture/a67d813ae7ffe5be951a4f41f2aa619a5e3894e85ea5d4986f84949c63d7672e";
//            case 'B':
//                return "http://textures.minecraft.net/texture/50c1b584f13987b466139285b2f3f28df6787123d0b32283d8794e3374e23";
//            case 'C':
//                return "http://textures.minecraft.net/texture/abe983ec478024ec6fd046fcdfa4842676939551b47350447c77c13af18e6f";
//            case 'D':
//                return "http://textures.minecraft.net/texture/3193dc0d4c5e80ff9a8a05d2fcfe269539cb3927190bac19da2fce61d71";
//            case 'E':
//                return "http://textures.minecraft.net/texture/dbb2737ecbf910efe3b267db7d4b327f360abc732c77bd0e4eff1d510cdef";
//            case 'F':
//                return "http://textures.minecraft.net/texture/b183bab50a3224024886f25251d24b6db93d73c2432559ff49e459b4cd6a";
//            case 'G':
//                return "http://textures.minecraft.net/texture/1ca3f324beeefb6a0e2c5b3c46abc91ca91c14eba419fa4768ac3023dbb4b2";
//            case 'H':
//                return "http://textures.minecraft.net/texture/31f3462a473549f1469f897f84a8d4119bc71d4a5d852e85c26b588a5c0c72f";
//            case 'I':
//                return "http://textures.minecraft.net/texture/46178ad51fd52b19d0a3888710bd92068e933252aac6b13c76e7e6ea5d3226";
//            case 'J':
//                return "http://textures.minecraft.net/texture/3a79db9923867e69c1dbf17151e6f4ad92ce681bcedd3977eebbc44c206f49";
//            case 'K':
//                return "http://textures.minecraft.net/texture/9461b38c8e45782ada59d16132a4222c193778e7d70c4542c9536376f37be42";
//            case 'L':
//                return "http://textures.minecraft.net/texture/319f50b432d868ae358e16f62ec26f35437aeb9492bce1356c9aa6bb19a386";
//            case 'M':
//                return "http://textures.minecraft.net/texture/49c45a24aaabf49e217c15483204848a73582aba7fae10ee2c57bdb76482f";
//            case 'N':
//                return "http://textures.minecraft.net/texture/35b8b3d8c77dfb8fbd2495c842eac94fffa6f593bf15a2574d854dff3928";
//            case 'O':
//                return "http://textures.minecraft.net/texture/d11de1cadb2ade61149e5ded1bd885edf0df6259255b33b587a96f983b2a1";
//            case 'P':
//                return "http://textures.minecraft.net/texture/a0a7989b5d6e621a121eedae6f476d35193c97c1a7cb8ecd43622a485dc2e912";
//            case 'Q':
//                return "http://textures.minecraft.net/texture/43609f1faf81ed49c5894ac14c94ba52989fda4e1d2a52fd945a55ed719ed4";
//            case 'R':
//                return "http://textures.minecraft.net/texture/a5ced9931ace23afc351371379bf05c635ad186943bc136474e4e5156c4c37";
//            case 'S':
//                return "http://textures.minecraft.net/texture/3e41c60572c533e93ca421228929e54d6c856529459249c25c32ba33a1b1517";
//            case 'T':
//                return "http://textures.minecraft.net/texture/1562e8c1d66b21e459be9a24e5c027a34d269bdce4fbee2f7678d2d3ee4718";
//            case 'U':
//                return "http://textures.minecraft.net/texture/607fbc339ff241ac3d6619bcb68253dfc3c98782baf3f1f4efdb954f9c26";
//            case 'V':
//                return "http://textures.minecraft.net/texture/cc9a138638fedb534d79928876baba261c7a64ba79c424dcbafcc9bac7010b8";
//            case 'W':
//                return "http://textures.minecraft.net/texture/269ad1a88ed2b074e1303a129f94e4b710cf3e5b4d995163567f68719c3d9792";
//            case 'X':
//                return "http://textures.minecraft.net/texture/5a6787ba32564e7c2f3a0ce64498ecbb23b89845e5a66b5cec7736f729ed37";
//            case 'Y':
//                return "http://textures.minecraft.net/texture/c52fb388e33212a2478b5e15a96f27aca6c62ac719e1e5f87a1cf0de7b15e918";
//            case 'Z':
//                return "http://textures.minecraft.net/texture/90582b9b5d97974b11461d63eced85f438a3eef5dc3279f9c47e1e38ea54ae8d";
//            default:
//                return null;
//        }
//    }
}
