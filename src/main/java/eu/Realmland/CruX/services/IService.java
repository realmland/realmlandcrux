package eu.Realmland.CruX.services;

/**
 * Represents class that has to be Initialized/Terminated
 */
public interface IService {

    void initialize() throws Exception;

    default void terminate() throws Exception {}

}
