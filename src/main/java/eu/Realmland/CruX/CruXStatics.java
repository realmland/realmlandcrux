package eu.Realmland.CruX;

import org.jetbrains.annotations.NotNull;

/**
 *
 */
public class CruXStatics {

    /**
     * Checks if instance of CruX is running.
     *
     * @return Boolean
     */
    public static boolean isInstanceRunning() {
        return CruX.get().running;
    }

    /**
     * @return Instance of CruX
     */
    public static @NotNull CruX getInstance() {
        return CruX.get();
    }

}
