package eu.Realmland.CruX.statics;

import eu.Realmland.CruX.tuples.Pair;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class WorldStatics {

    /**
     * Sorts locations
     *
     * @param locations Locations to be sorted
     * @return Pair
     */
    public static Pair<Location, Location> sortLocations(@NotNull Pair<Location, Location> locations) {
        if (!locations.isValid())
            return null;

        Location first = locations.getFirst();
        Location second = locations.getSecond();


        Pair<Integer, Integer> xAxis = calculateHigher(first.getBlockX(), second.getBlockX(), false);
        int minX = xAxis.getSecond(), maxX = xAxis.getFirst();

        Pair<Integer, Integer> zAxis = calculateHigher(first.getBlockZ(), second.getBlockZ(), true);
        int minZ = zAxis.getSecond(), maxZ = zAxis.getFirst();

        Pair<Integer, Integer> yAxis = calculateHigher(first.getBlockY(), second.getBlockY(), true);
        int minY = yAxis.getSecond(), maxY = yAxis.getFirst();

        return new Pair<>(new Location(second.getWorld(), maxX, maxY, maxZ), new Location(first.getWorld(), minX, minY, minZ));
    }

    /**
     * Sorts numbers to Pair, the higher one onto the first position, the other one as second position
     *
     * @param first  First Number
     * @param second Second Number
     * @param abs    Calculate as absolute numbers?
     * @return Pair
     */
    public static Pair<Integer, Integer> calculateHigher(int first, int second, boolean abs) {
        Pair<Integer, Integer> data = new Pair<>(null, null);

        if (abs) {
            if (Math.abs(first) > Math.abs(second)) {
                data.setFirst(first);
                data.setSecond(second);
            } else {
                data.setFirst(second);
                data.setSecond(first);
            }
        } else {
            if (first > second) {
                data.setFirst(first);
                data.setSecond(second);
            } else {
                data.setFirst(second);
                data.setSecond(first);
            }
        }

        return data;
    }

    /**
     * @param locations
     */
    public static void showCuboidArea(@NotNull Pair<Location, Location> locations, @NotNull Player player) {
        World world = locations.getFirst().getWorld();
        Location higher = locations.getFirst();
        Location smaller = locations.getSecond();

        for (int x = smaller.getBlockX(); x <= higher.getBlockX(); x++) {
            // we gotta to low&&high Z line now
            if (x == smaller.getBlockX() || x == higher.getBlockX()) {
                System.out.println("Z");
                for (int z = smaller.getBlockZ(); z <= higher.getBlockZ(); z++) {
                    ProtocolStatics.sendParticlePacket(player, Color.YELLOW, 1f, x, higher.getBlockY(), z, 10);
                    ProtocolStatics.sendParticlePacket(player, Color.YELLOW, 1f, x, smaller.getBlockY(), z, 10);
                }
            }

            // X up
            ProtocolStatics.sendParticlePacket(player, Color.YELLOW, 1f, x, higher.getBlockY(), higher.getBlockZ(), 10);
            ProtocolStatics.sendParticlePacket(player, Color.YELLOW, 1f, x, higher.getBlockY(), smaller.getBlockZ(), 10);
            // X down
            ProtocolStatics.sendParticlePacket(player, Color.YELLOW, 1f, x, smaller.getBlockY(), higher.getBlockZ(), 10);
            ProtocolStatics.sendParticlePacket(player, Color.YELLOW, 1f, x, smaller.getBlockY(), smaller.getBlockZ(), 10);

        }

        for (int y = smaller.getBlockY(); y <= higher.getBlockY(); y++) {
            ProtocolStatics.sendParticlePacket(player, Color.YELLOW, 1f, smaller.getBlockX(), y, smaller.getBlockZ(), 10);
            ProtocolStatics.sendParticlePacket(player, Color.YELLOW, 1f, higher.getBlockX(), y, higher.getBlockZ(), 10);

            ProtocolStatics.sendParticlePacket(player, Color.YELLOW, 1f, smaller.getBlockX(), y, higher.getBlockZ(), 10);
            ProtocolStatics.sendParticlePacket(player, Color.YELLOW, 1f, higher.getBlockX(), y, smaller.getBlockZ(), 10);
        }


    }


}
