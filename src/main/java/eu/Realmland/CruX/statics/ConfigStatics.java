package eu.Realmland.CruX.statics;

import eu.Realmland.CruX.localization.Lang;
import eu.Realmland.CruX.localization.Localization;
import eu.Realmland.CruX.localization.model.LangNode;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

/**
 * Some useful static methods
 */
public class ConfigStatics {

    /**
     * Loads string from configuration as LangNode
     * The config structure must look like
     * <code>
     * lang_data_root:
     *  sk: "Slovenský"
     *  cz: "Českej vole"
     *  en: "English"
     * </code>
     *
     * @param configuration Configuration
     * @param root          Root of LangNode (Full path)
     * @return LangNode; Null if Section does not exists
     */
    public static LangNode getLangNodeFromString(@NotNull FileConfiguration configuration, @NotNull String root) {
        ConfigurationSection section = configuration.getConfigurationSection(root);
        if (section == null)
            return null;

        return getLangNodeFromString(section);
    }

    /**
     * Loads string from configuration as LangNode
     * The config root structure must look like
     * <code>
     * lang_data_root:
     *  sk: "Slovenský"
     *  cz: "Českej vole"
     *  en: "English"
     * </code>
     *
     * @param root Root of LangNode
     * @return LangNode
     */
    public static LangNode getLangNodeFromString(@NotNull ConfigurationSection root) {
        Map<Lang, String> nodeElems = new HashMap<>();
        for (final String langName : root.getKeys(false)) {
            if (Lang.isLang(langName)) {
                Lang lang = Lang.of(langName.toLowerCase());
                String data = root.getString(langName);

                if (data == null)
                    data = "";

                nodeElems.put(lang, data);
            }
        }
        return new LangNode(nodeElems, null, null, null);
    }

    /**
     * Loads array from configuration as LangNode
     * The config root structure must look like
     * <code>
     * lang_data_root:
     * sk:
     * - "Slovaci"
     * - "su"
     * - "naj"
     * cz:
     * - "Pivo"
     * - "vole"
     * en:
     * - "God damn..."
     * - "those fellers..."
     * </code>
     *
     * @param root Configuration root
     * @return LangNode
     */
    public static LangNode getLangNodeFromArray(@NotNull ConfigurationSection root) {
        Map<Lang, String> nodes = new HashMap<>();
        for (final String langName : root.getKeys(false))
            if (Lang.isLang(langName)) {
                Lang lang = Lang.of(langName.toLowerCase());
                String lines = Localization.join("\n", root.getStringList(langName));
                nodes.put(lang, lines);
            }
        return new LangNode(nodes, null, null, null);
    }
}
