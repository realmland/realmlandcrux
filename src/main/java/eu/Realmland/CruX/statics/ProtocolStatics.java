package eu.Realmland.CruX.statics;

import net.minecraft.server.v1_14_R1.PacketPlayOutWorldParticles;
import net.minecraft.server.v1_14_R1.ParticleParamRedstone;
import org.bukkit.Color;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class ProtocolStatics {


    /**
     * Sends packet about world particle to player
     * <a>https://wiki.vg/Protocol#Particle_2</a>
     *
     * @param player Player to receive packet
     * @param color  Color of particle
     * @param size   Size of particle
     * @param x      X in world
     * @param y      Y in world
     * @param z      Z in World
     */
    public static void sendParticlePacket(@NotNull Player player, @NotNull Color color, float size, float x, float y, float z, int count) {
        ((CraftPlayer) player).getHandle().
                playerConnection.sendPacket(
                new PacketPlayOutWorldParticles(
                        new ParticleParamRedstone(color.getRed() / 255f, color.getGreen() / 255f, color.getBlue() / 255f, size),
                        false, x, y, z, 0, 0, 0, 1, count));
    }


}
