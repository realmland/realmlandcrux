package eu.Realmland.CruX.endpoint;

import com.sun.net.httpserver.HttpServer;
import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.endpoint.commands.EndpointCommandRegistry;
import eu.Realmland.CruX.services.IService;
import lombok.Getter;
import me.MrWener.RealmLandLoggerLib.Logger;
import org.bukkit.Bukkit;
import org.jetbrains.annotations.NotNull;
import org.json.simple.JSONObject;

import java.net.InetSocketAddress;

public class Endpoint implements IService {

    @Getter
    private HttpServer server;

    @Getter
    private @NotNull CruX instance;

    @Getter
    private @NotNull Logger logger;

    @Getter
    private TokenRegistry tokenRegistry;

    @Getter
    private EndpointCommandRegistry endpointCommandRegistry;



    /**
     * Default constructor
     *
     * @param instance Instance
     */
    public Endpoint(@NotNull CruX instance) {
        this.instance = instance;
        this.logger = instance.logger();
        this.tokenRegistry = new TokenRegistry(this);
        this.endpointCommandRegistry = new EndpointCommandRegistry(this);
    }

    @Override
    public void initialize() throws Exception {
        server = HttpServer.create(new InetSocketAddress(getInstance().getConfig().getInt("endpoint-settings.port")), 29870);
        Bukkit.getScheduler().runTaskAsynchronously(instance, () -> server.start());

        endpointCommandRegistry.initialize();
    }

    @Override
    public void terminate() throws Exception {
        endpointCommandRegistry.terminate();
        server.stop(0);
    }


    /**
     * Response type of Endpoint context.
     */
    public enum ResponseType {
        SUCCESS, FAILED;

        public byte[] toResponse(@NotNull String message) {
            JSONObject response = new JSONObject();
            response.put("type", name().toLowerCase());
            response.put("message", message);
            return response.toJSONString().getBytes();
        }
    }

}
