package eu.Realmland.CruX.endpoint.commands;

import com.sun.net.httpserver.HttpExchange;
import eu.Realmland.CruX.endpoint.Endpoint;
import eu.Realmland.CruX.endpoint.TokenRegistry;
import eu.Realmland.CruX.endpoint.commands.model.EndpointCommand;
import eu.Realmland.CruX.endpoint.commands.model.EndpointGoods;
import eu.Realmland.CruX.services.IService;
import eu.Realmland.CruX.ux.pairer.PairRegistry;
import eu.Realmland.CruX.ux.profiles.model.Profile;
import me.MrWener.RealmLandLoggerLib.Logger;
import org.bukkit.Bukkit;
import org.jetbrains.annotations.NotNull;
import org.json.simple.JSONObject;
import sun.misc.IOUtils;

import java.io.InputStream;
import java.util.Map;

public class EndpointCommandRegistry implements IService {

    private @NotNull Endpoint endpoint;
    private @NotNull Logger logger;

    private TokenRegistry tokenRegistry;

    private int staffOnline = 0;
    private int online = 0;

    /**
     * Default constructor
     * @param endpoint Endpoint
     */
    public EndpointCommandRegistry(@NotNull Endpoint endpoint) {
        this.endpoint = endpoint;
        this.logger = endpoint.getLogger();
        this.tokenRegistry = endpoint.getTokenRegistry();


    }

    @Override
    public void initialize() throws Exception {

        Bukkit.getScheduler().runTaskTimerAsynchronously(endpoint.getInstance(), () -> {
            staffOnline = 0;
            online = 0;
            Bukkit.getOnlinePlayers().forEach(player -> {
                Profile profile = endpoint.getInstance().getProfileRegistry().getProfile(player.getUniqueId());
                online++;
                if (profile.isStaff())
                    staffOnline++;
            });
        }, 0, 20 * 60);

        registerEndpointCommand(new EndpointCommand(endpoint) {
            @Override
            @EndpointGoods(value = "/", requiresToken = false)
            public byte[] execute(@NotNull Map<String, String> args,  @NotNull HttpExchange exchange) {

                JSONObject failed = new JSONObject();
                failed.put("type", "failed");

                byte[] response;
                try (InputStream str = getClass().getClassLoader().getResourceAsStream("endpoint/index.html")) {
                    response = IOUtils.readFully(str, str.available(), true);
                } catch (Exception e) {
                    failed.put("message", e.getMessage());
                    response = failed.toJSONString().getBytes();
                    e.printStackTrace();
                }

                return response;

            }
        });

        registerEndpointCommand(new EndpointCommand(endpoint) {
            @Override
            @EndpointGoods(value = "/getOnlineStaff", requiresToken = false)
            public byte[] execute(@NotNull Map<String, String> args, @NotNull HttpExchange exchange) {
                JSONObject response = new JSONObject();
                response.put("count", staffOnline);

                return response.toJSONString().getBytes();
            }
        });
        registerEndpointCommand(new EndpointCommand(endpoint) {
            @Override
            @EndpointGoods(value = "/getOnline", requiresToken = false)
            public byte[] execute(@NotNull Map<String, String> args, @NotNull HttpExchange exchange) {
                JSONObject response = new JSONObject();
                response.put("count", online);
                return response.toJSONString().getBytes();
            }
        });

        registerEndpointCommand(new EndpointCommand(endpoint) {
            @Override
            @EndpointGoods("/getCode")
            public byte[] execute(@NotNull Map<String, String> args, @NotNull HttpExchange exchange) {
                JSONObject response = new JSONObject();


                if(args.isEmpty() || !args.containsKey("code"))
                    return Endpoint.ResponseType.FAILED.toResponse("Lack of isValid arguments.");

                int code = 0;
                try {
                    code = Integer.valueOf(args.get("code"));
                } catch (NumberFormatException x) {
                    return Endpoint.ResponseType.FAILED.toResponse("Malformed Integer.");
                }


                PairRegistry registry = endpoint.getInstance().getPairRegistry();

                if(registry.isCodeRegistered(code)) {
                    response.put("code", code);
                    response.put("uuid", registry.getByCode(code));

                    return response.toJSONString().getBytes();
                } else
                    return Endpoint.ResponseType.FAILED.toResponse("Code does not exist.");
            }
        });
    }

    @Override
    public void terminate() throws Exception {

    }

    public void registerEndpointCommand(@NotNull EndpointCommand command) {
        EndpointGoods goods;
        try {
            goods = command.getClass().getMethod("execute", Map.class, HttpExchange.class).getAnnotation(EndpointGoods.class);
        } catch (NoSuchMethodException e) {
            logger.error("Endpoint command class %s is missing method execute", command.getClass().getSimpleName());
            return;
        }

        String name = goods.value();
        endpoint.getServer().createContext(name).setHandler((exchange) -> {

            Map<String, String> args = command.resolveArguments(exchange.getRequestURI());
            String token = args.get("token");
            if (goods.requiresToken()) {
                if (token == null || !tokenRegistry.isTokenRegistered(token)) {
                    byte[] response = Endpoint.ResponseType.FAILED.toResponse("Access denied");
                    exchange.sendResponseHeaders(200, response.length);
                    exchange.getResponseBody().write(response);
                    exchange.close();
                    return;
                }
            }

            byte[] response = command.execute(args, exchange);
            exchange.sendResponseHeaders(200, response.length);
            exchange.getResponseBody().write(response);
            exchange.close();
        });
    }


}
