package eu.Realmland.CruX.endpoint.commands.model;

import com.sun.net.httpserver.HttpExchange;
import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.endpoint.Endpoint;
import lombok.Getter;
import me.MrWener.RealmLandLoggerLib.Logger;
import org.jetbrains.annotations.NotNull;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

public abstract class EndpointCommand {

    @Getter
    private Endpoint endpoint;

    @Getter
    private CruX instance;

    @Getter
    private Logger logger;

    /**
     * Default constructor
     * @param endpoint Endpoint
     */
    public EndpointCommand(@NotNull Endpoint endpoint) {
        this.endpoint = endpoint;
        this.instance = endpoint.getInstance();
        this.logger = endpoint.getLogger();
    }


    public abstract byte[] execute(@NotNull Map<String, String> args, @NotNull HttpExchange exchange);

    public Map<String, String> resolveArguments(@NotNull URI uri) {
        final Map<String, String> args = new HashMap<>();

        String[] parts = uri.toString().split("\\?");
        if(parts.length > 1) {
            String[] arguments = parts[1].split("&");
            for (String argument : arguments) {
                String[] elements = argument.split("=");
                if(elements.length > 1) {
                    args.put(elements[0], elements[1]);
                } else if (elements.length == 1){
                    args.put(elements[0], "true");
                }
            }
        }
        return args;
    }
}
