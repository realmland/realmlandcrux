package eu.Realmland.CruX.endpoint;

import lombok.Getter;
import me.MrWener.RealmLandLoggerLib.Logger;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class TokenRegistry {

    @Getter
    private final Endpoint endpoint;
    private Logger logger;
    /**
     * All tokens
     */
    @Getter
    private final List<String> tokens = new ArrayList<>();

    private final String CHARACTERS = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789";
    private final char[] charArray = CHARACTERS.toCharArray();

    private final int TOKEN_LENGTH = 20;


    /**
     * Default configuration
     * @param endpoint Endpoint
     */
    public TokenRegistry(Endpoint endpoint) {
        this.endpoint = endpoint;
        this.logger = endpoint.getLogger();
    }


    /**
     * Generates token
     * @return Token
     */
    public String generateToken() {
        Random ran = new Random();
        StringBuilder token = new StringBuilder();
        for(int i = 0; i < TOKEN_LENGTH; i++) {
            int next = ran.nextInt(charArray.length);
            token.append(charArray[next]);
        }

        if(isTokenRegistered(token.toString()))
            return generateToken();
        return token.toString();
    }


    /**
     * Registers token
     * @param token Token
     * @return True if token is successfully registered, else false
     */
    public boolean registerToken(@NotNull String token) {
        if(isTokenRegistered(token))
            return false;
        tokens.add(token);
        return true;
    }

    /**
     * Unregisters token
     * @param token Token
     * @return True if token is successfully unregistered, else false
     */
    public boolean unregisterToken(@NotNull String token) {
        if(!isTokenRegistered(token))
            return false;
        tokens.remove(token);
        return true;
    }

    /**
     * Checks if token is registered
     * @param token Token
     * @return True if token's registered, else false
     */
    public boolean isTokenRegistered(@NotNull String token) {
        return tokens.contains(token);
    }

}
