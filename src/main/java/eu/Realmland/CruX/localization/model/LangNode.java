package eu.Realmland.CruX.localization.model;

import eu.Realmland.CruX.localization.Lang;
import eu.Realmland.CruX.localization.Localization;
import lombok.Getter;
import me.MrWener.RealmLandDatabase.payload.Payload;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

/**
 * Represents dynamic Message for more languages.
 */
public class LangNode {

    /**
     * Message in different languages
     */
    @Getter
    private Map<Lang, String> messageVarieties = new HashMap<>();

    @Getter
    private String key;

    @Getter
    private Localization.Prefix prefix = Localization.Prefix.NONE;

    @Getter
    private Localization.PlaceholderType placeholderType;

    @Getter
    private Localization.Type type = Localization.Type.TEXT;

    /**
     * Model from payload
     *
     * @param payload Payload of languages
     */
    public LangNode(@NotNull Payload payload) {
        this.prefix = Localization.Prefix.of(payload.getColumn("message_prefix"));
        this.placeholderType = Localization.PlaceholderType.of(payload.getColumn("placeholder_type"));
        this.key = payload.getColumn("message_key");
        this.type = Localization.Type.valueOf(payload.getColumn("message_type").toString().toUpperCase());

        if (placeholderType == null)
            placeholderType = Localization.PlaceholderType.JAVA_FORMAT;

        for (Lang lang : Lang.values()) {
            messageVarieties.put(lang, payload.getColumn(lang.getDatabaseName()));
        }
    }

    /**
     * Model from HashMap
     *
     * @param messageVarieties Message in different languages
     */
    public LangNode(@NotNull Map<Lang, String> messageVarieties, @Nullable Localization.Prefix prefix, @Nullable Localization.PlaceholderType placeholderType, @Nullable Localization.Type type) {
        if (prefix != null)
            this.prefix = prefix;
        if (type != null)
            this.type = type;
        if (placeholderType != null)
            this.placeholderType = placeholderType;

        this.messageVarieties = messageVarieties;
    }

    private LangNode(LangNode node) {
        messageVarieties = node.messageVarieties;
        key = node.key;
        prefix = node.prefix;
        type = node.type;
        placeholderType = node.placeholderType;
    }

    public static LangNode of(@NotNull String sk, @NotNull String cz, @NotNull String en) {
        HashMap<Lang, String> msgs = new HashMap<>();
        msgs.put(Lang.SK, sk);
        msgs.put(Lang.CZ, cz);
        msgs.put(Lang.EN, en);
        return new LangNode(msgs, null, null, null);
    }

    /**
     * Gets message in specified language. Returns null if nothing's found
     *
     * @param lang Language
     * @return Message in specified Language.
     */
    public String get(@NotNull Lang lang) {
        return get(lang, "");
    }

    /**
     * Gets message in specified language. Returns null if nothing's found. Supports format.
     *
     * @param lang Language
     * @return Message in specified Language.
     */
    public String get(@NotNull Lang lang, @NotNull Object... varargs) {
        String msg = messageVarieties.get(lang);
        if(msg.isEmpty())
            return "";

        if (placeholderType != null)
            if (placeholderType.equals(Localization.PlaceholderType.PLACEHOLDER_API))
                if (prefix != null)
                    // if the type is text, we want to put prefix in front of the message
                    return (type == Localization.Type.TEXT ? prefix.getPrefix().get(lang) : "") + msg;
                else
                    return msg;


        if (prefix != null)
            // if the type is text, we want to put prefix in front of the message
            return String.format((type == Localization.Type.TEXT ? prefix.getPrefix().get(lang) : "") + msg, varargs);
        else
            return String.format(msg, varargs);
    }

    /**
     * Sets prefix of LangNode
     *
     * @param prefix Prefix
     * @return This object
     */
    public LangNode setPrefix(@NotNull Localization.Prefix prefix) {
        this.prefix = prefix;
        return this;
    }

    /**
     * Sets type of placeholders
     *
     * @param type Type of placeholders
     * @return This object
     */
    public LangNode setPlaceholderType(@NotNull Localization.PlaceholderType type) {
        this.placeholderType = type;
        return this;
    }


    /**
     * Sets type of LangNode
     *
     * @param type Type
     * @return This object
     */
    public LangNode setType(@NotNull Localization.Type type) {
        this.type = type;
        return this;
    }

    @Override
    public LangNode clone() {
        return new LangNode(this);
    }
}
