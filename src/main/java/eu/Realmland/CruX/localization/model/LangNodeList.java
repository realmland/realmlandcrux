package eu.Realmland.CruX.localization.model;

import eu.Realmland.CruX.localization.Lang;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

public class LangNodeList {

    @Getter
    private Map<Lang, LangNode> nodes = new HashMap<>();


    public LangNode getNode(@NotNull Lang lang) {
        return nodes.get(lang);
    }

    public void addNode(@NotNull Lang lang, @NotNull LangNode node) {
        nodes.put(lang, node);
    }
}
