package eu.Realmland.CruX.localization;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.localization.model.LangNode;
import eu.Realmland.CruX.services.IService;
import eu.Realmland.CruX.ux.profiles.model.Profile;
import lombok.Getter;
import lombok.Setter;
import me.MrWener.RealmLandDatabase.Database;
import me.MrWener.RealmLandDatabase.payload.Payload;
import me.MrWener.RealmLandLoggerLib.Logger;
import me.clip.placeholderapi.PlaceholderAPI;
import net.minecraft.server.v1_14_R1.IChatBaseComponent;
import net.minecraft.server.v1_14_R1.PacketPlayOutChat;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.regex.Pattern;

/**
 * Localization
 */
public class Localization implements IService {

    private CruX instance;
    private Database database;

    private String table;
    private Logger logger;

    @Getter
    private static final Pattern STRIP_COLOR_PATTERN = Pattern.compile("(?i)[&§][0-9A-FK-OR]");


    private final HashMap<String, LangNode> messages = new HashMap<>();

    /**
     * Default constructor
     *
     * @param instance CruX Instance
     */
    public Localization(CruX instance, String table) {
        this.instance = instance;
        this.table = table;
        this.database = instance.getDatabase();
        this.logger = instance.logger();
    }

    @Override
    public void initialize() throws Exception {
        database.executeSQL("CREATE TABLE IF NOT EXISTS `" + table + "` (" +
                "  `message_key` varchar(64) NOT NULL PRIMARY KEY UNIQUE," +
                "  `placeholder_type` varchar(32) NOT NULL DEFAULT 'java_format'," +
                "  `message_prefix` varchar(32) NOT NULL," +
                "  `message_type` varchar(32) NOT NULL DEFAULT 'text'," +
                "  `lang_sk` text," +
                "  `lang_cz` text," +
                "  `lang_en` text" +
                ") ");
        loadMessages();
    }


    /**
     * Loads messages from Database.
     */
    public void loadMessages() {
        try {
            messages.clear();
            List<Payload> dbMessages = database.SQLSelect(table, getFields(), null);
            dbMessages.forEach(payload -> messages.put(payload.getColumn("message_key"), new LangNode(payload)));
        } catch (SQLException e) {
            logger.error("Failed to load messages from Database.", e);
        }

    }

    /**
     * Gets LangNode from Localization
     *
     * @param messageKey Key of message
     * @return LangNode... Null if not found.
     */
    public @Nullable LangNode get(@NotNull String messageKey) {
        // get message from cache
        if (instance.getConfig().getBoolean("generalLocalization.cache", true)) {
            return messages.get(messageKey);
        } else {
            // get message from database
            List<Payload> response = null;
            try {
                response = database.SQLSelect(table, getFields(), Payload.of("message_key", messageKey));
            } catch (SQLException e) {
                e.printStackTrace();
            }
            if (response != null && response.size() > 0) {
                Payload payload = response.get(0);
                return new LangNode(payload);
            } else {
                return null;
            }
        }
    }

    /**
     * Gets message from Localization.
     *
     * @param messageKey Key of message.
     * @param prefix     If specified, sets message's prefix to this argument
     * @param type       If specified, sets message's type to this argument
     * @param lang       Language message is going to be in.
     * @param varargs    Format arguments.
     * @return Localized message.
     */
    public @NotNull String get(@NotNull String messageKey, @Nullable Prefix prefix, @Nullable Type type, @NotNull Lang lang, @NotNull Object... varargs) {
        LangNode model = get(messageKey);

        if (model != null) {
            if (prefix != null)
                model.setPrefix(prefix);
            if (type != null)
                model.setType(type);
        }

        return (model != null ? model.get(lang, varargs) : Prefix.ERROR + "Failed to get message '" + messageKey + "'.");
    }

    /**
     * Gets message from Localization.
     *
     * @param messageKey Key of message.
     * @param lang       Language message is going to be in.
     * @param varargs    Format arguments.
     * @return Localized message.
     */
    public @NotNull String get(@NotNull String messageKey, @NotNull Lang lang, @NotNull Object... varargs) {
        return get(messageKey, null, null, lang, varargs);
    }


    /**
     * Gets message from Localization.
     *
     * @param messageKey Key of message.
     * @param prefix     If specified, sets message's prefix to this argument
     * @param type       If specified, sets message's type to this argument
     * @param lang       Language message is going to be in.
     * @return Localized message.
     */
    public @NotNull String get(@NotNull String messageKey, @Nullable Prefix prefix, @Nullable Type type, @NotNull Lang lang) {
        return get(messageKey, prefix, type, lang, "");
    }


    /**
     * Gets message from Localization.
     *
     * @param messageKey Key of message.
     * @param lang       Language message is going to be in.
     * @return Localized message.
     */
    public @NotNull String get(@NotNull String messageKey, @NotNull Lang lang) {
        return get(messageKey, null, null, lang);
    }


    /**
     * Broadcasts message to all players
     *
     * @param messageKey Key of message
     * @param sprefix    Prefix of message
     * @param stype      Type of message
     * @param varargs    Message format arguments
     */
    public void broadcast(@NotNull String messageKey, @Nullable Prefix sprefix, @Nullable Type stype, @Nullable OfflinePlayer papiPlayer, @NotNull Object... varargs) {

        LangNode node = get(messageKey);
        Type type;

        for (Player player : Bukkit.getOnlinePlayers()) {
            if (player == null)
                break;
            Profile profile = instance.getProfileRegistry().getProfile(player.getUniqueId());
            if (profile == null) {
                if (player.isOnline())
                    logger.warn("Profile for player '%s' is null", player.getName());

                break;
            }
            if (node != null) {

                type = node.getType();
                if (stype != null)
                    type = stype;
                if (sprefix != null)
                    node.setPrefix(sprefix);

                String msg = node.get(profile.getLang(), varargs);
                if (papiPlayer != null)
                    if (Bukkit.getPluginManager().isPluginEnabled("PlaceholderAPI"))
                        msg = PlaceholderAPI.setPlaceholders(papiPlayer, msg);
                    else
                        logger.warn("Papi player is null!");

                if (type.equals(Type.TEXT))
                    player.sendMessage(msg);
                else
                    ((CraftPlayer) player).getHandle().playerConnection.sendPacket(new PacketPlayOutChat(IChatBaseComponent.ChatSerializer.a(msg)));
            } else {
                logger.error("Uh Oh! Node %s is NULL! O_O", messageKey);
                break;
            }

        }
    }

    /**
     * Broadcasts message to all players
     *
     * @param messageKey Key of message
     * @param varargs    Message format arguments
     */
    public void broadcast(@NotNull String messageKey, @NotNull Object... varargs) {
        broadcast(messageKey, null, null, null, varargs);
    }


    /**
     * Broadcasts message to all players
     *
     * @param messageKey Key of message
     * @param sprefix    Prefix of message
     * @param stype      Type of message
     */
    public void broadcast(@NotNull String messageKey, @Nullable Prefix sprefix, @Nullable Type stype) {
        broadcast(messageKey, sprefix, stype, null, "");
    }


    /**
     * Broadcasts message to all players
     *
     * @param messageKey Key of message
     */
    public void broadcast(@NotNull String messageKey) {
        broadcast(messageKey, "");
    }

    /**
     * Broadcasts message to all players
     *
     * @param messageKey Key of message
     * @param player     Player
     */
    public void broadcast(@NotNull String messageKey, @NotNull OfflinePlayer player) {
        broadcast(messageKey, null, null, player, "");
    }

    /**
     * Message prefix
     */
    public enum Prefix {

        SUCCESS("§a# §f"), ERROR("§c# §f"), WARN("§e# §f"), INFO("§9# §f"), ACTION("§8* §7"), NONE("§f"),
        HIGHLIGHT("§e"), SENTENCE("§f"),
        HINT("§e§lRada §7", "§e§lRada §7", "§e§lHint §7"),
        DESC("§9§lPopis §7", "§9§lPopis §7", "§9§lDescription §7");

        @Getter
        @Setter
        private LangNode prefix;

        Prefix(String prefix) {
            this(prefix, prefix, prefix);
        }

        Prefix(@NotNull String skPrefix, @NotNull String czPrefix, @NotNull String enPrefix) {
            this.prefix = LangNode.of(skPrefix, czPrefix, enPrefix);
        }

        public String toString(Lang lang) {
            return getPrefix().get(lang);
        }

        @Override
        public String toString() {
            return getPrefix().get(Lang.EN);
        }

        /**
         * Matches Prefix
         *
         * @param value Name
         * @return Prefix matching with name
         */
        public static Prefix of(@NotNull String value) {
            for (Prefix prefix : values())
                if (prefix.getDatabaseName().equalsIgnoreCase(value.toLowerCase()))
                    return prefix;
            return NONE;
        }

        /**
         * @return Database column name.
         */
        public String getDatabaseName() {
            return name().toLowerCase();
        }

    }

    /**
     * Mesasge type
     */
    public enum Type {
        TEXT, JSON;

        public static Type of(@NotNull String text) {
            for (Type value : values()) {
                if (value.name().equalsIgnoreCase(text))
                    return value;
            }
            return null;
        }
    }

    public enum PlaceholderType {
        JAVA_FORMAT, PLACEHOLDER_API;

        public static PlaceholderType of(@NotNull String input) {
            for (PlaceholderType value : values()) {
                if (value.name().equalsIgnoreCase(input))
                    return value;
            }
            return null;
        }

    }

    /**
     * Strips input of color
     * @param string Input
     * @return Stripped String
     */
    public static String stripColor(@NotNull String string) {
        return STRIP_COLOR_PATTERN.matcher(string).replaceAll("");
    }


    public static String firstToUpper(@NotNull String input) {
        char[] array = input.toCharArray();
        if(array.length > 0) {
            char first = array[0];
            int code = (int) first;
            // if is lowercase character
            if(code > 96 && code < 123) {
                // http://www.asciitable.com
                // slide it 32 codes down
                code-=32;
            }
            array[0] = (char) code;
        }
        return  new String(array);
    }

    /**
     * Joins List elements with elegance
     * @param delimiter Delimiter
     * @param list      List to be joined
     * @return Joined List to String
     */
    public static String join(@NotNull String delimiter, @NotNull List<String> list) {
        StringBuilder joined = new StringBuilder();
        ListIterator<String> iterator = list.listIterator();
        while (iterator.hasNext()) {
            String line = iterator.next();
            if (iterator.hasNext())
                joined.append(line).append(delimiter);
            else
                joined.append(line);
        }
        return joined.toString();
    }

    /**
     * @return Message fields
     */
    private static List<String> getFields() {
        return Arrays.asList("message_key", "placeholder_type", "message_prefix", "message_type", "lang_sk", "lang_cz", "lang_en");
    }
}
