package eu.Realmland.CruX.localization;

import org.jetbrains.annotations.NotNull;

/**
 * Enum of Languages
 */
public enum Lang {
    SK, CZ, EN;

    /**
     * @return Database column name.
     */
    public String getDatabaseName() {
        return "lang_" + getConfigName();
    }

    /**
     * @return Config name.
     */
    public String getConfigName() {
        return name().toLowerCase();
    }

    @Override
    public String toString() {
        return getDatabaseName();
    }

    /**
     * @param name Name of Language
     * @return Matching Language
     */
    public static Lang of(@NotNull String name) {
        for (Lang lang : values())
            if (lang.getDatabaseName().equalsIgnoreCase(name) || lang.name().toLowerCase().equalsIgnoreCase(name.toLowerCase()))
                return lang;

        return null;
    }

    /**
     * Checks if Language with specified name exists
     *
     * @param name Name of lang
     * @return Boolean
     */
    public static boolean isLang(@NotNull String name) {
        for (Lang lang : values())
            if (lang.getConfigName().equalsIgnoreCase(name) || lang.getDatabaseName().equalsIgnoreCase(name))
                return true;
        return false;
    }

}
