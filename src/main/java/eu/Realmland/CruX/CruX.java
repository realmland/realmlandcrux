package eu.Realmland.CruX;

import com.zaxxer.hikari.HikariConfig;
import eu.Realmland.CruX.area.AreaRegistry;
import eu.Realmland.CruX.control.ControlCenter;
import eu.Realmland.CruX.endpoint.Endpoint;
import eu.Realmland.CruX.localization.Localization;
import eu.Realmland.CruX.ui.commands.CommandRegistry;
import eu.Realmland.CruX.ux.bank.BankRegistry;
import eu.Realmland.CruX.ux.pairer.PairRegistry;
import eu.Realmland.CruX.ux.profiles.ProfileRegistry;
import eu.Realmland.CruX.ux.quests.QuestRegistry;
import eu.Realmland.CruX.ux.ranks.RankRegistry;
import lombok.Getter;
import me.MrWener.RealmLandConfigurationsLib.Configuration;
import me.MrWener.RealmLandDatabase.Database;
import me.MrWener.RealmLandDatabase.protocols.MySQL;
import me.MrWener.RealmLandLoggerLib.Logger;
import me.clip.placeholderapi.PlaceholderAPI;
import me.clip.placeholderapi.PlaceholderAPIPlugin;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public class CruX extends JavaPlugin {

    private static CruX instance;

    {
        instance = this;
    }

    // Logger
    private Logger logger;

    // Database
    @Getter
    private Database database;

    protected boolean running = true;

    private Configuration config;

    @Getter
    private Localization generalLocalization;

    @Getter
    private Localization commandsLocalization;

    @Getter
    private CommandRegistry commandRegistry;

    @Getter
    private ProfileRegistry profileRegistry;

    @Getter
    private RankRegistry rankRegistry;

    @Getter
    private QuestRegistry questRegistry;

    @Getter
    private BankRegistry bankRegistry;

    @Getter
    private ControlCenter controlCenter;

    @Getter
    private AreaRegistry areaRegistry;

    @Getter
    private PairRegistry pairRegistry;


    @Getter
    private Endpoint endpoint;

    @Override
    public void onLoad() {
        super.onLoad();

        logger = new Logger("RealmLandCrux", new ChatColor[]{ChatColor.GOLD});

        try {
            config = new Configuration(this, "config", true);
        } catch (IOException e) {
            logger.error("Failed to construct Configuration", e);
        }

        try {

            ConfigurationSection section = config.getConfig().getConfigurationSection("database");
            if (section != null) {

                String name = section.getString("username");
                String pass = section.getString("password");

                String host = section.getString("host");
                String db = section.getString("database");

                int port = section.getInt("port", -1);
                int poolSize = section.getInt("pool-size", -1);

                // use default port
                if (port == -1)
                    poolSize = 3306;

                // use default pool size
                if (poolSize == -1)
                    poolSize = 10;

                logger.debug("Database using port §f%d", port);
                logger.debug("Database using max pool size §f%d", poolSize);

                // if nothing's null
                if ((
                        name != null)
                        || pass != null
                        || host != null
                        || db != null)
                database = new Database(new MySQL(host, db, name, pass, port));

                HikariConfig config = database.getProtocol().getConfiguration();
                config.setMaximumPoolSize(poolSize);
                database.getProtocol().setConfiguration(config);
            } else
                logger.error("Failed to find section 'database'");

        } catch (Exception e) {
            logger.error("Failed to construct Database", e);

            running = false;
        }


        // if is not null, check if connection is stable. If its not running = false; if database is null: running = false;
        if(database != null) {
            logger.debug((database.isConnectionStable() ? "Connection to database §astable" : "Connection to database §cunstable"));

            if(!database.isConnectionStable())
                running = false;
        } else
            running = false;

        // construct other things if everything is alright...
        if(running) {
            database.setupActiveConnection();

            try {
                generalLocalization = new Localization(this, "localization");
                commandsLocalization = new Localization(this, "localization_commands");
            } catch (Exception x) {
                logger.error("Failed to construct Localization", x);
            }

            try {
                commandRegistry = new CommandRegistry(this);
            } catch (Exception x) {
                logger.error("Failed to construct CommandRegistry", x);
            }


            try {
                profileRegistry = new ProfileRegistry(this);
            } catch (Exception x) {
                logger.error("Failed to construct ProfileRegistry", x);
            }

            try {
                rankRegistry = new RankRegistry(this);
            } catch (Exception x) {
                logger.error("Failed to construct RankRegistry", x);
            }

            try {
                questRegistry = new QuestRegistry(this);
            } catch (Exception x) {
                logger.error("Failed to construct QuestRegistry", x);
            }

            try {
                bankRegistry = new BankRegistry(this);
            } catch (Exception x) {
                logger.error("Failed to construct BankRegistry", x);
            }

            try {
                controlCenter = new ControlCenter(this);
            } catch (Exception x) {
                logger.error("Failed to construct ControlCenter", x);
            }

            try {
                areaRegistry = new AreaRegistry(this);
            } catch (Exception x) {
                logger.error("Failed to construct AreaRegistry", x);
            }

            try {
                pairRegistry = new PairRegistry(this);
            } catch (Exception x) {
                logger.error("Failed to construct PairRegistry", x);
            }

            try {
                endpoint = new Endpoint(this);
            } catch (Exception x) {
                logger.error("Failed to construct Endpoint", x);
            }


        }
    }

    @Override
    public void onEnable() {
        super.onEnable();

        if(!running) {
            logger.warn("Disabling plugin due critical failure, read more above!");
            Bukkit.getPluginManager().disablePlugin(this);
            return;
        }

        if(Bukkit.getPluginManager().isPluginEnabled(PlaceholderAPIPlugin.getInstance())) {
            logger.info("Registering placeholder expansion...");
            PlaceholderAPI.registerExpansion(new PlaceholderExtension(this));
        }

        Bukkit.getPluginManager().addPermission(new Permission("realmland.premission.color", PermissionDefault.OP));

        try {
            generalLocalization.initialize();
            commandsLocalization.initialize();
        } catch (Exception x) {
            logger.error("Failed to initialize Localization", x);
        }

        try {
            commandRegistry.initialize();
        } catch (Exception e) {
            logger.error("Failed to initialize %s!", e, commandRegistry.getClass().getSimpleName());
        }


        try {
            profileRegistry.initialize();
        } catch (Exception x) {
            logger.error("Failed to initialize ProfileRegistry", x);
        }

        try {
            rankRegistry.initialize();
        } catch (Exception x) {
            logger.error("Failed to initialize RankRegistry", x);
        }

        try {
            questRegistry.initialize();
        } catch (Exception x) {
            logger.error("Failed to initialize QuestRegistry", x);
        }

        try {
            bankRegistry.initialize();
        } catch (Exception x) {
            logger.error("Failed to initialize BankRegistry", x);
        }

        try {
            controlCenter.initialize();
        } catch (Exception x) {
            logger.error("Failed to initialize ControlCenter", x);
        }

        try {
            areaRegistry.initialize();
        } catch (Exception x) {
            logger.error("Failed to initialize AreaRegistry", x);
        }

        try {
            endpoint.initialize();
            logger.info("Endpoint running on " + endpoint.getServer().getAddress());
        } catch (Exception x) {
            logger.error("Failed to initialize Endpoint", x);
        }
    }

    @Override
    public void onDisable() {
        super.onDisable();

        if(!running) {
            // just try to close it, just in case
            if(database != null) {
                try {
                    database.close();
                } catch (IOException ignored) {}
            }
            return;
        }

        try {
            endpoint.terminate();
        } catch (Exception x) {
            logger.error("Failed to terminate Endpoint", x);
        }

        try {
            areaRegistry.terminate();
        } catch (Exception x) {
            logger.error("Failed to terminate AreaRegistry", x);
        }

        try {
            controlCenter.terminate();
        } catch (Exception x) {
            logger.error("Failed to terminate ControlCenter", x);
        }

        try {
            bankRegistry.terminate();
        } catch (Exception x) {
            logger.error("Failed to terminate BankRegistry", x);
        }

        try {
            questRegistry.terminate();
        } catch (Exception x) {
            logger.error("Failed to terminate QuestRegistry", x);
        }


        try {
            rankRegistry.terminate();
        } catch (Exception x) {
            logger.error("Failed to terminate RankRegistry", x);
        }

        try {
            profileRegistry.terminate();
        } catch (Exception x) {
            logger.error("Failed to terminate ProfileRegistry", x);
        }


        try {
            commandRegistry.terminate();
        } catch (Exception e) {
            logger.error("Failed to terminate %s!", e, commandRegistry.getClass().getSimpleName());
        }

        try {
            generalLocalization.terminate();
            commandsLocalization.terminate();
        } catch (Exception x) {
            logger.error("Failed to terminate Localization", x);
        }

        // CLOSE THAT SUCKER
        if(database != null) {
            try {
                database.close();
            } catch (IOException ignored) {}
        }
    }

    protected static CruX get() {
        return instance;
    }

    @Override
    public void reloadConfig() {
        this.config.load();
    }

    @NotNull
    @Override
    public FileConfiguration getConfig() {
        return config.getConfig();
    }

    /**
     * @return Logger class for plugin.
     */
    public Logger logger() {
        return logger;
    }

}
