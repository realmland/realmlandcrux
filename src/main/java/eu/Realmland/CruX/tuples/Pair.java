package eu.Realmland.CruX.tuples;

import lombok.Getter;
import lombok.Setter;

import javax.annotation.Nullable;

public class Pair<FirstType, SecondType> {

    @Getter
    @Setter
    private FirstType first;
    @Getter
    @Setter
    private SecondType second;

    /**
     * Default constructor of Pair
     *
     * @param first  First value
     * @param second Second value
     */
    public Pair(@Nullable FirstType first, @Nullable SecondType second) {
        this.first = first;
        this.second = second;
    }

    /**
     * Checks if both elements are set
     *
     * @return Boolean
     */
    public boolean isValid() {
        return isFirstSet() && isSecondSet();
    }

    /**
     * Checks if first element is NULL
     *
     * @return Boolean
     */
    public boolean isFirstSet() {
        return first != null;
    }

    /**
     * Checks if second element is NULL
     *
     * @return Boolean
     */
    public boolean isSecondSet() {
        return second != null;
    }

    @Override
    public String toString() {
        return "[FirstValue = " + (getFirst() == null ? "null" : getFirst().toString()) + ", SecondValue = " + (getSecond() == null ? "null" : getSecond().toString()) + "]";
    }
}
