package eu.Realmland.CruX.control.scheduler;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.control.scheduler.model.Notification;
import eu.Realmland.CruX.localization.Localization;
import eu.Realmland.CruX.localization.model.LangNode;
import eu.Realmland.CruX.services.IService;
import eu.Realmland.CruX.statics.ConfigStatics;
import lombok.Getter;
import me.MrWener.RealmLandConfigurationsLib.Configuration;
import me.MrWener.RealmLandLoggerLib.Logger;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RealmScheduler implements IService {

    private @NotNull CruX instance;
    private @NotNull Logger logger;

    @Getter
    @NotNull
    private Configuration schedulerConfiguration;

    @Getter
    private Map<Integer, Notification> notifications = new HashMap<>();

    /**
     * Default constructor
     *
     * @param instance
     */
    public RealmScheduler(@NotNull CruX instance) {
        this.instance = instance;
        this.logger = instance.logger();

        try {
            schedulerConfiguration = new Configuration(instance, "localization/scheduler/scheduler.yml", true);
        } catch (IOException e) {
            logger.error("Failed to construct scheduler configuration", e);
        }
    }

    @Override
    public void initialize() throws Exception {

        ConfigurationSection notCat = getConfig().getConfigurationSection("scheduled-notifications");
        if (notCat == null) {
            logger.error("Failed to get configuration section for notifications!");
            return;
        }

        List<String> notifications = new ArrayList<>(notCat.getKeys(false));
        notifications.forEach(notif -> {

            int time = notCat.getInt(notif + ".time");
            LangNode node = ConfigStatics.getLangNodeFromString(notCat.getConfigurationSection(notif + ".notification"));
            node.setType(Localization.Type.JSON);

            Notification n = new Notification(time, node);
            int task = Bukkit.getScheduler().runTaskTimerAsynchronously(instance, n::broadcast, 0, time * 20 * 60).getTaskId();
            this.notifications.put(task, n);
            logger.debug("New notification w/ id %d registered... Repeating every %d min", task, time);
        });

    }

    @Override
    public void terminate() throws Exception {

    }

    public FileConfiguration getConfig() {
        return getSchedulerConfiguration().getConfig();
    }
}
