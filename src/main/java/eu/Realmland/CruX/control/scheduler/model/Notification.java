package eu.Realmland.CruX.control.scheduler.model;

import eu.Realmland.CruX.CruXStatics;
import eu.Realmland.CruX.localization.model.LangNode;
import eu.Realmland.CruX.ux.profiles.model.Profile;
import lombok.Getter;
import net.minecraft.server.v1_14_R1.IChatBaseComponent;
import net.minecraft.server.v1_14_R1.PacketPlayOutChat;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftPlayer;
import org.jetbrains.annotations.NotNull;

public class Notification {

    @Getter
    private int timer;

    @Getter
    private LangNode payload;

    /**
     * Default constructor
     *
     * @param timer   Time in minutes which tells broadcaster how often to broadcast this message
     * @param payload Human-comprehensible message
     */
    public Notification(int timer, @NotNull LangNode payload) {
        this.timer = timer;
        this.payload = payload;
    }

    public void broadcast() {
        Bukkit.getOnlinePlayers().forEach(player -> {
            Profile profile = CruXStatics.getInstance().getProfileRegistry().getProfile(player.getUniqueId());
            if (profile != null)
                if (!CruXStatics.getInstance().getControlCenter().getAfkService().isPlayerAfk(profile.getUuid()))
                    if (!profile.getProfileData().hasSetting("ignore_notifications"))
                        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(new PacketPlayOutChat(IChatBaseComponent.ChatSerializer.a(payload.get(profile.getLang()))));

        });
    }
}
