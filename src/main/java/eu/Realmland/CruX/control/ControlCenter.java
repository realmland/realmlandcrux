package eu.Realmland.CruX.control;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.control.fun.Sitting;
import eu.Realmland.CruX.control.scheduler.RealmScheduler;
import eu.Realmland.CruX.control.services.AFKService;
import eu.Realmland.CruX.control.services.ChatService;
import eu.Realmland.CruX.control.services.InterfaceService;
import eu.Realmland.CruX.control.services.MuteService;
import eu.Realmland.CruX.localization.Localization;
import eu.Realmland.CruX.services.IService;
import lombok.Getter;
import me.MrWener.RealmLandLoggerLib.Logger;
import org.jetbrains.annotations.NotNull;

public class ControlCenter implements IService {

    private @NotNull CruX instance;
    private Localization localization;
    private Logger logger;

    @Getter
    private MuteService muteService;

    @Getter
    private ChatService chatService;

    @Getter
    private AFKService afkService;

    @Getter
    private RealmScheduler scheduler;

    @Getter
    private InterfaceService interfaceService;

    @Getter
    private Sitting sitting;

    /**
     * Default constructor
     * @param instance Instance
     */
    public ControlCenter(@NotNull CruX instance) {
        this.instance = instance;
        this.localization = instance.getGeneralLocalization();
        this.logger = instance.logger();

        // At the testing, i forgot to construct it. Daily Cunt#1 3.8.2019 15:45 Saturday
        muteService = new MuteService();

        // At the testing, i forgot to construct it. Daily Cunt#2 3.8.2019 19:12 Saturday
        chatService = new ChatService(instance, muteService);

        afkService = new AFKService(instance);

        interfaceService = new InterfaceService(instance);

        sitting = new Sitting(instance);

        try {
            scheduler = new RealmScheduler(instance);
        } catch (Exception x) {
            logger.error("Failed to construct RealmScheduler", x);
        }
    }

    @Override
    public void initialize() throws Exception {
        // It is pointless to save mutes to DB. Yet.

        chatService.initialize();
        afkService.initialize();
        interfaceService.initialize();
        sitting.initialize();

        try {
            scheduler.initialize();
        } catch (Exception x) {
            logger.error("Failed to initialize RealmScheduler", x);
        }
    }

    @Override
    public void terminate() throws Exception {

        try {
            scheduler.terminate();
        } catch (Exception x) {
            logger.error("Failed to terminate RealmScheduler", x);
        }
        sitting.terminate();
        interfaceService.terminate();
        chatService.terminate();
        afkService.terminate();
    }

}
