package eu.Realmland.CruX.control.spigot.commands;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.control.services.AFKService;
import eu.Realmland.CruX.ui.commands.model.Command;
import eu.Realmland.CruX.ui.commands.model.CommandGoods;
import eu.Realmland.CruX.ux.profiles.model.Profile;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionDefault;
import org.jetbrains.annotations.NotNull;

public class AFKCommand extends Command {

    @CommandGoods(name = "afk", permission = "realmland.player.afk", aliases = {"away"}, permissionDefault = PermissionDefault.TRUE)
    public AFKCommand(@NotNull CruX instance) {
        super(instance);
    }

    @Override
    public boolean executeCommand(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) {
        Profile profile = requireOnlyPlayer(sender);
        if(profile == null)
            return true;

        Player player = (Player) sender;

        AFKService service = instance.getControlCenter().getAfkService();
        if(!service.isPlayerAfk(player.getUniqueId())) {
            service.addAfkPlayer(player.getUniqueId());
        } else {
            service.removeAfkPlayer(player.getUniqueId());
        }
        return true;
    }
}
