package eu.Realmland.CruX.control.spigot.commands;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.control.ControlCenter;
import eu.Realmland.CruX.localization.Localization;
import eu.Realmland.CruX.ui.commands.model.Command;
import eu.Realmland.CruX.ui.commands.model.CommandGoods;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MuteCommand extends Command {

    private ControlCenter controlCenter;

    @CommandGoods(name = "mute", aliases = {"unmute"}, permission = "realmland.admin.mute")
    public MuteCommand(@NotNull CruX instance) {
        super(instance);
        this.controlCenter = instance.getControlCenter();
    }

    @Override
    public boolean executeCommand(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) {

        if (args.length > 0) {
            if (args[0].equalsIgnoreCase("blacklistBegger")) {
                if (args.length > 1) {
                    String name = args[1];
                    OfflinePlayer player = Bukkit.getOfflinePlayer(name);
                    if (controlCenter.getMuteService().isBeggerBlacklisted(player.getUniqueId())) {
                        sender.sendMessage(Localization.Prefix.ERROR + "Player's already blacklisted!");
                        return true;
                    }
                    controlCenter.getMuteService().blacklistBegger(player.getUniqueId());
                    sender.sendMessage(Localization.Prefix.SUCCESS + "Player's now blacklisted.");
                    return true;
                } else {
                    sender.sendMessage(Localization.Prefix.ERROR + "Specify player's name.");
                    return true;
                }
            } else if (args[0].equalsIgnoreCase("whitelistBegger")) {
                if (args.length > 1) {
                    String name = args[1];
                    OfflinePlayer player = Bukkit.getOfflinePlayer(name);
                    if (!controlCenter.getMuteService().isBeggerBlacklisted(player.getUniqueId())) {
                        sender.sendMessage(Localization.Prefix.ERROR + "Player's not blacklisted!");
                        return true;
                    }
                    controlCenter.getMuteService().whitelistBegger(player.getUniqueId());
                    sender.sendMessage(Localization.Prefix.SUCCESS + "Player's now whitelisted.");
                    return true;
                } else {
                    sender.sendMessage(Localization.Prefix.ERROR + "Specify player's name.");
                    return true;
                }
            }

            String playerName = args[0];
            OfflinePlayer player = Bukkit.getOfflinePlayer(playerName);

            if (player == null) {
                sender.sendMessage(Localization.Prefix.ERROR + "I don't know about any player named '" + player.getName() + "'.");
                return true;
            }

            if (alias.equalsIgnoreCase("mute")) {
                if (!controlCenter.getMuteService().isMuted(player.getUniqueId())) {
                    controlCenter.getMuteService().mute(player.getUniqueId());

                    // broadcast
                    if (instance.getConfig().getBoolean("justice.announce-mute", true))
                        generalLocalization.broadcast("mute_announcement", playerName);

                    sender.sendMessage(Localization.Prefix.SUCCESS + "Muted player '" + playerName + "'.");
                    return true;
                } else {
                    sender.sendMessage(Localization.Prefix.ERROR + "This player's already muted");
                    return true;
                }
            } else {
                if (controlCenter.getMuteService().isMuted(player.getUniqueId())) {
                    // unmute
                    controlCenter.getMuteService().unmute(player.getUniqueId());
                    // whitelist from begging
                    if (controlCenter.getMuteService().isBeggerBlacklisted(player.getUniqueId()))
                        controlCenter.getMuteService().whitelistBegger(player.getUniqueId());

                    String name = player.getName();

                    // broadcast
                    if (instance.getConfig().getBoolean("justice.announce-mute", true))
                        generalLocalization.broadcast("unmute_announcement", playerName);

                    sender.sendMessage(Localization.Prefix.SUCCESS + "Unmuted player '" + playerName + "'.");

                    return true;
                } else {
                    sender.sendMessage(Localization.Prefix.ERROR + "This player's not muted");
                    return true;
                }
            }
        }
        sender.sendMessage(Localization.Prefix.ERROR + "Specify player's name!");
        return true;
    }

    @Override
    public List<String> executeTabComplete(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) {
        if (args.length > 1) {
            String arg = args[0];
            if (arg.equalsIgnoreCase("blacklistBegger")) {
                return controlCenter.getMuteService().getMutedPlayers().stream().map(Bukkit::getOfflinePlayer).map(OfflinePlayer::getName).collect(Collectors.toList());
            }
            if (arg.equalsIgnoreCase("whitelistBeggers")) {
                return controlCenter.getMuteService().getBlacklistedBeggers().stream().map(Bukkit::getOfflinePlayer).map(OfflinePlayer::getName).collect(Collectors.toList());
            }
            return new ArrayList<>();
        }
        if (alias.equalsIgnoreCase("unmute")) {
            List<String> players = controlCenter.getMuteService().getMutedPlayers().stream().map(entry -> Bukkit.getOfflinePlayer(entry).getName()).collect(Collectors.toList());
            players.add("blacklistBegger");
            players.add("whitelistBegger");
            return players;
        }

        List<String> players = Bukkit.getOnlinePlayers().stream().map(Player::getName).collect(Collectors.toList());
        players.add("blacklistBegger");
        players.add("whitelistBegger");
        return players;
    }
}
