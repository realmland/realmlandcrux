package eu.Realmland.CruX.control.spigot.commands;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.control.ControlCenter;
import eu.Realmland.CruX.localization.Localization;
import eu.Realmland.CruX.ui.commands.model.Command;
import eu.Realmland.CruX.ui.commands.model.CommandGoods;
import org.bukkit.command.CommandSender;
import org.bukkit.permissions.PermissionDefault;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;

public class ReportCommand extends Command {

    private ControlCenter controlCenter;

    @CommandGoods(name = "report", permission = "realmland.player.report",  permissionDefault = PermissionDefault.TRUE)
    public ReportCommand(@NotNull CruX instance) {
        super(instance);
        this.controlCenter = instance.getControlCenter();
    }

    @Override
    public boolean executeCommand(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) {
        sender.sendMessage(Localization.Prefix.ERROR + "Not ready yet.");
        return true;
    }

    @Override
    public List<String> executeTabComplete(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) {
        return Arrays.asList();
    }
}
