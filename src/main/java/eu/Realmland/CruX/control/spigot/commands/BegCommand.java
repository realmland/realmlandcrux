package eu.Realmland.CruX.control.spigot.commands;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.control.ControlCenter;
import eu.Realmland.CruX.ui.commands.model.Command;
import eu.Realmland.CruX.ui.commands.model.CommandGoods;
import eu.Realmland.CruX.ux.profiles.model.Profile;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionDefault;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class BegCommand extends Command {

    private ControlCenter controlCenter;

    @CommandGoods(name = "beg", aliases = {"please"}, permission = "realmland.player.beg",  permissionDefault = PermissionDefault.TRUE)
    public BegCommand(@NotNull CruX instance) {
        super(instance);
        this.controlCenter = instance.getControlCenter();
    }

    @Override
    public boolean executeCommand(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) {

        Profile profile = requireOnlyPlayer(sender);
        if (profile == null)
            return true;
        Player player = (Player) sender;

        // if not muted
        if (!controlCenter.getMuteService().isMuted(profile.getUuid())) {
            sender.sendMessage(generalLocalization.get("not_muted", profile.getLang()));
            return true;
        }

        // if on blacklist
        if (controlCenter.getMuteService().isBeggerBlacklisted(profile.getUuid())) {
            sender.sendMessage(generalLocalization.get("beg_blacklisted", profile.getLang()));
            return true;
        }

        long timeout = instance.getConfig().getInt("justice.beg-timeout", 20) * 1000;
        if (controlCenter.getMuteService().getBegTimeout().containsKey(profile.getUuid())) {
            long last = controlCenter.getMuteService().getBegTimeout().get(profile.getUuid());
            long calc = (System.currentTimeMillis() - last);

            if (!(calc >= timeout)) {
                sender.sendMessage(generalLocalization.get("beg_timeout", profile.getLang(), (double) ((calc - timeout) / 1000d * -1d)));
                return true;
            }
        }

        if(args.length == 0) {
            sender.sendMessage(generalLocalization.get("specify_message", profile.getLang())
            );
            return true;
        }

        final StringBuilder builder = new StringBuilder();
        for (String arg : args) {
            builder.append(arg + " ");
        }

        AtomicInteger sent = new AtomicInteger();

        instance.getProfileRegistry().getProfiles().forEach((uuid, onlineProfile) -> {
            if (onlineProfile.isStaff()) {
                sent.addAndGet(1);

                Bukkit.getPlayer(uuid).sendMessage(generalLocalization.get("player_begs", onlineProfile.getLang(), player.getName(), builder.toString().trim()));
            }
        });
        if(sent.get() == 0) {
            sender.sendMessage(generalLocalization.get("beg_not_sent", profile.getLang()));
            return true;
        }
        controlCenter.getMuteService().getBegTimeout().put(profile.getUuid(), System.currentTimeMillis());
        return true;
    }


    @Override
    public List<String> executeTabComplete(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) {
        return new ArrayList<>();
    }
}
