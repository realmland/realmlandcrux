package eu.Realmland.CruX.control.spigot.commands;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.localization.Localization;
import eu.Realmland.CruX.ui.commands.model.Command;
import eu.Realmland.CruX.ui.commands.model.CommandGoods;
import eu.Realmland.CruX.ux.profiles.model.Profile;
import me.clip.placeholderapi.PlaceholderAPI;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.chat.ComponentSerializer;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class StaffChatCommand extends Command {

    @CommandGoods(name = "staffchat", aliases = {"sc", "adminchat"}, permission = "realmland.admin.staff-chat")
    public StaffChatCommand(@NotNull CruX instance) {
        super(instance);
    }

    @Override
    public boolean executeCommand(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) {
        if(!isPlayer) {
            String format = instance.getConfig().getString("staff-chat-config.console-format");
            handleMessage(args, sender, format);
            return true;
        }

        String format = instance.getConfig().getString("staff-chat-config.player-format");

        format = PlaceholderAPI.setPlaceholders((Player) sender, format);

        handleMessage(args, sender, format);
        return true;
    }

    private void handleMessage(String[] args, CommandSender sender, String format) {
        StringBuilder message = new StringBuilder();
        if(args.length > 0) {
            for(int i = 0; i < args.length; i++)
                message.append(args[i]).append(" ");

            String msg = message.toString().replace("\"", "\\\\\"");
            System.out.println(msg);

            format = String.format(format, msg);
            broadcastToStaff(format);
        } else
            sender.sendMessage(Localization.Prefix.ERROR + "Specify message.");
    }


    private void broadcastToStaff(@NotNull String format) {
        TextComponent component = new TextComponent("§7(StaffChat) ");
        for (BaseComponent baseComponent : ComponentSerializer.parse(format))
            component.addExtra(baseComponent);


        Bukkit.getConsoleSender().spigot().sendMessage(component);
        Bukkit.getOnlinePlayers().forEach(player -> {
            Profile profile = requireOnlyPlayer(player);
            if(profile.isStaff())
                player.spigot().sendMessage(component);

        });
    }
}
