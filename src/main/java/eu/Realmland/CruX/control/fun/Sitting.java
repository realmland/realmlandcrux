package eu.Realmland.CruX.control.fun;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.services.IService;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.data.Bisected;
import org.bukkit.block.data.type.Stairs;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;
import org.spigotmc.event.entity.EntityDismountEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Sitting implements Listener, IService {

    @NotNull
    private CruX instance;

    @Getter
    private List<UUID> entites = new ArrayList<>();

    public Sitting(@NotNull CruX instance) {
        this.instance = instance;
    }

    @Override
    public void initialize() throws Exception {
        Bukkit.getPluginManager().registerEvents(this, instance);
    }

    @Override
    public void terminate() throws Exception {
        entites.forEach(uuid -> {
            try {
                Bukkit.getEntity(uuid).remove();
            } catch (NullPointerException ignored) {
            }
        });
        entites.clear();
    }

    @EventHandler
    public void onClick(PlayerInteractEvent e) {
        Block b = e.getClickedBlock();
        if (b != null)
            if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK))
                if (e.getPlayer().getInventory().getItemInMainHand().getType().equals(Material.AIR) && e.getPlayer().getInventory().getItemInOffHand().getType().equals(Material.AIR))
                    if (b.getType().name().contains("_STAIRS"))
                        sit(b, e.getPlayer());
    }

    @EventHandler
    public void dismount(EntityDismountEvent e) {
        if (e.getEntity() instanceof Player)
            if (entites.contains(e.getDismounted().getUniqueId())) {
                entites.remove(e.getDismounted().getUniqueId());
                e.getDismounted().remove();

                e.getEntity().setVelocity(new Vector(0, .5, 0));
            }

    }

    public void sit(@NotNull Block block, @NotNull Player player) {
        Stairs stairs = (Stairs) block.getBlockData();
        if (stairs.getHalf() == Bisected.Half.BOTTOM) {
            // gotta flip dat boy so he isnt facing goddamn wall
            int yawBase = 0;

            Vector direction = ((Stairs) block.getBlockData()).getFacing().getDirection();
            boolean isInner = stairs.getShape().name().contains("INNER");
            if (!isInner) {
                // get block faced by the stairs. if the area isnt empty... yeet that boy
                Block frontBlock = block.getWorld().getBlockAt(block.getLocation().add(direction.multiply(new Vector(-1, -1, -1))));
                if (!frontBlock.getType().equals(Material.AIR))
                    return;
            } else {
                System.out.println(direction);
            }

            // get the top block, if the area isn't empty... yeet that boyo
            Block topBlock = block.getWorld().getBlockAt(block.getLocation().add(new Vector(0, 1, 0)));
            if (!topBlock.getType().equals(Material.AIR))
                return;


            if (direction.getZ() == 1)
                yawBase = -180;
                // we really dont have to do that, but just for the future developer, so we can save him from killing himself
                // after all he lived trough...
            else if (direction.getZ() == -1)
                yawBase = 0;
            else if (direction.getX() == 1)
                yawBase = 90;
            else if (direction.getX() == -1)
                yawBase = -90;
            else if (direction.getX() == 1)
                yawBase = 90;
            else
                return;

            if (isInner)
                yawBase += 45;


            Vector center = block.getBoundingBox().getCenter();
            center.setY(center.getY() - 0.95D);

            Location loc = center.toLocation(player.getWorld());


            loc.setYaw(yawBase);
            ArmorStand armorStand = (ArmorStand) block.getWorld().spawnEntity(
                    loc, EntityType.ARMOR_STAND);

            armorStand.setSmall(true);
            armorStand.setGravity(false);
            armorStand.addPassenger(player);
            armorStand.setVisible(false);

            entites.add(armorStand.getUniqueId());
        }

    }
}
