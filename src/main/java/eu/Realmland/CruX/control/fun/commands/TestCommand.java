package eu.Realmland.CruX.control.fun.commands;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.localization.Localization;
import eu.Realmland.CruX.ui.commands.model.Command;
import eu.Realmland.CruX.ui.commands.model.CommandGoods;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftParrot;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Parrot;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.permissions.PermissionDefault;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TestCommand extends Command implements Listener {

    private List<Thread> threads = new ArrayList<>();
    private List<UUID> waiting = new ArrayList<>();

    @CommandGoods(name = "test", aliases = {""}, permission = "realmland.test", permissionDefault = PermissionDefault.OP)
    public TestCommand(@NotNull CruX instance) {
        super(instance);
        Bukkit.getPluginManager().registerEvents(this, instance);
    }

    @Override
    public boolean executeCommand(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) {
        ((CraftPlayer) sender).getHandle().n();
        waiting.add(((Player) sender).getUniqueId());

        if(args.length > 0) {
            threads.forEach(Thread::interrupt);
            threads.clear();
            waiting.clear();
            sender.sendMessage(Localization.Prefix.SUCCESS + "Cleared.");
        }
        return true;
    }

    @EventHandler
    public void selectEntity(PlayerInteractEntityEvent e) {
        if(waiting.contains(e.getPlayer().getUniqueId())) {
            Entity entity = e.getRightClicked();
            Parrot parrot = (Parrot) entity;
            parrot.setAI(false);

            CraftParrot parrot1 = (CraftParrot) parrot;
            Thread dance = new Thread(() -> {
                for(;;) {
                    Bukkit.getScheduler().runTaskAsynchronously(instance, () -> {
                        parrot.setSitting(true);
                        parrot1.getHandle().setHeadRotation(parrot1.getHandle().getHeadRotation() + 45f);
                    });
                    try {
                        Thread.sleep(300);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                        parrot.setAI(true);
                        return;
                    }
                    Bukkit.getScheduler().runTaskAsynchronously(instance, () -> {
                        parrot.setSitting(false);
                        parrot1.getHandle().setHeadRotation(parrot1.getHandle().getHeadRotation() - 90f);
                    });
                    try {
                        Thread.sleep(300);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                        parrot.setAI(true);
                        return;
                    }
                    parrot1.getHandle().setHeadRotation(parrot1.getHandle().getHeadRotation() + 45f);
                }
            });

            dance.start();
            threads.add(dance);
        }
    }

}
