package eu.Realmland.CruX.control.fun.commands;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.ui.commands.model.Command;
import eu.Realmland.CruX.ui.commands.model.CommandGoods;
import eu.Realmland.CruX.ux.profiles.model.Profile;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftPlayer;
import org.bukkit.event.Listener;
import org.bukkit.permissions.PermissionDefault;
import org.jetbrains.annotations.NotNull;

public class WtfCommand extends Command implements Listener {


    @CommandGoods(name = "tancujvykrucaj", aliases = {"cotokurvarobi"}, permission = "realmland.premium.flex", permissionDefault = PermissionDefault.TRUE)
    public WtfCommand(@NotNull CruX instance) {
        super(instance);
        Bukkit.getPluginManager().registerEvents(this, instance);
    }

    @Override
    public boolean executeCommand(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) {
        Profile profile = requireOnlyPlayer(sender);
        if(profile == null)
            return true;

        ((CraftPlayer) sender).getHandle().q(100);
        sender.sendMessage("§7Chod do tretej osoby (Zvyčajne tlačítko F5) (Tento efekt potrvá 5s)");
        return true;
}


}
