package eu.Realmland.CruX.control.services;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class MuteService {

    // TODO TIME MUTE

    /**
     * Muted profiles
     */
    @Getter
    private List<UUID> mutedPlayers = new ArrayList<>();

    @Getter
    private Map<UUID, Long> begTimeout = new HashMap<>();

    @Getter
    private List<UUID> blacklistedBeggers = new ArrayList<>();

    /**
     * Mutes player
     * @param uuid UUID
     */
    public void mute(@NotNull UUID uuid) {
        if (!isMuted(uuid))
            mutedPlayers.add(uuid);
    }

    /**
     * Un-mutes player
     * @param uuid UUID
     */
    public void unmute(@NotNull UUID uuid) {
        if (isMuted(uuid))
            mutedPlayers.remove(uuid);
    }

    /**
     * Checks if player is muted
     * @param uuid UUID
     * @return Is Player muted?
     */
    public boolean isMuted(@NotNull UUID uuid) {
        return mutedPlayers.contains(uuid);
    }


    /**
     * Adds begger to blacklist
     * @param uuid UUID
     */
    public void blacklistBegger(@NotNull UUID uuid) {
        if(!isBeggerBlacklisted(uuid))
            blacklistedBeggers.add(uuid);
    }

    /**
     *
     * @param uuid
     */
    public void whitelistBegger(@NotNull UUID uuid) {
        if(isBeggerBlacklisted(uuid))
            blacklistedBeggers.remove(uuid);
    }

    /**
     * Checks if Begger is blacklisted
     * @param uuid UUID
     * @return True/False
     */
    public boolean isBeggerBlacklisted(@NotNull UUID uuid) {
        return blacklistedBeggers.contains(uuid);
    }
}
