package eu.Realmland.CruX.control.services.justice;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.services.IService;
import me.MrWener.RealmLandDatabase.Database;
import me.MrWener.RealmLandLoggerLib.Logger;
import org.jetbrains.annotations.NotNull;

public class BanService implements IService {

    @NotNull
    private CruX instance;

    @NotNull
    private Logger logger;

    @NotNull
    private Database database;

    /**
     * Default constructor
     * @param instance
     */
    public BanService(@NotNull CruX instance) {
        this.instance = instance;
    }

    @Override
    public void initialize() throws Exception {

    }

    @Override
    public void terminate() throws Exception {

    }
}
