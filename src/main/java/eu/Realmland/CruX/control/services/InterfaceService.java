package eu.Realmland.CruX.control.services;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.services.IService;
import eu.Realmland.CruX.ux.profiles.model.Profile;
import eu.Realmland.CruX.ux.ranks.model.Rank;
import lombok.Getter;
import me.MrWener.RealmLandConfigurationsLib.Configuration;
import me.MrWener.RealmLandLoggerLib.Logger;
import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;
import org.jetbrains.annotations.NotNull;
import ru.tehkode.permissions.PermissionGroup;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

public class InterfaceService implements IService {

    private @NotNull CruX instance;
    private @NotNull Logger logger;

    private @NotNull Configuration config;

    @Getter
    private Scoreboard scoreboard;

    @Getter
    private Objective objective;

    @Getter
    private Map<String, Integer> priorities = new HashMap<>();

    private int taskIDtablist = -1;
    private int taskIDteams = -1;
    private final String AFK_SUFFIX = " §7AFK ";

    /**
     * Default constructor
     *
     * @param instance Instance
     */
    public InterfaceService(@NotNull CruX instance) {
        this.instance = instance;
        this.logger = instance.logger();
    }


    @Override
    public void initialize() throws Exception {
        boolean success = loadConfig();
        if (!success) {
            logger.error("Failed to load config");
            return;
        }

        // creative scoreboard
        scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();


        int update = 5;
        String renderType = null;

        ConfigurationSection settings = getConfig().getConfigurationSection("tablist-settings");
        if (settings != null) {
            update = settings.getInt("update", 5);
            renderType = getConfig().getString("render-type", null);
        }

        updateTeams();

        // get render type
        RenderType type = RenderType.INTEGER;
        if (renderType != null)
            for (RenderType nextType : RenderType.values()) {
                if (nextType.name().toUpperCase().equalsIgnoreCase(renderType))
                    type = nextType;
            }

        // create objective
        objective = scoreboard.registerNewObjective("InterfaceService", "dummy", "InterfaceServiceDisplay", type);
        objective.setDisplaySlot(DisplaySlot.PLAYER_LIST);

        taskIDtablist = Bukkit.getScheduler().scheduleSyncRepeatingTask(instance, () -> Bukkit.getScheduler().runTaskAsynchronously(instance, this::updateTablist), 0, update * 20);
        taskIDteams = Bukkit.getScheduler().scheduleSyncRepeatingTask(instance, () -> Bukkit.getScheduler().runTaskAsynchronously(instance, this::updateTeams), 0, update * 20);
    }

    @Override
    public void terminate() throws Exception {
        Bukkit.getScheduler().cancelTask(taskIDtablist);
        Bukkit.getScheduler().cancelTask(taskIDteams);

        priorities.clear();
        scoreboard.getTeams().forEach(Team::unregister);
        scoreboard.clearSlot(DisplaySlot.PLAYER_LIST);
    }

    public void updateTeams() {
        ConfigurationSection settings = getConfig().getConfigurationSection("tablist-settings");
        // register ranks
        for (Rank rank : instance.getRankRegistry().getRanks().values()) {
            priorities.put(rank.getName(), rank.getPriority());
            Team t = scoreboard.getTeam(rank.getPriority() + rank.getName());
            Team afk = scoreboard.getTeam(rank.getPriority() + rank.getName() + "K");
            if (t == null)
                t = scoreboard.registerNewTeam(rank.getPriority() + rank.getName());
            if (afk == null)
                afk = scoreboard.registerNewTeam(rank.getPriority() + rank.getName() + "K");

            t.setPrefix(" " + rank.getPrefix() + " ");
            afk.setPrefix(t.getPrefix());
            afk.setSuffix(AFK_SUFFIX);
        }

        // if settings are not null
        if (settings != null) {
            ConfigurationSection groups = settings.getConfigurationSection("groups");
            if (groups != null) {
                if (!Bukkit.getPluginManager().isPluginEnabled("PermissionsEx")) {
                    logger.warn("PermissionsEx not found. Ignoring groups priority");
                    return;
                }
                // Register groups
                for (String group : groups.getKeys(false)) {
                    PermissionGroup g = PermissionsEx.getPermissionManager().getGroup(group);
                    // if exists
                    if (g != null) {
                        priorities.put(group, groups.getInt(group + ".priority", 0));
                        Team t = scoreboard.getTeam("!" + priorities.get(group) + group);
                        Team afk = scoreboard.getTeam("!" + priorities.get(group) + group + "K");

                        if (t == null)
                            t = scoreboard.registerNewTeam("!" + priorities.get(group) + group);
                        if (afk == null)
                            afk = scoreboard.registerNewTeam("!" + priorities.get(group) + group + "K");

                        t.setPrefix(" " + g.getPrefix() + " ");
                        afk.setPrefix(t.getPrefix());
                        afk.setSuffix(AFK_SUFFIX);
                    }
                }
            }
        }
    }

    public void updateTablist() {
        Bukkit.getOnlinePlayers().forEach(this::sendHeaderAndFooter);
        // update profiles
        Bukkit.getOnlinePlayers().stream().forEach(player -> {
            Profile profile = instance.getProfileRegistry().getProfile(player.getUniqueId());
            if (profile == null)
                return;

            Rank r = instance.getRankRegistry().getRank(profile.getRankPoints());

            String prefix = "";
            int priority = -1;

            // if PermissionsEx is running, and player's staff
            if (Bukkit.getPluginManager().isPluginEnabled("PermissionsEx") && profile.isStaff()) {
                // get permisisonsex group
                PermissionUser user = PermissionsEx.getPermissionManager().getUser(player.getUniqueId());
                List<String> groups = user.getParentIdentifiers();
                // if has groups
                if (groups.size() > 0) {
                    String groupName = groups.get(0);

                    // priority
                    if (priorities.get(groupName) != null)
                        priority = priorities.get(groupName);

                    Team team = scoreboard.getTeam("!" + priority + groupName);

                    // if team exists
                    if (team != null) {
                        prefix = team.getPrefix();
                        team.addEntry(player.getName());
                    }

                    // afk
                    if (instance.getControlCenter().getAfkService().isPlayerAfk(player.getUniqueId())) {
                        Team afk = scoreboard.getTeam("!" + priority + groupName + "K");
                        if (afk != null)
                            afk.addEntry(player.getName());
                        else
                            logger.warn("afk group is null");
                    }

                }
            } else {
                // priority
                if (r == null) {
                    logger.warn("Rank is NULL! Probably you haven't specfied rank for %d rp", profile.getRankPoints());
                } else {
                    try {
                        if (priorities.get(r.getName()) != null)
                            priority = priorities.get(r.getName());
                    } catch (NullPointerException x) {
                        logger.warn("Priority for rank %s is null!", r.getName());
                    }
                    if (priority != -1) {
                        Team team = scoreboard.getTeam(priority + r.getName());

                        if (team != null) {
                            prefix = team.getPrefix();
                            team.addEntry((player.getName()));
                        }
                        if (instance.getControlCenter().getAfkService().isPlayerAfk(player.getUniqueId())) {
                            Team afk = scoreboard.getTeam(priority + r.getName() + "K");
                            if (afk != null)
                                afk.addEntry(player.getName());
                            else
                                logger.warn("afk rank is null");
                        }
                    }
                }
            }


            objective.getScore(player.getName()).setScore(((CraftPlayer) player).getHandle().playerConnection.player.ping);


            player.setScoreboard(scoreboard);
        });
    }

    public void updateScoreboard() {

    }

    /**
     * Sends Header and footer to player based on language
     *
     * @param player Player
     */
    public void sendHeaderAndFooter(@NotNull Player player) {
        Profile profile = instance.getProfileRegistry().getProfile(player.getUniqueId());
        if (profile == null)
            return;

        String headerString = "";
        String footerString = "";

        // process header
        ListIterator<String> header = getConfig().getStringList("header." + profile.getLang().name().toLowerCase()).listIterator();
        while (header.hasNext()) {
            String line = header.next() + (header.hasNext() ? "\n" : "");
            headerString = headerString.concat(processLine(line, player));
        }

        // process footer
        ListIterator<String> footer = getConfig().getStringList("footer." + profile.getLang().name().toLowerCase()).listIterator();
        while (footer.hasNext()) {
            String line = footer.next() + (footer.hasNext() ? "\n" : "");
            footerString = footerString.concat(processLine(line, player));
        }


        player.setPlayerListHeaderFooter(headerString, footerString);
    }

    /**
     * Replaces placeholders
     *
     * @param line   Line
     * @param player Player
     * @return Processed line
     */
    public String processLine(@NotNull String line, @NotNull Player player) {
        if (Bukkit.getPluginManager().isPluginEnabled("PlaceholderAPI")) {
            line = PlaceholderAPI.setPlaceholders(player, line);
        }
        return line;
    }

    /**
     * @return Loads config
     */
    public boolean loadConfig() {
        try {
            this.config = new Configuration(instance, "localization/interface.yml", true);
            return true;
        } catch (IOException e) {
            logger.error("Failed to load interface configuration", e);
        }
        return false;
    }

    /**
     * @return Config
     */
    public FileConfiguration getConfig() {
        return config.getConfig();
    }
}
