package eu.Realmland.CruX.control.services;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.localization.Lang;
import eu.Realmland.CruX.localization.Localization;
import eu.Realmland.CruX.services.IService;
import lombok.Getter;
import me.MrWener.RealmLandLoggerLib.Logger;
import me.clip.placeholderapi.PlaceholderAPI;
import net.md_5.bungee.chat.ComponentSerializer;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.*;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class AFKService implements IService, Listener {

    @NotNull
    private CruX instance;

    @NotNull
    private Logger logger;

    @NotNull
    private Localization localization;

    @NotNull
    @Getter
    private final Map<UUID, Long> lastActions = new HashMap<>();

    @NotNull
    @Getter
    private final List<UUID> afkPlayers = new ArrayList<>();

    /**
     * Default constructor
     *
     * @param instance Instance
     */
    public AFKService(@NotNull CruX instance) {
        this.instance = instance;
        this.logger = instance.logger();
        this.localization = instance.getGeneralLocalization();
    }

    @Override
    public void initialize() throws Exception {
        Bukkit.getPluginManager().registerEvents(this, instance);
        Bukkit.getScheduler().scheduleSyncRepeatingTask(
                instance,
                () -> Bukkit.getScheduler().runTaskAsynchronously(instance, this::updateAfkPlayers),
                2 * 20,
                2 * 20);

        Bukkit.getOnlinePlayers().forEach(player -> setLastAction(player.getUniqueId(), System.currentTimeMillis()));
    }

    @Override
    public void terminate() throws Exception {

    }

    /**
     * Handles PlayerMoveEvent... If player moved more than 2.7 blocks (running) then remove him from being afk
     *
     * @param event PlayerMoveEvent
     */
    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        Location from = event.getFrom();
        Location to = event.getTo();
        UUID uuid = event.getPlayer().getUniqueId();
        if (to != null)
            if (to.distance(from) >= 0.27D)
                if (isPlayerAfk(uuid))
                    removeAfkPlayer(uuid);
        if (!isPlayerAfk(uuid))
            setLastAction(uuid, System.currentTimeMillis());
    }

    @EventHandler
    public void onChat(PlayerCommandPreprocessEvent e) {
        if (!isPlayerAfk(e.getPlayer().getUniqueId()))
            setLastAction(e.getPlayer().getUniqueId(), System.currentTimeMillis());
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {
        if (!isPlayerAfk(e.getPlayer().getUniqueId()))
            setLastAction(e.getPlayer().getUniqueId(), System.currentTimeMillis());
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        UUID uuid = event.getPlayer().getUniqueId();
        if (isPlayerAfk(uuid))
            afkPlayers.remove(uuid);

        lastActions.remove(uuid);

    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        setLastAction(event.getPlayer().getUniqueId(), System.currentTimeMillis());
    }

    /**
     * goes trough all players and if their last action was long time ago, set them as afk
     */
    public void updateAfkPlayers() {
        long max = instance.getConfig().getInt("afk-settings.timeout", 3) * 60 * 1000;

        Bukkit.getOnlinePlayers().forEach(player -> {
            long lastAction = getLastAction(player.getUniqueId());

            if (System.currentTimeMillis() - lastAction > max)
                addAfkPlayer(player.getUniqueId());
        });
    }

    /**
     * Sets UUID's last action to specified time.
     *
     * @param uuid UUID
     * @param time Time
     */
    public void setLastAction(@NotNull UUID uuid, long time) {
        getLastActions().put(uuid, time);
    }

    public long getLastAction(@NotNull UUID uuid) {
        return getLastActions().get(uuid);
    }

    /**
     * Adds AFK player to the list.
     *
     * @param uuid UUID
     */
    public void addAfkPlayer(@NotNull UUID uuid) {
        if (isPlayerAfk(uuid))
            return;

        getAfkPlayers().add(uuid);
        localization.broadcast("player_afk", Bukkit.getOfflinePlayer(uuid));
        String consoleMsg = localization.get("player_afk", Lang.EN);
        consoleMsg = PlaceholderAPI.setPlaceholders(Bukkit.getOfflinePlayer(uuid), consoleMsg);
        Bukkit.getConsoleSender().spigot().sendMessage(ComponentSerializer.parse(consoleMsg));
    }

    /**
     * Removes AFK player from the list.
     *
     * @param uuid UUID
     */
    public void removeAfkPlayer(@NotNull UUID uuid) {
        if (!isPlayerAfk(uuid))
            return;

        localization.broadcast("player_afk_back", Bukkit.getOfflinePlayer(uuid));
        String consoleMsg = localization.get("player_afk_back", Lang.EN);
        consoleMsg = PlaceholderAPI.setPlaceholders(Bukkit.getOfflinePlayer(uuid), consoleMsg);
        Bukkit.getConsoleSender().spigot().sendMessage(ComponentSerializer.parse(consoleMsg));
        getAfkPlayers().remove(uuid);
        setLastAction(uuid, System.currentTimeMillis());
    }

    /**
     * Checks if player is afk
     *
     * @param uuid UUID
     * @return True if player is afk else false
     */
    public boolean isPlayerAfk(@NotNull UUID uuid) {
        return getAfkPlayers().contains(uuid);
    }
}
