package eu.Realmland.CruX.control.services;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.localization.Localization;
import eu.Realmland.CruX.services.IService;
import eu.Realmland.CruX.ux.profiles.model.Profile;
import lombok.Getter;
import me.MrWener.RealmLandLoggerLib.Logger;
import me.clip.placeholderapi.PlaceholderAPI;
import net.minecraft.server.v1_14_R1.IChatBaseComponent;
import net.minecraft.server.v1_14_R1.PacketPlayOutChat;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.UUID;

public class ChatService implements IService, Listener {

    @NotNull
    private CruX instance;
    private Logger logger;
    private Localization localization;

    private MuteService muteService;


    @Getter
    private final HashMap<UUID, Long> timeout = new HashMap<>();

    /**
     * Default constructor
     *
     * @param instance Instance of CruX
     */
    public ChatService(@NotNull CruX instance, @NotNull MuteService muteService) {
        this.instance = instance;
        this.localization = instance.getGeneralLocalization();
        this.logger = instance.logger();

        this.muteService = muteService;
    }

    @Override
    public void initialize() throws Exception {
        Bukkit.getPluginManager().registerEvents(this, instance);

    }

    @Override
    public void terminate() throws Exception {

    }


    @EventHandler
    public void handleFormat(AsyncPlayerChatEvent e) {
        if (e.isCancelled())
            return;

        if (e.getMessage().toLowerCase().startsWith("token::")) {
            e.setCancelled(true);
            return;
        }

        ConfigurationSection section = instance.getConfig().getConfigurationSection("chat-settings");

        if (section == null) {
            logger.warn("Bad configuration: ChatSettings are null");
            return;
        }

        Player player = e.getPlayer();
        UUID uuid = player.getUniqueId();

        Profile profile = instance.getProfileRegistry().getProfile(uuid);
        if (profile == null) {
            player.sendMessage("Please re-login to continue chatting.");
            logger.warn("Player '%s' does not have player profile", player.getName());
            e.setCancelled(true);
            return;
        }


        // If is muted, send him to hell
        if (muteService.isMuted(uuid)) {
            player.sendMessage(localization.get("muted", profile.getLang()));
            e.setCancelled(true);
            return;
        }

        long delay = section.getInt("timeout", 2) * 1000;
        if (timeout.containsKey(uuid)) {
            long last = timeout.get(uuid);
            long calc = (System.currentTimeMillis() - last);
            /// L     T     Left
            // 800 - 2000 = -1200 * -1 = 1200 / 1000 = 1.2s
            if (!(calc >= delay)) {
                player.sendMessage(localization.get("chat_timeout", profile.getLang(), (double) ((((calc - delay) * -1d) / 1000d))));
                e.setCancelled(true);
                return;
            }
        }

        String format = section.getString("format");
        if (format == null) {
            logger.warn("Bad configuration: Format is null");
            return;
        }

        String message = e.getMessage();
        if (player.hasPermission("realmland.premium.color")) {
            String permColor = profile.getProfileData().getSetting("perm-color");
            if(permColor != null)
                message = permColor + message;
       }
        message = message.replace("\"", "\\\\\"");

        // if he does not have permission for colors, strip 'em!
        format = format.replaceAll("%message%", (player.hasPermission("realmland.premium.color") ? message : Localization.stripColor(message)));
        if (Bukkit.getPluginManager().isPluginEnabled("PlaceholderAPI"))
            format = PlaceholderAPI.setPlaceholders(player, format);

        String stringType = section.getString("type");
        if (stringType == null) {
            logger.warn("Bad configuration: Prefix is null");
            return;
        }

        Localization.Type type = Localization.Type.of(stringType);

        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', String.format("§f%s§8: §7%s", player.getName(), e.getMessage())));
        broadcastRaw(format, type);
        timeout.put(uuid, System.currentTimeMillis());
        e.setCancelled(true);
    }


    @EventHandler
    public void death(PlayerDeathEvent e) {
        ConfigurationSection section = instance.getConfig().getConfigurationSection("death-settings");
        if (section == null) {
            logger.warn("Bad configuration: DeathSettings are null");
            return;
        }

        Localization.Type type = null;
        // get type name
        String typeName = section.getString("type");


        if (typeName != null)
            type = Localization.Type.of(typeName);

        if (typeName == null || type == null)
            type = Localization.Type.TEXT;


        String format = section.getString("format");


        // if format is null, set to defaults
        if (format == null) {
            format = "{\"text\":\"§8# §7%s\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/profiles %s\"}}";
            type = Localization.Type.JSON;
        }
        format = String.format(format, e.getDeathMessage(), e.getEntity().getName());

        broadcastRaw(format, type);
        Bukkit.getConsoleSender().sendMessage("§8# §7" + e.getDeathMessage());
        e.setDeathMessage("");

    }

    @EventHandler
    public void onRespawn(PlayerRespawnEvent e) {
        ConfigurationSection section = instance.getConfig().getConfigurationSection("death-settings");
        if (section == null) {
            logger.warn("Bad configuration: DeathSettings are null");
            return;
        }

        Player player = e.getPlayer();
        UUID uuid = player.getUniqueId();
        Profile profile = instance.getProfileRegistry().getProfile(uuid);
        if (profile == null)
            return;

        if (section.getBoolean("send-location-info", true)) {
            Location diedLoc = player.getLocation();
            player.sendMessage(localization.get("death_info", profile.getLang(), diedLoc.getBlockX(), diedLoc.getBlockY(), diedLoc.getBlockZ()));
        }
    }




    public void broadcastRaw(@NotNull String payload, Localization.Type type) {
        // List<Player> profiles = instance.getAuthRegistry().getAuthQueue().stream().filter(LoginSession::isLogged).map(session -> Bukkit.getPlayer(session.getUUID())).filter(Objects::nonNull).collect(Collectors.toList());
        if (type.equals(Localization.Type.JSON))
            for (Player userPlayer : Bukkit.getOnlinePlayers()) {
                if (userPlayer != null)
                    ((CraftPlayer) userPlayer).getHandle().playerConnection.sendPacket(new PacketPlayOutChat(IChatBaseComponent.ChatSerializer.a(payload)));
            }
        else
            for (Player userPlayer : Bukkit.getOnlinePlayers()) {
                if (userPlayer != null) userPlayer.sendMessage(payload);
            }
    }


}
