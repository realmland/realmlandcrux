package eu.Realmland.CruX.api;

import eu.Realmland.CruX.localization.Lang;
import org.jetbrains.annotations.NotNull;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Address Api
 */
public class AApi {

    /**
     * Detects player's country using http://ip-api.com/
     *
     * @param address Address of player
     * @return Player's language, Language is set to english if language is not supported or couldn't be fetched
     */
    public static @NotNull Lang detectCountry(InetSocketAddress address) {
        try {
            URL url = new URL("http://ip-api.com/json/" + address.getAddress().getHostAddress());
            HttpURLConnection c = (HttpURLConnection) url.openConnection();

            StringBuilder response = new StringBuilder();
            int byt;
            do {
                byt = c.getInputStream().read();
                if (byt != -1) {
                    response.append((char) byt);
                }
            } while (byt != -1);
            c.getInputStream().close();


            JSONObject object = (JSONObject) new JSONParser().parse(response.toString());
            String status = object.get("status").toString();
            if(status.equalsIgnoreCase("fail"))
                return Lang.EN;

            String strLang = object.get("countryCode").toString();

            Lang lang = Lang.EN;

            for (Lang next : Lang.values())
                if (next.name().toUpperCase().equalsIgnoreCase(strLang))
                    lang = next;


            return lang;

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return Lang.EN;
    }



}
