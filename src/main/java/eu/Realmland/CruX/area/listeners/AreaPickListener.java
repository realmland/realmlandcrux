package eu.Realmland.CruX.area.listeners;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.services.IService;
import eu.Realmland.CruX.tuples.Pair;
import lombok.Getter;
import me.MrWener.RealmLandLoggerLib.Logger;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class AreaPickListener implements IService, Listener {

    private final CruX instance;
    @Getter
    private final Map<UUID, Pair<Location, Location>> pickedArea = new HashMap<>();
    private Logger logger;

    public AreaPickListener(@NotNull CruX instance) {
        this.instance = instance;
        this.logger = instance.logger();
    }

    @Override
    public void initialize() throws Exception {
        Bukkit.getPluginManager().registerEvents(this, instance);
    }

    @Override
    public void terminate() throws Exception {

    }

    /**
     * Handles picking of locations
     *
     * @param e Event
     */
    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        Action action = e.getAction();
        Player player = e.getPlayer();
        UUID uuid = e.getPlayer().getUniqueId();

        if (e.getHand() != null)
            if (e.getHand().equals(EquipmentSlot.HAND))
                if (action.equals(Action.LEFT_CLICK_BLOCK) || action.equals(Action.RIGHT_CLICK_BLOCK)) {
                    ItemStack item = e.getItem();
                    Block block = e.getClickedBlock();
                    if (block == null)
                        return;
                    Location clicked = block.getLocation();

                    Pair<Location, Location> data = pickedArea.getOrDefault(uuid, new Pair<>(null, null));
                    if (item != null && item.getType().equals(Material.WOODEN_HOE)) {
                        // set first pos
                        if (action.equals(Action.RIGHT_CLICK_BLOCK)) {
                            data.setFirst(clicked);
                            player.sendMessage("§8§l# §fSet #1 position.");
                        }
                        // set second pos
                        if (action.equals(Action.LEFT_CLICK_BLOCK)) {
                            data.setSecond(clicked);
                            player.sendMessage("§8§l# §fSet #2 position.");
                        }
                        pickedArea.put(uuid, data);
                        e.setCancelled(true);
                    }

                }
    }
}
