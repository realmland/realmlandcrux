package eu.Realmland.CruX.area.commands;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.area.AreaRegistry;
import eu.Realmland.CruX.area.listeners.AreaPickListener;
import eu.Realmland.CruX.statics.WorldStatics;
import eu.Realmland.CruX.tuples.Pair;
import eu.Realmland.CruX.ui.commands.model.Command;
import eu.Realmland.CruX.ui.commands.model.CommandGoods;
import eu.Realmland.CruX.ux.profiles.model.Profile;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionDefault;
import org.jetbrains.annotations.NotNull;

public class AreaCommand extends Command {

    private AreaRegistry areaRegistry;
    private AreaPickListener areaPickListener;

    @CommandGoods(name = "area", permission = "realmland.area", permissionDefault = PermissionDefault.TRUE)
    public AreaCommand(@NotNull CruX instance) {
        super(instance);
        this.areaRegistry = instance.getAreaRegistry();
        this.areaPickListener = areaRegistry.getAreaPickListener();
    }

    @Override
    public boolean executeCommand(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) {
        Profile profile = requireOnlyPlayer(sender);
        if (profile == null)
            return true;
        Player player = (Player) sender;

        Pair<Location, Location> locations = this.areaPickListener.getPickedArea().getOrDefault(player.getUniqueId(), null);
        if (locations == null || !locations.isValid()) {
            sender.sendMessage("§cChoose area");
            return true;
        }

        Bukkit.getScheduler().runTaskTimerAsynchronously(instance, () -> WorldStatics.showCuboidArea(WorldStatics.sortLocations(locations), player), 0, 10);

        return true;
    }
}
