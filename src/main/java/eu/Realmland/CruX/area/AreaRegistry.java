package eu.Realmland.CruX.area;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.area.listeners.AreaPickListener;
import eu.Realmland.CruX.services.IService;
import lombok.Getter;
import me.MrWener.RealmLandLoggerLib.Logger;
import org.jetbrains.annotations.NotNull;

public class AreaRegistry implements IService {

    private final CruX instance;
    private Logger logger;

    @Getter
    private AreaPickListener areaPickListener;

    /**
     * Default constructor
     *
     * @param instance Instance of CruX
     */
    public AreaRegistry(@NotNull CruX instance) {
        this.instance = instance;
        this.logger = instance.logger();

        this.areaPickListener = new AreaPickListener(instance);
    }

    @Override
    public void initialize() throws Exception {
        try {
            this.areaPickListener.initialize();
        } catch (Exception x) {
            logger.error("Failed to initialize AreaPickListener", x);
        }
    }

    @Override
    public void terminate() throws Exception {
        try {
            this.areaPickListener.terminate();
        } catch (Exception x) {
            logger.error("Failed to terminate AreaPickListener", x);
        }
    }
}
