package eu.Realmland.CruX.ux.quests.programmatic;

import eu.Realmland.CruX.ux.profiles.model.Profile;
import eu.Realmland.CruX.ux.quests.model.Quest;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

/**
 * Represent reward which can be only programmed.
 */
public abstract class ProgrammaticReward {

    /**
     * Executes when player completes the Quest.
     * @param player   Player to be rewarded.
     * @param profile  Player's profile.
     * @param quest    Quest player has completed
     */
    public abstract void rewardPlayer(@NotNull Player player, @NotNull Profile profile, @NotNull Quest quest);


    public abstract String getValue();


    /**
     * Represents Reward which is Numeric
     */
    public static interface NumericReward {

        default int calculateBonus(int amount, float percentage) {
            return (int) (amount + (amount * percentage));
        }
    }

}
