package eu.Realmland.CruX.ux.quests.reward.model;

import eu.Realmland.CruX.localization.model.LangNode;
import eu.Realmland.CruX.ux.profiles.model.Profile;
import eu.Realmland.CruX.ux.quests.model.Quest;
import eu.Realmland.CruX.ux.quests.programmatic.ProgrammaticReward;
import lombok.Getter;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

/**
 * Pre-Made Programmatic rewards that can be used in configuration
 */
public class RewardTypes {


    /**
     * This ProgrammaticReward gives profile RankPoints
     */
    public static class RankPoints extends ProgrammaticReward implements ProgrammaticReward.NumericReward {

        @Getter
        private int amount = 0;

        private LangNode lore;

        /**
         * Default constructor
         * @param amount Amount of RankPoints
         */
        public RankPoints(int amount) {
            this.amount = amount;
        }

        @Override
        public void rewardPlayer(@NotNull Player player, @NotNull Profile profile, @NotNull Quest quest) {
            profile.addRankPoints(calculateBonus(amount, quest.getDifficultyPercentage()));
        }

        @Override
        public String getValue() {
            return amount + "";
        }
    }

    /**
     * This ProgrammaticReward gives profile Coins
     */
    public static class Coins extends ProgrammaticReward implements ProgrammaticReward.NumericReward {
        @Getter
        private int amount = 0;

        /**
         * Default constructor
         * @param amount Amount of Coins
         */
        public Coins(int amount) {
            this.amount = amount;
        }

        @Override
        public void rewardPlayer(@NotNull Player player, @NotNull Profile profile, @NotNull Quest quest) {
            //profile.addCoins((calculateBonus(amount, quest.getDifficultyPercentage())));
        }

        @Override
        public String getValue() {
            return amount + "";
        }

    }

    /**
     * This ProgrammaticReward gives profile Gems
     */
    public static class Gems extends ProgrammaticReward implements ProgrammaticReward.NumericReward {

        @Getter
        private int amount = 0;

        /**
         * Default constructor
         * @param amount Amount of Gems
         */
        public Gems(int amount) {
            this.amount = amount;
        }

        @Override
        public void rewardPlayer(@NotNull Player player, @NotNull Profile profile, @NotNull Quest quest) {
            //profile.addGems(calculateBonus(amount, quest.getDifficultyPercentage()));
        }


        @Override
        public String getValue() {
            return amount + "";
        }
    }

    /**
     * This ProgrammaticReward gives profile Gems
     */
    public static class Item extends ProgrammaticReward  {

        @Getter
        private ItemStack item;

        /**
         * Default constructor
         * @param itemStack ItemStack
         */
        public Item(@NotNull ItemStack itemStack) {
            this.item = itemStack;
        }

        @Override
        public void rewardPlayer(@NotNull Player player, @NotNull Profile profile, @NotNull Quest quest) {
            player.getInventory().addItem(item);
        }

        @Override
        public String getValue() {
            return item.getItemMeta().getDisplayName();
        }
    }

    public static Class<? extends ProgrammaticReward> valueOf(@NotNull String string) {
        if(string.equalsIgnoreCase("RankPoints"))
            return RankPoints.class;
        else if(string.equalsIgnoreCase("Coins"))
            return Coins.class;
        else if(string.equalsIgnoreCase("Gems"))
            return Gems.class;
        else if(string.equalsIgnoreCase("Item"))
            return Item.class;
        return null;
    }


}
