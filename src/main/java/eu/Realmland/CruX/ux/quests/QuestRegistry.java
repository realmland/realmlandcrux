package eu.Realmland.CruX.ux.quests;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.localization.Localization;
import eu.Realmland.CruX.localization.model.LangNode;
import eu.Realmland.CruX.services.IService;
import eu.Realmland.CruX.ui.gui.custom.QuestsGUI;
import eu.Realmland.CruX.ux.quests.model.Quest;
import eu.Realmland.CruX.ux.quests.model.QuestGoal;
import eu.Realmland.CruX.ux.quests.model.QuestType;
import eu.Realmland.CruX.ux.quests.programmatic.ProgrammaticGoal;
import eu.Realmland.CruX.ux.quests.programmatic.ProgrammaticReward;
import eu.Realmland.CruX.ux.quests.reward.RewardBulk;
import eu.Realmland.CruX.ux.quests.reward.model.RewardTypes;
import lombok.Getter;
import me.MrWener.RealmLandConfigurationsLib.Configuration;
import me.MrWener.RealmLandLoggerLib.Logger;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Collectors;

public class QuestRegistry implements IService {

    @NotNull
    @Getter
    private CruX instance;

    @NotNull
    private Logger logger;

    @Getter
    private final Map<String, Quest> quests = new HashMap<>();

    @Getter
    @NotNull
    private Configuration questConfiguration;

    @NotNull
    @Getter
    private Localization localization;

    private LocalDateTime nextQuests;

    @Getter
    private final QuestsGUI gui;

    /**
     * Default constructor
     *
     * @param instance Instance of CruX
     */
    public QuestRegistry(@NotNull CruX instance) {
        this.instance = instance;
        this.logger = instance.logger();

        try {
            this.questConfiguration = new Configuration(instance, "quests/quests.yml", true);
        } catch (IOException e) {
            logger.error("Failed to construct Quests configuration", e);
        }

        this.localization = new Localization(getInstance(), "localization_quests");

        this.gui = new QuestsGUI(instance, this);
    }

    @Override
    public void initialize() throws Exception {
        localization.initialize();


        loadQuestsFromConfig();
        // Load quests from Config
        // Load programmatic quests

        setupNextDay();

    }

    @Override
    public void terminate() throws Exception {
        localization.terminate();
    }

    public void setupNextDay() {
        LocalDate now = LocalDate.now();
        LocalTime next = LocalTime.of(0, 1);

        nextQuests = LocalDateTime.of(now.plusDays(1), next);
        long until = nextQuests.toInstant(ZoneOffset.UTC).toEpochMilli()-LocalDateTime.now().toInstant(ZoneOffset.UTC).toEpochMilli();
        Bukkit.getScheduler().runTaskLater(instance, ()-> {
            quests.clear();
            loadQuestsFromConfig();

            setupNextDay();
            logger.info("Quests renewed.");
        }, until / 1000 * 20);
    }

    public void loadQuestsFromConfig() {
        ConfigurationSection root = questConfiguration.getConfig().getConfigurationSection("quests");
        for (String questName : root.getKeys(false)) {
            ConfigurationSection section = root.getConfigurationSection(questName);

            int copy = section.getInt("_copy-quest", 0);
            quests.put(questName, loadQuest(questName, section));
            if(copy > 0) {
                for(int i = 0; i < copy; i++) {
                    Quest q = loadQuest(questName, section);
                    q.setName(questName + "_" + i);
                    quests.put(questName + "_" + i, q);
                }
            }

        }
        logger.debug("Loaded %d quests", quests.size());
    }

    public Quest loadQuest(@NotNull String questName, @NotNull ConfigurationSection section) {
        LangNode displayName = null;
        try {
            displayName = LangNode.of(
                    ChatColor.translateAlternateColorCodes('&', section.getString("display-name.sk")),
                    ChatColor.translateAlternateColorCodes('&', section.getString("display-name.cz")),
                    ChatColor.translateAlternateColorCodes('&', section.getString("display-name.en")));
        } catch (NullPointerException x) {
            logger.error("Display name is not set for quest %s", questName);
            return null;
        }


        List<LangNode> questLore = loadLore(section, "quest-lore", '7');
        List<LangNode> goalLore = loadLore(section, "goal-lore", 'a');

        String goalName = section.getString("goal");
        String typeName = section.getString("type");

        boolean ranCompute = section.getBoolean("_random-compute-goal", false);

        QuestType type = QuestType.of(typeName);
        // Quest goal
        Class<? extends ProgrammaticGoal> goal = QuestGoal.valueOf(goalName);
        // Quest rewards
        List<LangNode> rewardLore = loadLore(section, "reward-lore", '7');

        // rewards
        RewardBulk rewards = new RewardBulk(rewardLore);

        ConfigurationSection bulk = section.getConfigurationSection("reward-bulk");
        bulk.getKeys(false).forEach(rewardName -> {
            ConfigurationSection rewardSection = bulk.getConfigurationSection(rewardName);

            String rewardTypeName = rewardSection.getString("type");
            Class<? extends ProgrammaticReward> typeReward = RewardTypes.valueOf(rewardTypeName);

            if (typeReward == null)
                logger.warn("Unknown reward type class %s for quest %s", rewardTypeName, questName);
            else {
                try {
                    ProgrammaticReward reward = null;
                    if (typeReward.getSimpleName().equalsIgnoreCase("item")) {
                        ItemStack stack = rewardSection.getItemStack("item-stack");
                        reward = typeReward.getConstructor(ItemStack.class).newInstance(stack);
                    } else {
                        int amount = rewardSection.getInt("amount");
                        reward = typeReward.getConstructor(int.class).newInstance(amount);
                    }
                    rewards.addReward(reward);
                } catch (InstantiationException e) {
                    logger.error("Class %s can not be instantiated! Quest %s", typeReward.getSimpleName(), questName);
                } catch (IllegalAccessException e) {
                    logger.error("Class %s can not be accessed! Quest %s", typeReward.getSimpleName(), questName);
                } catch (NoSuchMethodException e) {
                    logger.error("Class %s is missing a isValid constructor; Quest %s", typeReward.getSimpleName(), questName);
                } catch (InvocationTargetException e) {
                    logger.error("Constructor of class %s threw unexpected exception!", e, typeReward.getSimpleName());
                }
            }
        });

        if (goal == null)
            logger.warn("Unknown goal class %s for quest %s", goalName, questName);
        else if (type == null)
            logger.warn("Unknown type class %s  for quest %s", goalName, questName);
        else {
            ProgrammaticGoal programmaticGoal = null;
            try {
                programmaticGoal = goal.getConstructor(CruX.class, List.class).newInstance(instance, goalLore);
            } catch (NoSuchMethodException e) {
                logger.error("Class %s is missing isValid constructor; Quest %s", goal.getSimpleName(), questName);
                return null;
            } catch (IllegalAccessException e) {
                logger.error("Constructor of class %s can not be accessed!", goal.getSimpleName());
                return null;
            } catch (InstantiationException e) {
                logger.error("Constructor of class %s can not be constructed!", goal.getSimpleName());
                return null;
            } catch (InvocationTargetException e) {
                logger.error("Constructor of class %s threw unexpected exception!", goal.getSimpleName());
                return null;
            }

            if (ranCompute) {
                if (programmaticGoal.getClass().getSimpleName().equalsIgnoreCase("KillEntitiesGoal")) {
                    programmaticGoal = randomComputeGoal(programmaticGoal, questName);
                    if(programmaticGoal == null) {
                        logger.warn("Could not random compute goal '%s'", goal.getSimpleName());
                        return null;
                    }
                }
            }
            return new Quest(questName, displayName, programmaticGoal, rewards, type, questLore);
        }
        return null;
    }

    public ProgrammaticGoal randomComputeGoal(@NotNull ProgrammaticGoal goal, @NotNull String questName) {
        switch (goal.getClass().getSimpleName()) {
            case "KillEntitiesGoal": {
                ConfigurationSection ranComputeSection = getQuestConfiguration().getConfig().getConfigurationSection("_random-compute-goal");
                if (ranComputeSection == null)
                    return null;

                List<String> entities =
                        ranComputeSection.getConfigurationSection("KillEntitiesGoal.entities")
                                .getKeys(false).stream().collect(Collectors.toList());

                Random random = new Random();
                for (int i = 0; i < 4; i++) {
                    // TODO Remake algorithm
                    int randomEntityNumber = random.nextInt(entities.size());
                    int difficultyRaw = ranComputeSection.getInt("KillEntitiesGoal.entities." + entities.get(randomEntityNumber) + ".difficulty");
                    int amount = random.nextInt(5) + 1;
                    if(amount == 0)
                        amount+=1;

                    if(difficultyRaw > ranComputeSection.getInt("KillEntitiesGoal.standard", 15))
                        amount/=2;

                    int difficulty =  difficultyRaw * amount;
                    goal.addDiffuculty(difficulty);

                    try {
                        ((QuestGoal.KillEntitiesGoal) goal).addEntityGoal(EntityType.valueOf(entities.get(randomEntityNumber).toUpperCase()), amount);
                    } catch (IllegalArgumentException x) {
                        logger.warn("Couldn't get entity by name of '%s' because it does not exist; Quest '%s'", entities.get(randomEntityNumber), questName);
                    }
                }
                return goal;
            }
        }
        return null;
    }
    /**
     * Loads lore from Configuration section.
     * @param section Section
     * @param root    Root of list
     * @param color   Color of each line
     * @return Localized lore
     */
    public List<LangNode> loadLore(@NotNull ConfigurationSection section, @NotNull String root, char color) {
        List<LangNode> lore = new ArrayList<>();
        List<String> skLoreList = null;
        List<String> czLoreList = null;
        List<String> enLoreList = null;
        try {
            skLoreList = section.getList(root + ".sk").stream().map(Object::toString).map(entry -> ChatColor.translateAlternateColorCodes('&', entry)).collect(Collectors.toList());
            czLoreList = section.getList(root + ".cz").stream().map(Object::toString).map(entry -> ChatColor.translateAlternateColorCodes('&', entry)).collect(Collectors.toList());
            enLoreList = section.getList(root + ".en").stream().map(Object::toString).map(entry -> ChatColor.translateAlternateColorCodes('&', entry)).collect(Collectors.toList());
            if (skLoreList.size() == czLoreList.size() && skLoreList.size() == enLoreList.size()) {
                for (int i = 0; i < skLoreList.size(); i++) {
                    String skLine = skLoreList.get(i);
                    if(!skLine.isEmpty())
                        skLine = "§" + color + skLine;

                    String czLine = czLoreList.get(i);
                    if(!skLine.isEmpty())
                        czLine = "§" + color + czLine;

                    String enLine = enLoreList.get(i);
                    if(!skLine.isEmpty())
                        enLine = "§" + color + enLine;

                    lore.add(LangNode.of(skLine, czLine, enLine));
                }
            } else {
                logger.warn("Lore sizes are not matching! Gotta improvize!");

                int biggest = skLoreList.size();
                if (biggest < czLoreList.size())
                    biggest = czLoreList.size();
                if (biggest < enLoreList.size())
                    biggest = enLoreList.size();

                for (int i = 0; i < biggest; i++) {

                    String skLine = "§" + color + getLine(skLoreList, i);
                    String czLine = "§" + color + getLine(czLoreList, i);
                    String enLine = "§" + color + getLine(enLoreList, i);

                    lore.add(LangNode.of(skLine, czLine, enLine));
                }
            }
            return lore;
        } catch (NullPointerException x) {
            logger.error("Lore is not set for section %s", section);
        }
        return null;
    }

    protected String getLine(List<String> list, int i) {
        String line = "";
        try {
            line = (list.get(i) == null ? "" : list.get(i));

        } catch (IndexOutOfBoundsException ignored) {}
        return line;
    }


}
