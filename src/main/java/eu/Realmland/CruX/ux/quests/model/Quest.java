package eu.Realmland.CruX.ux.quests.model;

import eu.Realmland.CruX.localization.model.LangNode;
import eu.Realmland.CruX.ux.profiles.model.Profile;
import eu.Realmland.CruX.ux.quests.programmatic.ProgrammaticGoal;
import eu.Realmland.CruX.ux.quests.reward.RewardBulk;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Quest {

    @Getter @Setter
    private @NotNull String name;

    @Getter @Setter
    private @NotNull LangNode displayName;

    @Getter
    private @NotNull List<LangNode>  questLore;

    @Getter
    private @NotNull QuestType type;

    @Getter
    private @NotNull ProgrammaticGoal goal;

    @Getter
    private final RewardBulk rewardBulk;

    @Getter
    private final List<UUID> completed = new ArrayList<>();

    /**
     * Default Quest constructor
     * @param name         Quest's unique name
     * @param displayName  Quest's display name
     * @param type         Quest's type
     * @param questLore    Quest's lore
     */
    public Quest(@NotNull String name, @NotNull LangNode displayName, @NotNull ProgrammaticGoal goal, @NotNull RewardBulk rewardBulk, @NotNull QuestType type, @NotNull List<LangNode> questLore) {
        this.name = name;
        this.displayName = displayName;
        this.rewardBulk = rewardBulk;
        this.type = type;
        this.questLore = questLore;
        this.goal = goal;
    }

    public int calculateDifficultyBonus(int baseValue) {
        float difficultyPercentage = getDifficultyPercentage();
        return (int)((float)baseValue * difficultyPercentage) + baseValue;
    }

    public float getDifficultyPercentage() {
        return getGoal().getDifficulty() / 100F;
    }

    /**
     * This method tries to turnIn the quest, and reward player
     * @param player  Player
     * @param profile Player's profile
     */
    public boolean turnIn(@NotNull Player player, @NotNull Profile profile) {
        if(completed.contains(player.getUniqueId()))
            return false;
        if(goal.isGoalReached(player)) {
            // goal is completed we can reward player
            rewardBulk.executeRewards(player, profile, this);
            completed.add(player.getUniqueId());
            return true;
        }
        return false;
    }
}
