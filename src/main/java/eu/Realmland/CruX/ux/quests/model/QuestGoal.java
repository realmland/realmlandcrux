package eu.Realmland.CruX.ux.quests.model;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.localization.Localization;
import eu.Realmland.CruX.localization.model.LangNode;
import eu.Realmland.CruX.ux.quests.programmatic.ProgrammaticGoal;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class QuestGoal {


    /**
     * QuestGoal based on killing entities
     */
    public static class KillEntitiesGoal extends ProgrammaticGoal implements Listener {

        @Getter
        private Map<UUID, Map<EntityType, Integer>> entityKills = new HashMap<>();

        @Getter @NotNull
        private Map<EntityType, Integer> entityGoals = new HashMap<>();

        /**
         * Constructs class without any entity goals.
         */
        public KillEntitiesGoal(@NotNull CruX instance, @NotNull List<LangNode> goalLore) {
            super(instance, goalLore);

            Bukkit.getPluginManager().registerEvents(this, instance);
        }

        /**
         * Constructs class without any entity goals.
         */
        public KillEntitiesGoal(@NotNull CruX instance, @NotNull List<LangNode> goalLore, @NotNull Map<EntityType, Integer> entityGoals) {
            this(instance, goalLore);
            this.entityGoals = entityGoals;
        }

        /**
         * Adds goal
         * @param type Type
         * @param goal Goal number
         */
        public void addEntityGoal(@NotNull EntityType type, int goal) {
            entityGoals.put(type, goal);
        }

        @Override
        public boolean isGoalReached(@NotNull Player player) {
            Map<EntityType, Integer> kills = entityKills.get(player.getUniqueId());

            if(kills == null)
                return false;

            // check if he has enough kills of every type
            for (EntityType type : entityGoals.keySet()) {
                int goal = entityGoals.get(type);

                // if does not have enough kills of this current type, send him to hell
                int actualKills = 0;
                try {
                    actualKills = kills.get(type);
                } catch (NullPointerException ignored) {}

                if(actualKills < goal)
                    return false;
            }

            // nothing's wrong
            return true;
        }


        @Override
        public List<String> getGoalProgressionLore(@NotNull UUID uuid) {
            List<String> lore = new ArrayList<>();
            entityGoals.forEach((type, amount) -> {
                try {
                    int kills = entityKills.get(uuid).get(type);
                    lore.add("§7" + Localization.firstToUpper(type.getName().replaceAll("_", " ")) + " §e" +  kills + "/" + amount);
                } catch (NullPointerException ignored) {
                    lore.add("§7" + Localization.firstToUpper(type.getName().replaceAll("_", " ")) + " §e" + 0 + "/" + amount);
                }
            });
            return lore;
        }

        @EventHandler
        public void handleKill(EntityDeathEvent e) {
            if(e.getEntity() instanceof  Player)
                return;

            Player killerPlayer = e.getEntity().getKiller();
            UUID killer = null;
            try {
                killer = e.getEntity().getKiller().getUniqueId();
            } catch (Exception ignored) {}

            if(killer == null) {
                return;
            }

            EntityType type = e.getEntityType();
            Map<EntityType, Integer> nowKills = entityKills.get(e.getEntity().getKiller().getUniqueId());
            if(nowKills == null)
                nowKills = new HashMap<>();
            int kills = (nowKills.get(type) == null ? 0 : nowKills.get(type));

            nowKills.put(e.getEntityType(), ++kills);
            this.entityKills.put(killer, nowKills);
        }
    }

    public static class GetItemGoal extends ProgrammaticGoal {

        @Getter
        private final List<ItemStack> items = new ArrayList<>();

        public GetItemGoal(@NotNull CruX instance, @NotNull List<LangNode> goalLore) {
            super(instance, goalLore);
        }

        /**
         * Adds item
         * @param material Material
         * @param amount   Amount of item
         */
        public void addItem(@NotNull Material material, int amount) {
            items.add(new ItemStack(material, amount));
        }

        @Override
        public boolean isGoalReached(@NotNull Player player) {
            return false;
        }

        @Override
        public List<String> getGoalProgressionLore(@NotNull UUID uuid) {
            return null;
        }
    }


    public static Class<? extends ProgrammaticGoal> valueOf(@NotNull String string) {
       if(string.equalsIgnoreCase("KillEntitiesGoal"))
           return KillEntitiesGoal.class;
       return null;
    }

}
