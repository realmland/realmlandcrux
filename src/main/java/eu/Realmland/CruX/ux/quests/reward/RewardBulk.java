package eu.Realmland.CruX.ux.quests.reward;

import eu.Realmland.CruX.localization.model.LangNode;
import eu.Realmland.CruX.ux.profiles.model.Profile;
import eu.Realmland.CruX.ux.quests.model.Quest;
import eu.Realmland.CruX.ux.quests.programmatic.ProgrammaticReward;
import lombok.Getter;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents Reward that can be configured
 */
public class RewardBulk {

    @Getter
    private final List<ProgrammaticReward> rewards = new ArrayList<>();

    @Getter @NotNull
    private List<LangNode> rewardLore = null;

    /**
     * Default constructor
     * @param rewardLore Reward lore
     */
    public RewardBulk(@NotNull List<LangNode>  rewardLore) {
        this.rewardLore = rewardLore;
    }

    /**
     * Adds reward to list.
     * @param reward    Reward
     */
    public void addReward(@NotNull ProgrammaticReward reward) {
        rewards.add(reward);
    }

    /**
     * Adds reward to list.
     * @param reward   Reward
     */
    public void removeReward(@NotNull ProgrammaticReward reward) {
        rewards.remove(reward);
    }

    /**
     * Executes all rewards
     * @param player   Player that is going to be rewarded
     * @param profile  Player's profile
     * @param quest    Quest
     */
    public void executeRewards(@NotNull Player player, @NotNull Profile profile, @NotNull Quest quest) {
        rewards.forEach(reward -> reward.rewardPlayer(player, profile, quest));
    }

}
