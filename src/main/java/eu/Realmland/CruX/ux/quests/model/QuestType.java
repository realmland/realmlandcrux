package eu.Realmland.CruX.ux.quests.model;

import org.jetbrains.annotations.NotNull;

public enum QuestType {

    DAILY_QUEST, QUEST_CHALLENGE;

    public static QuestType of(@NotNull String string) {
        for (QuestType value : values()) {
            if(value.name().equalsIgnoreCase(string))
                return value;
        }
        return null;
    }

}
