package eu.Realmland.CruX.ux.quests.programmatic;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.localization.model.LangNode;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.UUID;

public abstract class ProgrammaticGoal {

    @NotNull @Getter
    private CruX instance;

    @Getter @Setter
    private int difficulty = 1;

    @Getter
    private List<LangNode> goalLore;

    /**
     * Default constructor
     * @param instance Instance
     */
    public ProgrammaticGoal(@NotNull CruX instance, @NotNull List<LangNode> goalLore) {
        this.instance = instance;
        this.goalLore = goalLore;
    }

    public void addDiffuculty(int amount) {
        difficulty+=amount;
    }

    /**
     * Tests if player has completed Quest
     * @param player   Player
     */
    public abstract boolean isGoalReached(@NotNull Player player);


    /**
     * Returns progression lore of goal.
     * @param uuid Player's UUID
     * @return
     */
    public abstract List<String> getGoalProgressionLore(@NotNull UUID uuid);

}
