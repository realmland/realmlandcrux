package eu.Realmland.CruX.ux._commands.profile;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.localization.Lang;
import eu.Realmland.CruX.ui.commands.model.Command;
import eu.Realmland.CruX.ui.commands.model.CommandGoods;
import eu.Realmland.CruX.ux.profiles.model.Profile;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionDefault;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings("ALL")
public class ProfileCommand extends Command {


    @CommandGoods(name = "profile", permission = "realmland.player.profile", permissionDefault = PermissionDefault.TRUE)
    public ProfileCommand(@NotNull CruX instance) {
        super(instance);
    }

    @Override
    public boolean executeCommand(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) {
        if (!isPlayer) {
            if (!(args.length > 0)) {
                sender.sendMessage(generalLocalization.get("specify_player", Lang.EN));
                return true;
            }
            String name = args[0];
            OfflinePlayer player = Bukkit.getOfflinePlayer(name);
            if (player == null) {
                sender.sendMessage(generalLocalization.get("player_invalid", Lang.EN));
                return true;
            }
            Profile target = instance.getProfileRegistry().getProfile(player.getUniqueId());

            sender.sendMessage("§e" + player.getName());
            if (target.isStaff())
                sender.sendMessage(generalLocalization.get("player_is_staff", Lang.EN) + " §e" + target.getLang());
            sender.sendMessage(generalLocalization.get("name_language", Lang.EN) + " §e" + target.getLang());
            sender.sendMessage(generalLocalization.get("name_rank_points", Lang.EN) + " §e" + target.getRankPoints());
            //sender.sendMessage(generalLocalization.get("name_coins", Lang.EN) + " §e" + target.getCoins());
            //sender.sendMessage(generalLocalization.get("name_gems", Lang.EN) + " §e" + target.getGems());
            return true;
        }
        // only player
        Profile profile = requireOnlyPlayer(sender);
        if (profile == null)
            return true;
        Player player = (Player) sender;

        Lang lang = profile.getLang();
        if (args.length > 0) {
            String targetName = args[0];
            Player targetPlayer = Bukkit.getPlayer(targetName);
            if (targetPlayer == null) {
                sender.sendMessage(generalLocalization.get("player_invalid", profile.getLang()));
                return true;
            }
            Profile target = instance.getProfileRegistry().getProfile(targetPlayer.getUniqueId());

            player.openInventory(instance.getProfileRegistry().getProfileGUI().generateGUIProfile(lang, target, targetPlayer, (!targetName.equalsIgnoreCase(player.getName()))));

        } else {
            player.openInventory(instance.getProfileRegistry().getProfileGUI().generateGUIProfile(lang, profile, player, false));
        }

        return true;
    }

    @Override
    public List<String> executeTabComplete(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) {
        if (args.length == 1)
            return Bukkit.getOnlinePlayers().stream().map(Player::getName).collect(Collectors.toList());
        return null;
    }
}
