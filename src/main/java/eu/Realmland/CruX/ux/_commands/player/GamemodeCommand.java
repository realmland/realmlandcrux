package eu.Realmland.CruX.ux._commands.player;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.localization.Localization;
import eu.Realmland.CruX.ui.commands.model.Command;
import eu.Realmland.CruX.ui.commands.model.CommandGoods;
import eu.Realmland.CruX.ux.profiles.model.Profile;
import org.apache.commons.lang.ArrayUtils;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class GamemodeCommand extends Command {

    @CommandGoods(name = "gamemode", permission = "realmland.admin.gamemode", aliases = {"gm", "gmc", "gms", "gma", "gmsp"})
    public GamemodeCommand(@NotNull CruX instance) {
        super(instance);
    }

    @Override
    public boolean executeCommand(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) {
        GameMode mode = null;

        if (alias.equalsIgnoreCase("gmc")) mode = GameMode.CREATIVE;
        else if (alias.equalsIgnoreCase("gms")) mode = GameMode.SURVIVAL;
        else if (alias.equalsIgnoreCase("gma")) mode = GameMode.ADVENTURE;
        else if (alias.equalsIgnoreCase("gmsp")) mode = GameMode.SPECTATOR;


        if (args.length > 0) {
            // read from arguments if already not specified
            if (mode == null)
                try {
                    String modeName = args[0].toUpperCase();
                    args = (String[]) ArrayUtils.remove(args, 0);

                    // maybe its shortcut? try
                    switch (modeName) {
                        case "C":
                            mode = GameMode.CREATIVE;
                            break;
                        case "1":
                            mode = GameMode.CREATIVE;
                            break;
                        case "A":
                            mode = GameMode.ADVENTURE;
                            break;
                        case "2":
                            mode = GameMode.ADVENTURE;
                            break;
                        case "S":
                            mode = GameMode.SURVIVAL;
                            break;
                        case "0":
                            mode = GameMode.SURVIVAL;
                            break;
                        case "SP":
                            mode = GameMode.SPECTATOR;
                            break;
                        case "3":
                            mode = GameMode.SPECTATOR;
                            break;
                    }

                    // well what if not
                    if (mode == null)
                        mode = GameMode.valueOf(modeName);

                } catch (IllegalArgumentException x) {
                    sender.sendMessage(Localization.Prefix.ERROR + "Unknown Gamemode '" + args[0] + "'");
                    return true;
                }
        }

        if (mode == null) {
            sender.sendMessage(Localization.Prefix.ERROR + "You must specify gamemode");
            return true;

        }

        if(args.length > 0) {
            String targetName = args[0];
            Player target = Bukkit.getPlayer(targetName);

            if(!sender.hasPermission("realmland.admin.gamemode.others")) {
                sender.sendMessage(Localization.Prefix.ERROR + "You don't have enough permissions.");
                return true;
            }

            if(target != null) {

                if (target.getGameMode().equals(mode)) {
                    sender.sendMessage(Localization.Prefix.ERROR + "He already is in this gamemode! Chumaj:D");
                    return true;
                }

                logger.debug("Player '" + sender.getName() + "' has changed " + targetName + "'s gamemode to '§e" + mode.name().toLowerCase() + "§f' from '§e" + target.getGameMode().name().toLowerCase() + "§f'");
                target.setGameMode(mode);
                sender.sendMessage(Localization.Prefix.SUCCESS + "Gamemode of " + targetName + " changed to '§e" + mode.name().toLowerCase() + "§f'");
                return true;
            } else {
                sender.sendMessage(Localization.Prefix.ERROR + "Invalid player!");
                return true;
            }
        }

        Profile profile = requireOnlyPlayer(sender);
        if(profile == null)
            return true;

        Player p = (Player) sender;

        if (p.getGameMode().equals(mode)) {
            sender.sendMessage(Localization.Prefix.ERROR + "You already are in this gamemode! Chumaj:D");
            return true;
        }



        logger.debug("Player '" + p.getName() + "' has changed his gamemode to '§e" + mode.name().toLowerCase() + "§f' from '§e" + p.getGameMode().name().toLowerCase() + "§f'");
        p.setGameMode(mode);
        sender.sendMessage(Localization.Prefix.SUCCESS + "Gamemode changed to '§e" + mode.name().toLowerCase() + "§f'");


        return true;
    }

    @Override
    public List<String> executeTabComplete(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) {

        if (args.length == 1) {
            if(alias.equalsIgnoreCase("gmc") || alias.equalsIgnoreCase("gms") || alias.equalsIgnoreCase("gma") || alias.equalsIgnoreCase("gmsp")) {
                return Bukkit.getOnlinePlayers().stream().map(Player::getName).collect(Collectors.toList());
            }
            return Arrays.asList("creative", "survival", "adventure", "spectator");
        }
        if(args.length == 2)
            return Bukkit.getOnlinePlayers().stream().map(Player::getName).collect(Collectors.toList());
        return new ArrayList<>();
    }
}
