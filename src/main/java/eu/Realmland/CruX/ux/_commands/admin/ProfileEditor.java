package eu.Realmland.CruX.ux._commands.admin;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.localization.Lang;
import eu.Realmland.CruX.localization.Localization;
import eu.Realmland.CruX.ui.commands.model.Command;
import eu.Realmland.CruX.ui.commands.model.CommandGoods;
import eu.Realmland.CruX.ux.profiles.model.Profile;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class ProfileEditor extends Command {

    @CommandGoods(name = "profileeditor", aliases = {"pe", "profileditor", "editprofile"}, permission = "realmland.admin.profile-editor")
    public ProfileEditor(@NotNull CruX instance) {
        super(instance);
    }

    @Override
    public boolean executeCommand(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) {

        if (args.length > 0) {

            String playerName = args[0];
            if(playerName.equalsIgnoreCase("checkPlayers")) {
                TextComponent missing = new TextComponent("");
                Iterator<? extends Player> players = Bukkit.getOnlinePlayers().iterator();
                while (players.hasNext()) {
                    Player next = players.next();
                    if(next.isOnline()) {
                        Profile nextProfile = instance.getProfileRegistry().getProfile(next.getUniqueId());
                        if(nextProfile == null) {
                            if(players.hasNext())
                                missing.addExtra(next.getName() + ", ");
                            else
                                missing.addExtra(next.getName());
                        }
                    }
                }
                sender.spigot().sendMessage((missing.getText().isEmpty() ? new TextComponent(Localization.Prefix.ERROR + "There are no missing profiles.") : missing));
                return true;
            }
            Player player = Bukkit.getPlayer(playerName);
            if (player == null) {
                sender.sendMessage(Localization.Prefix.ERROR + "Invalid player");
                return true;
            }

            Profile profile = instance.getProfileRegistry().getProfile(player.getUniqueId());

            if (args.length > 1) {
                String action = args[1];

                if (args.length > 2) {
                    String value = args[2];


                    /**
                     * Rank points
                     */
                    if (action.equalsIgnoreCase("addRP")) {
                        int val = Integer.valueOf(value);

                        profile.addRankPoints(val);
                        sender.sendMessage(Localization.Prefix.SUCCESS + "Added RP.");
                        return true;
                    }
                    if (action.equalsIgnoreCase("removeRP")) {
                        int val = Integer.valueOf(value);

                        profile.removeRankPoints(val);
                        sender.sendMessage(Localization.Prefix.SUCCESS + "Removed RP.");
                        return true;
                    }
                    if (action.equalsIgnoreCase("setRP")) {
                        int val = Integer.valueOf(value);

                        profile.setRankPoints(val);
                        sender.sendMessage(Localization.Prefix.SUCCESS + "Set RP.");
                        return true;
                    }

                    /**
                     * Coins
                     */
                    if (action.equalsIgnoreCase("addCoins")) {
                        int val = Integer.valueOf(value);

                        ///profile.addCoins(val);
                        sender.sendMessage(Localization.Prefix.SUCCESS + "Added Coins.");
                        return true;
                    }
                    if (action.equalsIgnoreCase("removeCoins")) {
                        int val = Integer.valueOf(value);

                        //profile.removeCoins(val);
                        sender.sendMessage(Localization.Prefix.SUCCESS + "Removed Coins.");
                        return true;
                    }
                    if (action.equalsIgnoreCase("setCoins")) {
                        int val = Integer.valueOf(value);

                        //profile.setCoins(val);
                        sender.sendMessage(Localization.Prefix.SUCCESS + "Set Coins.");
                        return true;
                    }

                    /**
                     * Gems
                     */
                    if (action.equalsIgnoreCase("addGems")) {
                        int val = Integer.valueOf(value);

                        //profile.addGems(val);
                        sender.sendMessage(Localization.Prefix.SUCCESS + "Added Gems.");
                        return true;
                    }
                    if (action.equalsIgnoreCase("removeGems")) {
                        int val = Integer.valueOf(value);

                        //profile.removeGems(val);
                        sender.sendMessage(Localization.Prefix.SUCCESS + "Removed Gems.");
                        return true;
                    }
                    if (action.equalsIgnoreCase("setGems")) {
                        int val = Integer.valueOf(value);

                        //profile.setGems(val);
                        sender.sendMessage(Localization.Prefix.SUCCESS + "Set Gems.");
                        return true;
                    }

                    /**
                     * Staff
                     */
                    if (action.equalsIgnoreCase("setStaff")) {
                        boolean val;
                        if (value.equalsIgnoreCase("t"))
                            val = true;
                        else if (value.equalsIgnoreCase("f"))
                            val = false;
                        else
                            val = Boolean.valueOf(value);

                        profile.setStaff(val);
                        sender.sendMessage(Localization.Prefix.SUCCESS + "Staff set.");

                        return true;
                    }

                    /**
                     * Staff
                     */
                    if (action.equalsIgnoreCase("setDiscordID")) {
                        if (value.length() > 36) {
                            sender.sendMessage(Localization.Prefix.ERROR + "Discord ID is too lengthy!");
                            return true;
                        }
                        profile.setDiscordID(value);
                        sender.sendMessage(Localization.Prefix.SUCCESS + "Discord ID set.");

                        return true;
                    }

                    /**
                     * Lang
                     */
                    if (action.equalsIgnoreCase("setLang")) {
                        Lang lang = Lang.of(value);
                        if(lang == null) {
                            sender.sendMessage(Localization.Prefix.ERROR + "Invalid lang.");
                            return true;
                        }

                        profile.setLang(lang);
                        sender.sendMessage(Localization.Prefix.SUCCESS + "Lang set.");
                        return true;
                    }

                } else
                    sender.sendMessage(Localization.Prefix.ERROR + "Specify value");

            } else
                sender.sendMessage(Localization.Prefix.ERROR + "Unknown action");


        } else
            sender.sendMessage(Localization.Prefix.ERROR + "Specify player.");

        return true;
    }

    @Override
    public List<String> executeTabComplete(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) {
        if (args.length == 1) {
            List<String> complete = Bukkit.getOnlinePlayers().stream().map(Player::getName).collect(Collectors.toList());
            complete.add("checkPlayers");
            return complete;
        }
        if (args.length == 2)
            return Arrays.asList("addRP", "removeRP", "setDiscordID", "setRP", "addCoins", "removeCoins", "setCoins", "addGems", "removeGems", "setGems", "setStaff");
        return emptyTabComplete();
    }
}
