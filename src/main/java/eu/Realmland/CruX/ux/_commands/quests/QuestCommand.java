package eu.Realmland.CruX.ux._commands.quests;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.ui.commands.model.Command;
import eu.Realmland.CruX.ui.commands.model.CommandGoods;
import eu.Realmland.CruX.ux.profiles.model.Profile;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionDefault;
import org.jetbrains.annotations.NotNull;

public class QuestCommand extends Command {

    @CommandGoods(name = "quests", aliases = {"jobs"},permission = "realmland.player.quests", permissionDefault = PermissionDefault.TRUE)
    public QuestCommand(@NotNull CruX instance) {
        super(instance);
    }

    @Override
    public boolean executeCommand(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) {
        Profile profile = requireOnlyPlayer(sender);
        if(profile == null)
            return true;

        instance.getQuestRegistry().getGui().showTo((Player) sender, profile, profile.getLang());
        return true;
    }
}
