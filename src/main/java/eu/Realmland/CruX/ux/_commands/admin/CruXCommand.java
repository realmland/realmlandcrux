package eu.Realmland.CruX.ux._commands.admin;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.localization.Localization;
import eu.Realmland.CruX.ui.commands.model.Command;
import eu.Realmland.CruX.ui.commands.model.CommandGoods;
import eu.Realmland.CruX.ui.gui.custom.InfoGUI;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.minecraft.server.v1_14_R1.ChatMessage;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_14_R1.CraftServer;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class CruXCommand extends Command {

    @CommandGoods(name = "cruX")
    public CruXCommand(@NotNull CruX instance) {
        super(instance);
    }

    @Override
    public boolean executeCommand(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) {
        if (args.length > 0) {
            String cmd = args[0];
            if (cmd.equalsIgnoreCase("reloadConfig")) {
                instance.reloadConfig();
                instance.getControlCenter().getInterfaceService().loadConfig();
                InfoGUI.reloadConfig();

                sender.sendMessage(Localization.Prefix.SUCCESS + "Reloaded configs.");
                return true;
            } else if (cmd.equalsIgnoreCase("updateMotd")) {
                ((CraftServer) Bukkit.getServer()).getHandle().getServer().setMotd(((CraftServer) Bukkit.getServer()).getHandle().getServer().propertyManager.getProperties().motd);
                sender.sendMessage(Localization.Prefix.SUCCESS + "Updated MOTD.");
                return true;
            } else if (cmd.equalsIgnoreCase("updateSlots")) {
                System.out.println(new ChatMessage("multiplayer.player.joined").getText());
                return true;
            } else if (cmd.equalsIgnoreCase("reloadLocale")) {
                instance.getGeneralLocalization().loadMessages();
                instance.getCommandsLocalization().loadMessages();
                instance.getQuestRegistry().getLocalization().loadMessages();
                sender.sendMessage(Localization.Prefix.SUCCESS + "Reloaded localizations.");
                return true;
            } else if (cmd.equalsIgnoreCase("killChairs")) {
                AtomicInteger integer = new AtomicInteger(0);
                instance.getControlCenter().getSitting().getEntites().forEach(entity -> {
                    try {
                        Bukkit.getEntity(entity).remove();
                        integer.addAndGet(1);
                    } catch (NullPointerException ignored) {}
                });
                sender.sendMessage(Localization.Prefix.INFO + "Killed " + integer + " chairs.");
                return true;
            } else if (cmd.equalsIgnoreCase("updateTablist")) {
                instance.getControlCenter().getInterfaceService().updateTablist();
                sender.sendMessage(Localization.Prefix.SUCCESS + "Updated tablist.");
                return true;
            } else if (cmd.equalsIgnoreCase("database")) {
                sender.sendMessage(Localization.Prefix.INFO + "Database connected to " + database.getProtocol().getConfiguration().getJdbcUrl());
                sender.sendMessage(Localization.Prefix.INFO + "Is ActiveConnection alright? " + (database.getActiveConnection() != null ? "§ayes" : "§cfuck no"));
                return true;
            } else if (cmd.equalsIgnoreCase("restartInterface")) {

                try {
                    instance.getControlCenter().getInterfaceService().terminate();
                } catch (Exception e) {
                    logger.error("Failed to terminate", e);
                    sender.sendMessage(Localization.Prefix.ERROR + "Failed to terminate; See console");
                }

                try {
                    instance.getControlCenter().getInterfaceService().initialize();
                } catch (Exception e) {
                    logger.error("Failed to initialize", e);
                    sender.sendMessage(Localization.Prefix.ERROR + "Failed to initialize; See console");
                }

                sender.sendMessage(Localization.Prefix.SUCCESS + "Restarted interface.");
                return true;
            } else if (cmd.equalsIgnoreCase("updateScoreboard")) {
                instance.getControlCenter().getInterfaceService().updateScoreboard();
                sender.sendMessage(Localization.Prefix.SUCCESS + "Updated scoreboard.");
                return true;
            } else if (cmd.equalsIgnoreCase("getUUID")) {

                if (args.length > 1) {
                    String name = args[1];
                    OfflinePlayer player = Bukkit.getOfflinePlayer(name);
                    sender.spigot().sendMessage(generateClickable(player.getUniqueId().toString()));
                } else
                    sender.sendMessage(Localization.Prefix.ERROR + "Specify player");
                return true;
            } else if (cmd.equalsIgnoreCase("getName")) {
                if (args.length > 1) {
                    String uuid = args[1];
                    OfflinePlayer player = Bukkit.getOfflinePlayer(UUID.fromString(uuid));
                    sender.spigot().sendMessage(generateClickable(player.getName()));
                } else
                    sender.sendMessage(Localization.Prefix.ERROR + "Specify player");
                return true;
            } else if (cmd.equalsIgnoreCase("getIP")) {

                if (args.length > 1) {
                    String name = args[1];
                    Player player = Bukkit.getPlayer(name);
                    if (player == null) {
                        sender.sendMessage(Localization.Prefix.ERROR + "Specify online player");
                        return true;
                    }
                    sender.spigot().sendMessage(generateClickable(player.getAddress().toString().replaceFirst("^/", "")));
                } else
                    sender.sendMessage(Localization.Prefix.ERROR + "Specify player");
                return true;
            } else {
                sender.sendMessage(Localization.Prefix.ERROR + "Unknown action.");
                return true;
            }
        }
        sender.sendMessage(Localization.Prefix.INFO + "/cruX <§ereloadConfig§f, §ereloadLocalization§f>");
        return true;
    }

    protected TextComponent generateClickable(String string) {
        TextComponent ip = new TextComponent(Localization.Prefix.INFO + string + " §7(Click to Copy)");
        ip.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, string));
        return ip;
    }

    @Override
    public List<String> executeTabComplete(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) {
        if (args.length == 1)
            return Arrays.asList("reloadConfig", "reloadLocale", "getName", "getUUID", "updateTablist", "updateScoreboard", "restartInterface", "getIP", "database", "updateMotd", "killChairs");
        if (args.length == 2) {
            List<String> all = Bukkit.getOnlinePlayers().stream().map(Player::getName).collect(Collectors.toList());
            for (OfflinePlayer player : Bukkit.getOfflinePlayers()) {
                all.add(player.getName());
            }
            return all;
        }
        return emptyTabComplete();
    }
}
