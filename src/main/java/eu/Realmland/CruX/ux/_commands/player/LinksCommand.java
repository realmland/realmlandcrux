package eu.Realmland.CruX.ux._commands.player;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.ui.commands.model.Command;
import eu.Realmland.CruX.ui.commands.model.CommandGoods;
import org.bukkit.command.CommandSender;
import org.bukkit.permissions.PermissionDefault;
import org.jetbrains.annotations.NotNull;

public class LinksCommand extends Command {

    @CommandGoods(name = "links", aliases = {"discord", "ds", "web", "website", "ts", "teamspeak", "ts3"}, permission = "realmland.player.links", permissionDefault = PermissionDefault.TRUE)
    public LinksCommand(@NotNull CruX instance) {
        super(instance);
    }

    @Override
    public boolean executeCommand(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) {

        if (alias.equalsIgnoreCase("discord") || alias.equalsIgnoreCase("ds") || alias.equalsIgnoreCase("links")) {
            sender.sendMessage("§f");
            sender.sendMessage("§9§lDiscord Link #1 §fdiscord.realmland.eu");
            sender.sendMessage("§9§lDiscord Link #2 §fdiscord.gg/QqkHxqX");

        }
        if (alias.equalsIgnoreCase("website") || alias.equalsIgnoreCase("web") || alias.equalsIgnoreCase("links")) {
            sender.sendMessage("§f");
            sender.sendMessage("§e§lWeb §frealmland.eu");

        }
        if (alias.equalsIgnoreCase("ts") || alias.equalsIgnoreCase("teamspeak") || alias.equalsIgnoreCase("ts3") || alias.equalsIgnoreCase("links")) {
            sender.sendMessage("§f");
            sender.sendMessage("§6§lIp §ftsko.pro");
            sender.sendMessage("§6§lWeb §ftsko.pro");
        }
        return true;
    }

}
