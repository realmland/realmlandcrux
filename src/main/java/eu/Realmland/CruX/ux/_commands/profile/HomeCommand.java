package eu.Realmland.CruX.ux._commands.profile;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.localization.Lang;
import eu.Realmland.CruX.ui.commands.model.Command;
import eu.Realmland.CruX.ui.commands.model.CommandGoods;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;

public class HomeCommand extends Command {

    @CommandGoods(name = "home", aliases = "lang", permission = "realmland.player.home")
    public HomeCommand(@NotNull CruX instance) {
        super(instance);
    }

    @Override
    public boolean executeCommand(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) {
        // only player
        if(!isPlayer) {
            sender.sendMessage(generalLocalization.get("only_player", Lang.EN));
            return true;
        }



        return true;
    }

}
