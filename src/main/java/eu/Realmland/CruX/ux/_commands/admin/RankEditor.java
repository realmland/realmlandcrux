package eu.Realmland.CruX.ux._commands.admin;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.localization.Localization;
import eu.Realmland.CruX.ui.commands.model.Command;
import eu.Realmland.CruX.ui.commands.model.CommandGoods;
import eu.Realmland.CruX.ux.ranks.model.Rank;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class RankEditor extends Command {

    @CommandGoods(name = "rankeditor", aliases = {"re", "editrank"}, permission = "realmland.admin.rank-editor")
    public RankEditor(@NotNull CruX instance) {
        super(instance);
    }

    @Override
    public boolean executeCommand(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) {

        if(args.length > 0) {
            String sub = args[0];
            if(sub.equalsIgnoreCase("loadRanks")) {
                instance.getRankRegistry().loadRanks();
                sender.sendMessage(Localization.Prefix.SUCCESS + "Ranks loaded.");
                return true;
            }
            if(sub.equalsIgnoreCase("getRanks")) {
                sender.sendMessage(Localization.Prefix.INFO + instance.getRankRegistry().getRanks().values().stream().map(Rank::getName).collect(Collectors.toList()).toString());
                return true;
            }
            if(sub.equalsIgnoreCase("getRank")) {
                if(args.length > 1) {
                    String name = args[1];

                    Rank r = instance.getRankRegistry().getRank(name);
                    sender.sendMessage(Localization.Prefix.INFO + r.toString() + "") ;
                } else
                    sender.sendMessage(Localization.Prefix.ERROR + "Specify rank name!");
                return true;
            }
            else {
                sender.sendMessage(Localization.Prefix.ERROR + "Unknown action!");
            }
        } else
            sender.sendMessage(Localization.Prefix.ERROR + "Specify action!");

        return true;
    }

    @Override
    public List<String> executeTabComplete(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) {

        return Arrays.asList("loadRanks", "getRanks", "getRank");
    }
}
