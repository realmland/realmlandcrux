package eu.Realmland.CruX.ux._commands.profile;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.ui.commands.model.Command;
import eu.Realmland.CruX.ui.commands.model.CommandGoods;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.permissions.PermissionDefault;
import org.jetbrains.annotations.NotNull;

public class BalanceCommand extends Command {

    @CommandGoods(name = "balance", aliases = {"coins", "gems", "money"}, permission = "realmland.player.profile", permissionDefault = PermissionDefault.TRUE)
    public BalanceCommand(@NotNull CruX instance) {
        super(instance);
    }

    @Override
    public boolean executeCommand(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) {

        try {
            return Bukkit.getPluginCommand("profile").execute(sender, alias, args);
        } catch (NullPointerException x) {
            sender.sendMessage("OOps... Use /profile command.");
        }

        return true;
    }
}
