package eu.Realmland.CruX.ux._commands.profile;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.localization.Lang;
import eu.Realmland.CruX.ui.commands.model.Command;
import eu.Realmland.CruX.ui.commands.model.CommandGoods;
import eu.Realmland.CruX.ux.profiles.model.Profile;
import org.bukkit.command.CommandSender;
import org.bukkit.permissions.PermissionDefault;
import org.jetbrains.annotations.NotNull;

public class LanguageCommand extends Command {

    @CommandGoods(name = "lang", aliases = {"language"}, permission = "realmland.player.language", permissionDefault = PermissionDefault.TRUE)
    public LanguageCommand(@NotNull CruX instance) {
        super(instance);
    }

    @Override
    public boolean executeCommand(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) {
        // only player
        Profile profile = requireOnlyPlayer(sender);
        if (profile == null)
            return true;

        if(args.length > 0) {
            String lang = args[0].toLowerCase();
            Lang actual = null;
            for(Lang next : Lang.values()) {
                if(next.name().toUpperCase().equalsIgnoreCase(lang))
                    actual = next;
            }

            if(actual == null) {
                sender.sendMessage(generalLocalization.get("language_invalid", profile.getLang()));
                sender.sendMessage(commandsLocalization.get("language_hint", profile.getLang()));
                return true;
            }
            profile.setLang(actual);
            sender.sendMessage(generalLocalization.get("language_set", profile.getLang(), actual.name().toUpperCase()));
        } else {
            sender.sendMessage(commandsLocalization.get("language_desc", profile.getLang()));
            sender.sendMessage(commandsLocalization.get("language_hint", profile.getLang()));
            sender.sendMessage(generalLocalization.get("your_language", profile.getLang(), profile.getLang().name().toUpperCase()));
            return true;
        }

        return true;
    }

}
