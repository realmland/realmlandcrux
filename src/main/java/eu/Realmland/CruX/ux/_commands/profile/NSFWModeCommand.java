package eu.Realmland.CruX.ux._commands.profile;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.localization.Lang;
import eu.Realmland.CruX.ui.commands.model.Command;
import eu.Realmland.CruX.ui.commands.model.CommandGoods;
import eu.Realmland.CruX.ux.profiles.model.Profile;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionDefault;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;

public class NSFWModeCommand extends Command {

    @CommandGoods(name = "nsfw", permission = "realmland.player.nsfwmode", permissionDefault = PermissionDefault.TRUE)
    public NSFWModeCommand(@NotNull CruX instance) {
        super(instance);
    }


    @Override
    public boolean executeCommand(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) {
        // only player
        if(!isPlayer) {
            sender.sendMessage(generalLocalization.get("only_player", Lang.EN));
            return true;
        }

        Player p = (Player) sender;
        Profile profile = instance.getProfileRegistry().getProfile(p.getUniqueId());

        if(args.length > 0) {
            if(args[0].equalsIgnoreCase("info")) {
                p.sendMessage(generalLocalization.get("nsfw_info", profile.getLang()));

                if(profile.isNsfw()) {
                    p.sendMessage(generalLocalization.get("nsfw_mode_enabled", profile.getLang()));
                } else {
                    p.sendMessage(generalLocalization.get("nsfw_mode_disabled", profile.getLang()));
                }
                return true;
            }
        }

        if(profile.isNsfw()) {
            p.sendMessage(generalLocalization.get("nsfw_mode_disabled", profile.getLang()));
            profile.setNsfw(false);
        } else {
            p.sendMessage(generalLocalization.get("nsfw_mode_enabled", profile.getLang()));
            profile.setNsfw(true);
        }
        return true;
    }

    @Override
    public List<String> executeTabComplete(@NotNull CommandSender sender, @NotNull String alias,  @NotNull String[] args, boolean isPlayer) {
        return Arrays.asList("info");
    }
}
