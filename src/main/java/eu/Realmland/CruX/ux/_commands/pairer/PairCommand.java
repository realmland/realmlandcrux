package eu.Realmland.CruX.ux._commands.pairer;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.ui.commands.model.Command;
import eu.Realmland.CruX.ui.commands.model.CommandGoods;
import eu.Realmland.CruX.ux.profiles.model.Profile;
import org.bukkit.command.CommandSender;
import org.bukkit.permissions.PermissionDefault;
import org.jetbrains.annotations.NotNull;

public class PairCommand extends Command {

    @CommandGoods(name = "pair", aliases = {"connect", "code"}, permission = "realmland.player.pair", permissionDefault = PermissionDefault.TRUE)
    public PairCommand(@NotNull CruX instance) {
        super(instance);
    }

    @Override
    public boolean executeCommand(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) {
        Profile profile = requireOnlyPlayer(sender);
        if(profile == null)
            return true;

        int code = instance.getPairRegistry().generateCode();
        instance.getPairRegistry().registerCode(code, profile.getUuid());

        sender.sendMessage(commandsLocalization.get("pair_desc", profile.getLang()));
        sender.sendMessage(generalLocalization.get("pairer_code", profile.getLang(), code));

        return true;
    }
}
