package eu.Realmland.CruX.ux._commands.info;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.ui.commands.model.Command;
import eu.Realmland.CruX.ui.commands.model.CommandGoods;
import eu.Realmland.CruX.ui.gui.custom.InfoGUI;
import eu.Realmland.CruX.ux.profiles.model.Profile;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionDefault;
import org.jetbrains.annotations.NotNull;

public class InfoCommand extends Command {


    private InfoGUI infoGUI;

    @CommandGoods(name = "help", aliases = {"info", "pomoc"}, permission = "realmland.player.info", permissionDefault = PermissionDefault.TRUE)
    public InfoCommand(@NotNull CruX instance) {
        super(instance);
        infoGUI = new InfoGUI(instance);
    }

    @Override
    public boolean executeCommand(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) {
        Profile profile = requireOnlyPlayer(sender);
        if (profile == null)
            return true;

        infoGUI.showTo((Player) sender, profile);
        return true;
    }
}
