package eu.Realmland.CruX.ux._commands.player;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.ui.commands.model.Command;
import eu.Realmland.CruX.ui.commands.model.CommandGoods;
import eu.Realmland.CruX.ux.profiles.model.Profile;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.stream.Collectors;

public class RandomTeleport extends Command {

    @CommandGoods(name = "randomteleport", aliases = {"rtp", "randomlocation", "ranloc"})
    public RandomTeleport(@NotNull CruX instance) {
        super(instance);
    }

    private Random random = new Random();

    private HashMap<UUID, Long> timeout = new HashMap<>();

    @Override
    public boolean executeCommand(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) {
        Profile profile = requireOnlyPlayer(sender);
        if (profile == null)
            return true;
        Player player = (Player) sender;

        boolean spawned = false;

        do {
            int x = generate();
            int z = generate();
            int y = player.getWorld().getHighestBlockYAt(x, z);

            if (isSafe(x, y, z, player.getWorld())) {
                player.teleport(new Location(player.getWorld(), x, y+1, z));
                spawned = true;
            }
        } while (!spawned);
        return true;
    }

    public int generate() {
        int v = random.nextInt(instance.getConfig().getInt("random-location-settings.bound", 100000));

        boolean negateV = random.nextBoolean();
        return (negateV ? v * -1 : v);
    }

    public boolean isSafe(int x, int y, int z, World world) {
        List<Material> materialList = instance.getConfig().getList("random-location-settings.unsafe-spawn", Arrays.asList("LAVA", "WATER"))
                .stream().map(Object::toString).map(String::toUpperCase).map(materialName -> {
                    try {
                        return Material.valueOf(materialName);
                    } catch (Exception ignored) {}
                    return null;
        }).filter(Objects::nonNull).collect(Collectors.toList());

        if(materialList.contains(world.getBlockAt(x, y, z).getType())) {
            return false;
        }
        return true;
    }


}
