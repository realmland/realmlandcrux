package eu.Realmland.CruX.ux._commands.admin;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.ui.commands.model.Command;
import eu.Realmland.CruX.ui.commands.model.CommandGoods;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;

public class EndpointCommand extends Command {

    @CommandGoods(name = "endpoint", aliases = {}, permission = "realmland.admin.endpoint")
    public EndpointCommand(@NotNull CruX instance) {
        super(instance);
    }

    @Override
    public boolean executeCommand(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) {
     /*   TokenRegistry registry = instance.getEndpoint().getTokenRegistry();

        if(args.length > 0) {
            String action = args[0];
            if(action.equalsIgnoreCase("generateToken")) {
                String token = registry.generateToken();

                TextComponent response = new TextComponent(Localization.Prefix.SUCCESS + "Generated token §e" + token + " §7(Click to copy)");
                response.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "token::"+token));

                sender.spigot().sendMessage(response);
            } else if(action.equalsIgnoreCase("registerToken")) {
                if(args.length > 1) {
                    String token = args[1];
                    boolean success = registry.registerToken(token);
                    sender.sendMessage((success ?
                            (Localization.Prefix.SUCCESS + "Successfully registered.") :
                            ((Localization.Prefix.ERROR + "Failed to register. §7(Perhaps it is already in system?)"))));
                } else
                    sender.sendMessage(Localization.Prefix.ERROR + "Specify token.");
            } else if(action.equalsIgnoreCase("unregisterToken")) {
                if(args.length > 1) {
                    String token = args[1];
                    boolean success = registry.unregisterToken(token);
                    sender.sendMessage((success ?
                            (Localization.Prefix.SUCCESS + "Successfully unregistered.") :
                            ((Localization.Prefix.ERROR + "Failed to unregister."))));
                } else
                    sender.sendMessage(Localization.Prefix.ERROR + "Specify token.");
            } else if(action.equalsIgnoreCase("getTokens")) {
                TextComponent builder = new TextComponent();
                if(registry.getTokens().size() == 0) {
                    sender.sendMessage(Localization.Prefix.ERROR + "There are no tokens.");
                    return true;
                }
                registry.getTokens().forEach(token -> {
                    TextComponent tokenComp = new TextComponent("§e" + token + "§7, ");
                    tokenComp.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "token::"+token));

                    builder.addExtra(tokenComp);
                });
                sender.spigot().sendMessage(builder);
            } else if(action.equalsIgnoreCase("isTokenRegistered")) {
                if(args.length > 1) {
                    String token = args[1];
                    sender.sendMessage((registry.isTokenRegistered(token) ? Localization.Prefix.SUCCESS + "This token is registered." : Localization.Prefix.ERROR + "This token is not registered."));
                } else
                    sender.sendMessage(Localization.Prefix.ERROR + "Specify token.");
            } else {
                sender.sendMessage(Localization.Prefix.ERROR + "Unknown action");
            }
        } else
            sender.sendMessage(Localization.Prefix.ERROR + "Specify action.");
*/
        return true;
    }

    @Override
    public List<String> executeTabComplete(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) {
        if(args.length == 1)
            return Arrays.asList("generateToken", "registerToken", "unregisterToken", "isTokenRegistered", "getTokens");
        return emptyTabComplete();
    }
}
