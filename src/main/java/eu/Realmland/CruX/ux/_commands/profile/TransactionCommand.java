package eu.Realmland.CruX.ux._commands.profile;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.ui.commands.model.Command;
import eu.Realmland.CruX.ui.commands.model.CommandGoods;
import eu.Realmland.CruX.ux.bank.model.Transaction;
import eu.Realmland.CruX.ux.profiles.model.Profile;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionDefault;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class TransactionCommand extends Command {

    @CommandGoods(name = "transaction", aliases = {"pay", "sendMoney"}, permission = "realmland.player.transaction", permissionDefault = PermissionDefault.TRUE)
    public TransactionCommand(@NotNull CruX instance) {
        super(instance);
    }

    @SuppressWarnings("Duplicates")
    @Override
    public boolean executeCommand(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) {
        Profile profile = requireOnlyPlayer(sender);
        if (profile == null)
            return true;

        Player senderPlayer = (Player) sender;

       /* if (args.length > 0) {
            // receiver
            String receiverName = args[0];

            Player target = Bukkit.getPlayer(receiverName);
            if (target == null) {
                sender.sendMessage(generalLocalization.get("player_invalid", profile.getLang()));
                return true;
            }

            Profile receiver = instance.getProfileRegistry().getProfile(target.getUniqueId());
            if (receiver == null) {
                sender.sendMessage(generalLocalization.get("player_invalid", profile.getLang()));
                return true;
            }

            // type
            if (args.length > 1) {
                String typeName = args[1].toLowerCase();
                Transaction.Type type = null;
                switch (typeName) {
                    case "coins":
                        type = Transaction.Type.COINS;
                        break;
                    case "gems":
                        type = Transaction.Type.GEMS;
                        break;
                }

                if (type == null) {
                    sender.sendMessage(generalLocalization.get("invalid_transaction_type", profile.getLang()));
                    return true;
                }

                if (args.length > 2) {
                    String valueName = args[2];
                    int value;
                    try {
                        value = Integer.valueOf(valueName);
                    } catch (NumberFormatException x) {
                        sender.sendMessage(generalLocalization.get("invalid_value_type", profile.getLang()));
                        return true;
                    }

                    if (!requireBalance(sender, profile, type, value)) {
                        return true;
                    }

                    int id = instance.getBankRegistry().executeTransaction(profile.getBankAccount(), receiver.getBankAccount(), type, value);
                    // server error
                    if (id == -4 || id == -5 || id == -6) {
                        sender.sendMessage(generalLocalization.get("transaction_failed", profile.getLang()));
                        return true;
                    }

                    // invalid transaction
                    if (id == -1) {
                        sender.sendMessage(generalLocalization.get("invalid_transaction_type", profile.getLang()));
                        return true;
                    }

                    // missing balance
                    if (id == -2) {
                        requireBalance(sender, profile, type, value);
                        return true;
                    }

                    if (id == -3) {
                        if (type.equals(Transaction.Type.COINS))
                            sender.sendMessage(generalLocalization.get("transaction_max_coins_other", profile.getLang(), target.getName()));
                        else
                            sender.sendMessage(generalLocalization.get("transaction_max_gems_other", profile.getLang(), target.getName()));
                        return true;
                    }

                    sender.sendMessage(generalLocalization.get("transaction_success", profile.getLang(), id));
                    if (type.equals(Transaction.Type.COINS)) {
                        target.sendMessage(generalLocalization.get("transaction_coins_received", receiver.getLang(), value, senderPlayer.getName(), id));
                    } else {
                        target.sendMessage(generalLocalization.get("transaction_gems_received", receiver.getLang(), value, senderPlayer.getName(), id));
                    }
                    return true;
                } else {
                    sender.sendMessage(generalLocalization.get("specify_value", profile.getLang()));
                    sender.sendMessage(commandsLocalization.get("transaction_usage", profile.getLang(), alias));
                }

            } else {
                sender.sendMessage(generalLocalization.get("specify_transaction_type", profile.getLang()));
                sender.sendMessage(commandsLocalization.get("transaction_usage", profile.getLang(), alias));
            }

        } else {
            sender.sendMessage(generalLocalization.get("specify_player", profile.getLang()));
            sender.sendMessage(commandsLocalization.get("transaction_usage", profile.getLang(), alias));
        }*/
        return true;
    }

    public boolean requireBalance(@NotNull CommandSender sender, @NotNull Profile profile, @NotNull Transaction.Type type, int value) {
        /*if (type.equals(Transaction.Type.COINS))
            if (!profile.hasCoins(value)) {
                sender.sendMessage(generalLocalization.get("transaction_missing_funds_coins", profile.getLang()));
                return false;
            }
        if (type.equals(Transaction.Type.GEMS))
            if (!profile.hasGems(value)) {
                sender.sendMessage(generalLocalization.get("transaction_missing_funds_gems", profile.getLang()));
                return false;
            }*/
        return true;
    }

    @Override
    public List<String> executeTabComplete(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) {
        if (args.length == 1)
            return Bukkit.getOnlinePlayers().stream().map(Player::getName).collect(Collectors.toList());
        if (args.length == 2)
            return Arrays.asList("gems", "coins");

        return super.emptyTabComplete();
    }
}
