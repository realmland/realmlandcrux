package eu.Realmland.CruX.ux.pairer;

import eu.Realmland.CruX.CruX;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

/**
 * Pairs Minecraft account with other applications.
 */
public class PairRegistry {

    @NotNull
    private CruX instance;

    @NotNull
    private Random random;

    @Getter
    private Map<Integer, UUID> codes = new HashMap<>();

    /**
     * Default constructor
     * @param instance Instance
     */
    public PairRegistry(@NotNull CruX instance) {
        this.instance = instance;
        this.random = new Random();
    }

    /**
     * Generates random code
     * @return Code
     */
    public int generateCode() {
        int ran = random.nextInt(99999);

        return (isCodeRegistered(ran) ? generateCode() : ran);
    }

    /**
     * Registers specified code.
     * @param code  Code
     * @param uuid  UUID owner.
     * @return Success/Failure
     */
    public boolean registerCode(int code, @NotNull UUID uuid) {
        if(isCodeRegistered(code))
            return false;
        codes.put(code, uuid);
        return true;
    }

    /**
     * Unregisters specified code
     * @param code
     * @return Success/Failure
     */
    public boolean unregisterCode(int code) {
        if(!isCodeRegistered(code))
            return false;
        codes.remove(code);
        return true;
    }

    /**
     * Checks if code is already registered
     * @param code True if yes, else false
     */
    public boolean isCodeRegistered(int code) {
        return codes.containsKey(code);
    }


    /**
     * Gets code's uuid
     * @param code Code
     * @return UUID
     */
    public UUID getByCode(int code) {
        if(isCodeRegistered(code))
            return codes.get(code);

        return null;
    }
}
