package eu.Realmland.CruX.ux.market.model;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Market {

    @Getter
    private UUID owner;

    @Getter
    private List<UUID> contributors = new ArrayList<>();

    @Getter
    private int coins;

    @Getter
    private int gems;

}
