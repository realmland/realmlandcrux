package eu.Realmland.CruX.ux.bank;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.services.IService;
import eu.Realmland.CruX.ux.bank.model.BankAccount;
import eu.Realmland.CruX.ux.bank.model.Transaction;
import eu.Realmland.CruX.ux.profiles.ProfileRegistry;
import lombok.Getter;
import me.MrWener.RealmLandDatabase.Database;
import me.MrWener.RealmLandLoggerLib.Logger;
import org.jetbrains.annotations.NotNull;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class BankRegistry implements IService {

    private @NotNull CruX instance;
    private @NotNull Logger logger;

    private @NotNull Database database;
    private @NotNull ProfileRegistry profileRegistry;

    /**
     * Transactions
     */
    private final Map<Integer, Transaction> transactions = new HashMap<>();

    private String transactionsTable = "transactions";
    private String accountsTable = "bank_accounts";


    // todo Maybe improve speed?
    @NotNull
    @Getter
    private Connection connection;

    public BankRegistry(@NotNull CruX instance) {
        this.instance = instance;
        this.logger = instance.logger();
        this.database = instance.getDatabase();
        this.profileRegistry = instance.getProfileRegistry();
    }

    @Override
    public void initialize() throws Exception {
        database.executeSQL("create table if not exists `" + transactionsTable + "` (" +
                        "`id` int not null primary key unique," +
                        "`time` bigint not null," +
                        "`type` varchar(5) not null," +
                        "`sender` varchar(36) not null," +
                        "`receiver` varchar(36) not null," +
                        "`amount` int not null"+
                ")");

        database.executeSQL("create table if not exists `" + accountsTable + "` (" +
                "`uuid`   varchar(36) not null primary key unique," +
                "`name`   varchar(36) unique not null," +
                "`holder` varchar(12) not null," +
                "`closed` tinyint(1) not null," +
                "`coins`  int not null," +
                "`gems`   int not null" +
                ")");
    }

    @Override
    public void terminate() throws Exception {

    }

    /**
     * Adds bank account
     *
     * @param uuid UUID of Bank Account
     * @return Bank Account
     */
    private BankAccount addBankAccount(@NotNull UUID uuid) {
        return null;
    }

    private void removeBankAccount(@NotNull UUID uuid) {

    }

    public BankAccount getBankAccount(@NotNull UUID uuid) {
        return null;
    }
}
