package eu.Realmland.CruX.ux.bank.model;

import lombok.Getter;
import lombok.Setter;
import me.MrWener.RealmLandDatabase.payload.Payload;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class BankAccount {

    @Getter
    private UUID uuid;
    @Getter
    private UUID owner;

    @Getter
    private String name;

    @Getter
    private Holder holder;

    @Getter
    @Setter
    private boolean closed;

    @Getter
    private int coins = 0;

    @Getter
    private int gems = 0;


    public BankAccount(@NotNull UUID uuid, @NotNull UUID owner, @NotNull String name, @NotNull Holder holder, boolean closed, int coins, int gems) {
        this.uuid = uuid;
        this.owner = owner;
        this.name = name;
        this.holder = holder;
        this.closed = closed;
        this.coins = coins;
        this.gems = gems;
    }


    /**
     * Adds coins
     *
     * @param val Value
     */
    public void addCoins(int val) {
        coins += val;
    }

    /**
     * Removes coins
     *
     * @param val Value
     */
    public void removeCoins(int val) {
        coins -= val;
    }

    /**
     * Sets coins
     *
     * @param val Value
     */
    public void setCoins(int val) {
        coins = val;
    }

    /**
     * Checks if account has specified number of coins
     *
     * @param val Value
     * @return True if does, False otherwise
     */
    public boolean hasCoins(@NotNull int val) {
        return (this.coins >= val);
    }

    /**
     * Adds gems
     *
     * @param val Value
     */
    public void addGems(int val) {
        gems += val;
    }

    /**
     * Removes gems
     *
     * @param val Value
     */
    public void removeGems(int val) {
        gems -= val;
    }

    /**
     * Sets gems
     *
     * @param val Value
     */
    public void setGems(int val) {
        gems = val;
    }

    /**
     * Checks if account has specified number of gems
     *
     * @param val Value
     * @return True if does, False otherwise
     */
    public boolean hasGems(int val) {
        return (this.gems >= val);
    }


    /**
     * Saves BankAccount to Payload
     *
     * @param account Account
     * @return Payload
     */
    public static Payload toPayload(@NotNull BankAccount account) {
        Payload payload = new Payload();
        payload.addColumn("uuid", account.uuid);
        payload.addColumn("name", account.name);
        payload.addColumn("holder", account.holder.name().toLowerCase());
        payload.addColumn("closed", (account.closed ? 1 : 0));
        payload.addColumn("coins", account.coins);
        payload.addColumn("gems", account.gems);

        return payload;
    }

    /**
     * Loads BankAccount from Payload
     *
     * @param payload Payload
     * @return Loaded BankAccount
     */
    public static BankAccount fromPayload(@NotNull Payload payload) {
        UUID uuid = UUID.fromString(payload.getColumn("uuid").toString());
        UUID owner = UUID.fromString(payload.getColumn("owner").toString());
        String name = payload.getColumn("name");
        Holder holder = Holder.of(payload.getColumn("holder").toString());
        boolean closed = payload.getColumn("closed");
        int coins = payload.getColumn("coins");
        int gems = payload.getColumn("gems");

        return new BankAccount(uuid, owner, name, holder, closed, coins, gems);
    }

    /**
     * @return All payload fields
     */
    public static List<String> getFields() {
        return Arrays.asList("uuid", "owner", "name", "holder", "closed", "coins", "gems");
    }

    /**
     * Represents Account holders
     */
    public static enum Holder {
        PROFILE, MARKET, FARM;

        public static Holder of(@NotNull String string) {
            for (Holder holder : values())
                if (holder.name().equalsIgnoreCase(string)) return holder;

            return null;
        }
    }

}
