package eu.Realmland.CruX.ux.bank.model;

import lombok.Getter;
import me.MrWener.RealmLandDatabase.payload.Payload;

import java.util.UUID;

/**
 * Transaction that happened
 */
public class Transaction {

    @Getter
    private int id;

    @Getter
    private long time;

    @Getter
    private Type transactionType;

    @Getter
    private UUID sender;

    @Getter
    private UUID receiver;

    @Getter
    private int amount;

    /**
     * Default constructor
     * @param id              ID of transaction
     * @param time            Time of transaction
     * @param transactionType Prefix of transaction
     * @param sender          Sender of transaction
     * @param receiver        Receiver of transaction
     * @param amount          Amount sent
     */
    public Transaction(int id, long time, Type transactionType, UUID sender, UUID receiver, int amount) {
        this.id = id;
        this.time = time;
        this.transactionType = transactionType;
        this.receiver = receiver;
        this.sender = sender;
        this.amount = amount;
    }

    public Payload toPayload() {
        Payload payload = new Payload();
        payload.addColumn("id", id);
        payload.addColumn("time", time);
        payload.addColumn("type", transactionType.name());
        payload.addColumn("sender", sender.toString());
        payload.addColumn("receiver", receiver.toString());
        payload.addColumn("amount", amount);
        return payload;
    }


    public enum Type {
        GEMS, COINS
    }
}
