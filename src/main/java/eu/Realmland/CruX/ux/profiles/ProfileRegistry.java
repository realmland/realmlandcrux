package eu.Realmland.CruX.ux.profiles;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.api.AApi;
import eu.Realmland.CruX.localization.Lang;
import eu.Realmland.CruX.localization.Localization;
import eu.Realmland.CruX.services.IService;
import eu.Realmland.CruX.ui.gui.custom.ProfileGUI;
import eu.Realmland.CruX.ux.profiles.model.Profile;
import lombok.Getter;
import me.MrWener.RealmLandDatabase.Database;
import me.MrWener.RealmLandDatabase.payload.Payload;
import me.MrWener.RealmLandLoggerLib.Logger;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * Stores informations about current online profiles
 */
public class ProfileRegistry implements Listener, IService {

    private @NotNull CruX instance;
    private @NotNull Logger logger;

    private @NotNull Database database;
    private @NotNull Localization localization;

    @Getter
    private final HashMap<UUID, Profile> profiles = new HashMap<>();

    @NotNull
    private String table = "profiles";

    @Getter
    private ProfileGUI profileGUI;

    /**
     * Default Construtor
     *
     * @param instance Instance
     */
    public ProfileRegistry(@NotNull CruX instance) {
        this.instance = instance;
        this.database = instance.getDatabase();
        this.logger = instance.logger();
        this.localization = instance.getGeneralLocalization();

        this.profileGUI = new ProfileGUI(instance);
    }

    @Override
    public void initialize() throws Exception {
        Bukkit.getPluginManager().registerEvents(this, instance);

        try {
            database.executeSQL(
                    "CREATE TABLE IF NOT EXISTS `" + table + "` (" +
                            "`id` INT NOT NULL PRIMARY KEY UNIQUE AUTO_INCREMENT," +
                            "`uuid` varchar(36) UNIQUE," +
                            "`name` varchar(64) NOT NULL, " +
                            "`discord_id` varchar(37) UNIQUE DEFAULT ''," +
                            "`rank_points` int NOT NULL," +
                            "`is_staff` BOOLEAN NOT NULL," +
                            "`nsfw_mode` BOOLEAN NOT NULL," +
                            "`profile_data` TEXT NOT NULL," +
                            "`lang` varchar(7) NOT NULL" +
                            ")"
            );
        } catch (SQLException e) {
            logger.error("Oh uh! Failed to create table", e);
            throw e;
        }

        if (!profiles.isEmpty()) {
            File dump = new File(instance.getDataFolder(), "profilesDump - " + LocalDateTime.now() + ".realmShit");
            if (dump.createNewFile()) {

                try (BufferedWriter writer = new BufferedWriter(new FileWriter(dump))) {
                    profiles.forEach((uuid, profile) -> {
                        try {
                            writer.write(Profile.toPayload(profile).getFinalPayload().toString() + "\n");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
                }
                profiles.clear();
            }
        }

        Bukkit.getOnlinePlayers().forEach(this::addProfile);

        logger.debug("Saving profiles every %d minutes", instance.getConfig().getInt("profile-settings.save-time", 15));
        Bukkit.getScheduler().runTaskTimerAsynchronously(instance, () -> {
            getProfiles().forEach((uuid, profile) -> saveProfile(profile));
        }, instance.getConfig().getInt("profile-settings.save-time", 15) * 20 * 60, instance.getConfig().getInt("profile-settings.save-time", 15) * 20 * 60);
    }

    @Override
    public void terminate() throws Exception {
        Bukkit.getOnlinePlayers().forEach(this::removeProfile);
    }

    /**
     * Adds profile for player
     *
     * @param player Player
     * @return Profile
     */
    private @NotNull Profile addProfile(@NotNull Player player) {
        UUID uuid = player.getUniqueId();
        InetSocketAddress address = player.getAddress();

        Profile loaded = loadProfile(uuid);
        if (loaded != null)
            logger.info("Loaded player profile for '%s' from database.", loaded.getName());
        else {
            Lang lang = Lang.EN;
            if (address != null)
                lang = AApi.detectCountry(address);

            loaded = new Profile(uuid, player.getName(), null, 0, false, false, null, lang);
            logger.info("Created player profile for '%s'.", loaded.getName());
        }
        profiles.put(uuid, loaded);
        return loaded;
    }

    /**
     * Removes profile for player
     *
     * @param player Player
     */
    private void removeProfile(@NotNull Player player) {
        removeProfile(player.getUniqueId());
    }

    /**
     * Removes profile
     *
     * @param uuid Profile's uuid
     */
    private void removeProfile(@NotNull UUID uuid) {
        Profile profile = getProfile(uuid);
        if (profile == null) {
            logger.error("Can't save non-existing profile", uuid.toString());
            return;
        }

        saveProfile(profile);
        logger.info("Saved profile for '%s' to database.", profile.getName());
        profiles.remove(uuid);
    }

    /**
     * Gets profile
     *
     * @param uuid UUID of profile
     * @return Profile, Null if profile doesn't exist
     */
    public @Nullable Profile getProfile(@NotNull UUID uuid) {
        Profile saved = profiles.get(uuid);
        if (saved == null) {
            saved = loadProfile(uuid);
            if (saved != null)
                saved.setOffline(true);
        }

        return saved;
    }

    /**
     * Saves profile to database.
     *
     * @param profile Profile uuid
     */
    private void saveProfile(@NotNull Profile profile) {
        try {
            List<Payload> response = database.SQLSelect(table, Profile.getFields(), Profile.getAssurer(profile));
            if (response.size() == 0)
                database.SQLInsert(table, Profile.toPayload(profile));
            else
                database.SQLUpdate(table, Profile.toPayload(profile), Profile.getAssurer(profile));

        } catch (SQLException e) {
            logger.error("Failed to write to database.", e);
        }
    }

    /**
     * Loads profile from database.
     *
     * @param profile Profile uuid
     * @return Profile, Null if profile doesn't exist
     */
    private @Nullable Profile loadProfile(@NotNull UUID profile) {
        try {
            List<Payload> response = database.SQLSelect(table, Profile.getFields(), Profile.getAssurer(profile));
            if (response.size() == 0)
                return null;

            return Profile.fromPayload(response.get(0));
        } catch (SQLException e) {
            logger.error("Failed to read from database.", e);
        }
        return null;
    }

    /**
     * Handles player join event
     *
     * @param e Event
     */
    @EventHandler
    public void onJoin(@NotNull PlayerJoinEvent e) {
        Profile profile = addProfile(e.getPlayer());
        e.setJoinMessage("");

        if (!e.getPlayer().hasPlayedBefore()) {
            logger.info("Player %s has joined for the first time from address '%s'.", e.getPlayer().getName(), e.getPlayer().getAddress());
            instance.getGeneralLocalization().broadcast("player_first_join", e.getPlayer());
        } else if(profile.isStaff()) {
            logger.info("Staff member %s has joined from address '%s'.", e.getPlayer().getName(), e.getPlayer().getAddress());
            instance.getGeneralLocalization().broadcast("player_join_staff", e.getPlayer());
        } else {
            logger.info("Player %s has joined from address '%s'.", e.getPlayer().getName(), e.getPlayer().getAddress());
            instance.getGeneralLocalization().broadcast("player_join", e.getPlayer());
        }
    }

    /**
     * Handles player quit event
     * @param e Event
     */
    @EventHandler
    public void onQuit(@NotNull PlayerQuitEvent e) {
        Profile profile = getProfile(e.getPlayer().getUniqueId());
        e.setQuitMessage("");

        if (profile == null) {
            logger.error("How could offline player left? ProfileRegistry:218");
            return;
        }

        if(profile.isStaff()) {
            logger.info("Staff member %s has quit.", e.getPlayer().getName());
            instance.getGeneralLocalization().broadcast("player_quit_staff", e.getPlayer());
        } else {
            logger.info("Player %s has quit.", e.getPlayer().getName());
            instance.getGeneralLocalization().broadcast("player_quit", e.getPlayer());
        }

        removeProfile(e.getPlayer().getUniqueId());
    }
}
