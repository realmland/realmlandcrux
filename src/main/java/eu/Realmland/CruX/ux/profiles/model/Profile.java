package eu.Realmland.CruX.ux.profiles.model;

import eu.Realmland.CruX.localization.Lang;
import lombok.Getter;
import lombok.Setter;
import me.MrWener.RealmLandDatabase.payload.Payload;
import org.jetbrains.annotations.NotNull;
import org.json.simple.parser.ParseException;

import javax.annotation.Nullable;
import javax.xml.stream.Location;
import java.util.*;

/**
 * Profile
 */
public class Profile {

    @Getter @NotNull
    private UUID uuid;

    @Getter @Setter @NotNull
    private String name;

    @Getter
    @Setter
    @NotNull
    private String discordID;

    @Getter
    private int rankPoints = -1;

    @Getter
    private Map<String, Location> homes = new HashMap<>();

    @Getter
    @Setter
    private boolean isPremium = false;

    @Getter @Setter
    private boolean isStaff = false;

    @Getter
    @Setter
    private boolean nsfw = false;

    @Getter
    @Setter
    private Lang lang;

    @Getter @NotNull
    private ProfileData profileData;

    @Getter
    @Setter
    boolean offline = false;


    /**
     * Constructor
     *
     * @param uuid UUID
     * @param nsfw NSFW Mode?
     * @param lang Language?
     */
    public Profile(@NotNull UUID uuid, @NotNull String name, @Nullable String discordID, int rankPoints, boolean isStaff, boolean nsfw, @Nullable ProfileData profileData, @NotNull Lang lang) {
        this.uuid = uuid;
        this.name = name;

        if (discordID == null)
            this.discordID = "";
        else
            this.discordID = discordID;

        this.rankPoints = rankPoints;
        this.isStaff = isStaff;
        this.nsfw = nsfw;

        if (profileData != null)
            this.profileData = profileData;
        else
            this.profileData = new ProfileData();

        this.lang = lang;
    }

    /**
     * Adds player's home
     * @param name      Name
     * @param location  Location of home
     */
    public void addHome(@NotNull String name, @NotNull Location location) {
        if(!hasHome(name))
            homes.remove(name);
    }

    /**
     * Removes player's home
     * @param name  Name
     */
    public void removeHome(@NotNull String name) {
        if(hasHome(name))
            homes.remove(name);
    }

    /**
     * Checks if player has s home
     * @param name  Name
     * @return True if has, else false
     */
    public boolean hasHome(@NotNull String name) {
        return homes.containsKey(name.toLowerCase());
    }

    /**
     * Adds rank points
     *
     * @param val Value
     */
    public void addRankPoints(int val) {
        rankPoints += val;
    }

    /**
     * Removes rank points
     *
     * @param val Value
     */
    public void removeRankPoints(int val) {
        rankPoints -= val;
    }

    /**
     * Sets rank points
     *
     * @param val Value
     */
    public void setRankPoints(int val) {
        rankPoints = val;
    }


    /**
     * Saves profiles to Payload
     *
     * @param profile Profile to be Saved
     * @return Payload
     */
    public static Payload toPayload(@NotNull Profile profile) {
        Payload payload = new Payload();
        payload.addColumn("uuid", profile.getUuid());
        payload.addColumn("name", profile.getName());
        payload.addColumn("discord_id", profile.getDiscordID());


        payload.addColumn("rank_points", profile.getRankPoints());
        payload.addColumn("is_staff", (profile.isStaff() ? 1 : 0));
        payload.addColumn("profile_data", profile.getProfileData().toJSON());
        payload.addColumn("nsfw_mode", (profile.isNsfw() ? 1 : 0));
        payload.addColumn("lang", profile.getLang());
        return payload;
    }

    /**
     * Loads Profile from Payload
     *
     * @param payload Payload
     * @return Profile
     */
    public static Profile fromPayload(@NotNull Payload payload) {
        UUID uuid = UUID.fromString(payload.getColumn("uuid").toString());
        String name = payload.getColumn("name");
        String discordID = payload.getColumn("discord_id");

        int rankPoints = Integer.valueOf(payload.getColumn("rank_points").toString());
        boolean nsfw = Boolean.valueOf(payload.getColumn("nsfw_mode").toString());
        boolean isStaff = Boolean.valueOf(payload.getColumn("is_staff").toString());
        Lang lang = Lang.of(payload.getColumn("lang").toString());

        ProfileData settings = null;
        try {
            settings = new ProfileData(payload.getColumn("profile_data"));
        } catch (ParseException e) {
            try {
                settings = new ProfileData(null);
            } catch (ParseException ignored) {}
            e.printStackTrace();
        }

        if (lang == null) {
            return null;
        }

        return new Profile(uuid, name, discordID, rankPoints, isStaff, nsfw, settings, lang);
    }

    public static List<String> getFields() {
        return Arrays.asList("uuid", "name", "discord_id", "rank_points", "is_staff", "profile_data", "nsfw_mode", "lang");
    }

    public static Payload getAssurer(@NotNull Profile profile) {
        return getAssurer(profile.getUuid());
    }

    public static Payload getAssurer(@NotNull UUID uuid) {
        return Payload.of("uuid", uuid);
    }
}
