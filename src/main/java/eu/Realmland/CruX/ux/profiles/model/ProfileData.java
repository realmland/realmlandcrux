package eu.Realmland.CruX.ux.profiles.model;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

public class ProfileData {

    @Getter
    private final Map<String, String> settings = new HashMap<>();

    /**
     * Default constructor
     * @param json JSON String
     * @throws ParseException Parse Exception from specified json
     */
    public ProfileData(@Nullable String json) throws ParseException {
        if(json != null) {
            JSONObject object = (JSONObject) new JSONParser().parse(json);
            object.keySet().forEach(key -> {
                String val = object.get(key).toString();
                settings.put((String) key, val);
            });
        }
    }

    /**
     * Constructor
     */
    public ProfileData() {
    }

    /**
     * Gets setting from map.
     * @param key Key of setting.
     * @return Setting
     */
    public String getSetting(@NotNull String key) {
        return settings.get(key);
    }

    /**
     * Gets setting from map.
     * @param key Key of setting.
     * @return Setting
     */
    public String setSetting(@NotNull String key, @NotNull String value) {
        return settings.put(key, value);
    }

    /**
     * Checks if profile has settings set.
     * @param key Key of setting.
     * @return Setting
     */
    public boolean hasSetting(@NotNull String key) {
        return settings.containsKey(key);
    }

    /**
     * Puts all settings into JSONObject
     * @return JSONObject
     */
    public JSONObject toJSON() {
        JSONObject object = new JSONObject();
        settings.forEach((key, val) ->{
            object.put(key, val);
        });
        return object;
    }
}
