package eu.Realmland.CruX.ux.ranks.model;

import lombok.Getter;
import lombok.Setter;
import me.MrWener.RealmLandDatabase.payload.Payload;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;

public class Rank {

    @Getter
    private String name;

    @Getter
    private String prefix;

    private int rrp;

    @Getter @Setter
    private int priority;

    @Getter
    private boolean isPremiumRank;

    /**
     * Default constructor
     * @param name    Name of Rank
     * @param prefix  Rank prefix
     * @param rrp     Rank required rank points
     * @param priority Rank priority
     * @param premium  is Premium
     */
    public Rank(@NotNull String name, @NotNull String prefix, int rrp, int priority, boolean premium) {
        this.name = name;
        this.prefix = prefix;
        this.rrp = rrp;
        this.priority = priority;
        this.isPremiumRank = premium;
    }

    public void setRRP(int rrp) {
        this.rrp = rrp;
    }

    public int getRRP() {
        return rrp;
    }


    public Payload toPayload() {
        Payload payload = new Payload();
        payload.addColumn("name", name);
        payload.addColumn("prefix", prefix);
        payload.addColumn("rrp", rrp);
        payload.addColumn("priority", priority);
        payload.addColumn("premium", (isPremiumRank ? 1 : 0));
        return payload;
    }

    public static Rank fromPayload(@NotNull Payload payload) {
        String name = payload.getColumn("name");
        String prefix = payload.getColumn("prefix");
        int rrp = payload.getColumn("rrp");
        int priority = payload.getColumn("priority");

        boolean premium = payload.getColumn("premium");
        return new Rank(name, prefix, rrp, priority, premium);
    }

    @Override
    public String toString() {
        return "name=" + name + ", rrp="+rrp+", prefix=" + prefix +  "§r, priority=" + priority + ", premium=" + isPremiumRank;
    }

    public static List<String> getFields() {
        return Arrays.asList("name", "prefix", "rrp", "priority", "premium");
    }
}
