package eu.Realmland.CruX.ux.ranks;

import eu.Realmland.CruX.CruX;
import eu.Realmland.CruX.services.IService;
import eu.Realmland.CruX.ux.ranks.model.Rank;
import lombok.Getter;
import me.MrWener.RealmLandDatabase.Database;
import me.MrWener.RealmLandDatabase.payload.Payload;
import me.MrWener.RealmLandLoggerLib.Logger;
import org.jetbrains.annotations.NotNull;

import java.sql.SQLException;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RankRegistry implements IService {

    private @NotNull CruX instance;
    private Database database;
    private Logger logger;

    @Getter
    private final SortedMap<Integer, Rank> ranks = new TreeMap<>();


    private String table = "ranks";

    /**
     * Default constructor
     *
     * @param instance Instance
     */
    public RankRegistry(@NotNull CruX instance) {
        this.instance = instance;
        this.database = instance.getDatabase();
        this.logger = instance.logger();
    }

    @Override
    public void initialize() throws Exception {
        database.executeSQL("create table if not exists `" + table + "` (" +
                "`name` varchar(64) NOT NULL PRIMARY KEY UNIQUE, " +
                "`prefix` varchar(128) NOT NULL," +
                "`rrp` int NOT NULL, " +
                "`priority` int NOT NULL," +
                "`premium` boolean NOT NULL" +
                ")");
        loadRanks();
    }

    @Override
    public void terminate() throws Exception {

    }

    /**
     * Gets rank matching name
     * @param name Rank Name
     * @return Rank
     */
    public Rank getRank(@NotNull String name) {
        Supplier<Stream<Rank>> supplier = () -> ranks.values().stream().filter(r->r.getName().equalsIgnoreCase(name));
        return (supplier.get().findFirst().isPresent() ? supplier.get().findFirst().get() : null);
    }

    /**
     * Gets rank by rank points
     * @param rankPoints Rank Points
     * @return Rank
     */
    public Rank getRank(int rankPoints) {
        Rank[] array = ranks.values().toArray(new Rank[0]);
        for(int i = 0; i < array.length; i++) {
            Rank rank = array[i];
            // if player's rp are above required rp we can allow it unless he can reach higher rank
            if(rankPoints >= rank.getRRP()) {
                // if has higher rank
                if(i+2 <= array.length) {
                    Rank higher = array[i+1];
                    // can not reach higher
                    if(rankPoints < higher.getRRP()) {
                        return rank;
                    }
                } else {
                    // there is nothing higher
                    return rank;
                }
            }
        }
        return null;
    }

    /**
     * Gets next rank of specified Rank
     *
     * @param rankPoints RP
     * @return Next rank if available, else null
     */
    public Rank getNextRank(int rankPoints) {
        // if there is higher rank than this
        Rank[] array = ranks.values().toArray(new Rank[0]);
        for (Rank next : array) {
            if (next.getRRP() > rankPoints)
                return next;
        }
        return null;
    }

    /**
     * Checks if rank exists
     *
     * @param name Rank Name
     * @return True/False
     */
    public boolean rankExists(@NotNull String name) {
        return ranks.values().stream().map(Rank::getName).collect(Collectors.toList()).contains(name);
    }

    /**
     * Adds rank
     *
     * @param rank Rank
     */

    public void addRank(@NotNull Rank rank) {
        if (rankExists(rank.getName()))
            return;
        ranks.put(rank.getRRP(), rank);
    }

    /**
     * Loads all ranks from database
     */
    public void loadRanks() {
        try {
            List<Payload> response = database.SQLSelect(table, Rank.getFields(), null);
            response.stream().map(Rank::fromPayload).forEach(this::addRank);
        } catch (SQLException e) {
            logger.error("Failed to load ranks ", e);
        }
    }
}
