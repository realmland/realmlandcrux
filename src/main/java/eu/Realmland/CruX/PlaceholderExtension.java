package eu.Realmland.CruX;

import eu.Realmland.CruX.ux.profiles.model.Profile;
import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import ru.tehkode.permissions.bukkit.PermissionsEx;

import java.util.List;

public class PlaceholderExtension extends PlaceholderExpansion {

    private @NotNull CruX instance;

    public PlaceholderExtension(@NotNull CruX instance) {
        this.instance = instance;
    }

    @Override
    public String getIdentifier() {
        return "realmland";
    }

    @Override
    public String getAuthor() {
        return "Wener#5029";
    }

    @Override
    public String getVersion() {
        return "1.0";
    }

    @Override
    public String onPlaceholderRequest(Player p, String placeholder) {
        Profile profile = instance.getProfileRegistry().getProfile(p.getUniqueId());
        if(profile != null) {
            if(placeholder.equalsIgnoreCase("rank")) {
                if(profile.isStaff()) {
                    List<String> groups = PermissionsEx.getUser(p).getParentIdentifiers();
                    return PermissionsEx.getPermissionManager().getGroup(groups.get(0)).getPrefix();
                } else
                    return instance.getRankRegistry().getRank(profile.getRankPoints()).getPrefix();
            }
            if(placeholder.equalsIgnoreCase("coins")) {
                return "-1";
            }
            if(placeholder.equalsIgnoreCase("gems")) {
                return "-1";
            }
            if(placeholder.equalsIgnoreCase("phc")) {
                return profile.hashCode() + "";
            }
            if(placeholder.equalsIgnoreCase("rp")) {
                return profile.getRankPoints() + "";
            }
            if(placeholder.equalsIgnoreCase("lang")) {
                return profile.getLang().name().toUpperCase();
            }
            if(placeholder.startsWith("generalLocalization")) {
                String message = placeholder.split("-")[1];
                return instance.getGeneralLocalization().get(message, profile.getLang());
            }
        }
        return "";
    }
}
